﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class SquaresButtonControl : MonoBehaviour
{
    private bool startGame = false;
  //  public Image Progress1;
    
    public Text txtCount1;
    public Text txtCount2;
    public Text txtCount3;
    public Text txtCount4;

    public Button btnStart;
    public Button btnMainMenu;

    //public GameObject square1;
   // public GameObject square2;
   // public GameObject square3;
   // public GameObject square4;
    private int square = 0;

    float minutes = 2;
    float seconds = 0;
    float miliseconds = 0;

    private GameObject mainmusicObject;

    // private float countDownTime = 120.0f;

    void Start()
    {
		Screen.orientation = ScreenOrientation.Landscape;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        //Instantiate(square1);
        //Instantiate(square2);
        // Instantiate(square3);
        // Instantiate(square4);
        txtCount1.text = "30";
        txtCount2.text = "30";
        txtCount3.text = "30";
        txtCount4.text = "30";
        mainmusicObject = GameObject.FindGameObjectWithTag("Music");
        if (mainmusicObject != null)
        {
            mainmusicObject.GetComponent<AudioSource>().volume = 0.2f;
        }

    }
    void Update()
    {


        // Debug.Log(square);
        if (startGame)
        {
            // Debug.Log(countDownTime);
            // countDownTime -= Time.deltaTime;
            //  Progress1.fillAmount = (countDownTime / 120);
            if (square == 1)
            {
                txtCount1.text = "" + seconds;
            }
            if (square == 2)
            {
                
                txtCount2.text = "" + seconds;
            }
            if (square == 3)
            {
                
                txtCount3.text = "" + seconds;
            }
            if (square == 4)
            {
                
                txtCount4.text = "" + seconds;
                if (seconds <= 0)
                {
                    
                    startGame = false;
                    txtCount1.text = "30";
                    txtCount2.text = "30";
                    txtCount3.text = "30";
                    txtCount4.text = "30";
                    // Progress1.fillAmount = 1;
                    btnStart.interactable = true;
                    btnStart.gameObject.SetActive(true);
                    btnMainMenu.interactable = true;
                    btnMainMenu.gameObject.SetActive(true);
                    //countDownTime = 120.0f;

                }
            }
            if (miliseconds <= 0)
            {
                if (seconds <= 0)
                {
                    square++;
                    // minutes--;
                    seconds = 29;

                }
                else if (seconds >= 0)
                {
                    seconds--;
                }

                miliseconds = 100;
            }

            miliseconds -= Time.deltaTime * 100;

        }
        else {
            if (Input.GetKeyDown(KeyCode.Escape)) { SceneManager.LoadScene("GlobalMenu", LoadSceneMode.Single); }
        }

        if (Input.GetKeyDown(KeyCode.Escape)) { SceneManager.LoadScene("GlobalMenu", LoadSceneMode.Single); }
    }

    public void BtnStart()
    {
        startGame = true;
        btnStart.interactable = false;
        btnStart.gameObject.SetActive(false);
        btnMainMenu.interactable = false;
        btnMainMenu.gameObject.SetActive(false);
        //Debug.Log("START");

    }
    public void BtnMainMenu()
    {
        //SceneManager.LoadScene("GlobalMenu", LoadSceneMode.Single);
		SceneManager.LoadScene("home", LoadSceneMode.Single);
    }
}
