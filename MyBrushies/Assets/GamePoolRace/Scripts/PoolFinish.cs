﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PoolFinish : MonoBehaviour {
    public GameObject WhistleAudioFinish;
    private AudioSource whistlefinishsound;
    public GameObject PlayerAudioFinish;
    private AudioSource playerfinishsound;
    public GameObject spawner;
    public Text txtFinish1;
    public Text txtFinish2;
    public Text txtFinish3;
    public Text txtFinish4;
   
    private PoolSpawnerScript spawnerScript;

    void Start() {
		Screen.orientation = ScreenOrientation.Landscape;
        txtFinish1.text = "";
        txtFinish2.text = "";
        txtFinish3.text = "";
        txtFinish4.text = "";
        

        spawnerScript = spawner.GetComponent<PoolSpawnerScript>();
        whistlefinishsound = WhistleAudioFinish.GetComponent<AudioSource>();
        playerfinishsound = PlayerAudioFinish.GetComponent<AudioSource>();
    }
    void OnTriggerEnter(Collider other) {

        if (other.gameObject.tag == "racer1" && spawnerScript.racer1Lap > 2) {

            spawnerScript.finishPosition++;
            spawnerScript.racer1PositionInRace = spawnerScript.finishPosition;
            whistlefinishsound.Play();
            switch (spawnerScript.finishPosition)
            {
                case 1:
                    StartCoroutine(showFinishNumbers("1",1));
                    break;
                case 2:
                    StartCoroutine(showFinishNumbers("2", 1));
                    break;
                case 3:
                    StartCoroutine(showFinishNumbers("3", 1));
                    break;
                case 4:
                    StartCoroutine(showFinishNumbers("4", 1));
                    break;
                
            }
          //  Debug.Log(spawnerScript.racer1PositionInRace);
            }
        if (other.gameObject.tag == "racer2" && spawnerScript.racer2Lap > 2)
        {

            spawnerScript.finishPosition++;
            spawnerScript.racer2PositionInRace = spawnerScript.finishPosition;
            whistlefinishsound.Play();
            switch (spawnerScript.finishPosition)
            {
                case 1:
                    StartCoroutine(showFinishNumbers("1", 2));
                    break;
                case 2:
                    StartCoroutine(showFinishNumbers("2", 2));
                    break;
                case 3:
                    StartCoroutine(showFinishNumbers("3", 2));
                    break;
                case 4:
                    StartCoroutine(showFinishNumbers("4", 2));
                    break;
                
            }
          //  Debug.Log(spawnerScript.racer2PositionInRace);
        }
        if (other.gameObject.tag == "player" && spawnerScript.racer3Lap > 2)
        {

            spawnerScript.finishPosition++;
            spawnerScript.playerPositionInRace = spawnerScript.finishPosition;
            playerfinishsound.Play();
            switch (spawnerScript.finishPosition)
            {
                case 1:
                    StartCoroutine(showFinishNumbers("1", 3));
                    break;
                case 2:
                    StartCoroutine(showFinishNumbers("2", 3));
                    break;
                case 3:
                    StartCoroutine(showFinishNumbers("3", 3));
                    break;
                case 4:
                    StartCoroutine(showFinishNumbers("4", 3));
                    break;

            }


            // Debug.Log(spawnerScript.playerPositionInRace);
        }
        if (other.gameObject.tag == "racer3" && spawnerScript.racer4Lap > 2)
        {

            spawnerScript.finishPosition++;
            spawnerScript.racer3PositionInRace = spawnerScript.finishPosition;
            whistlefinishsound.Play();
            switch (spawnerScript.finishPosition)
            {
                case 1:
                    StartCoroutine(showFinishNumbers("1", 4));
                    break;
                case 2:
                    StartCoroutine(showFinishNumbers("2", 4));
                    break;
                case 3:
                    StartCoroutine(showFinishNumbers("3", 4));
                    break;
                case 4:
                    StartCoroutine(showFinishNumbers("4", 4));
                    break;
               
            }
          //  Debug.Log(spawnerScript.racer3PositionInRace);
        }
       /* if (other.gameObject.tag == "racer4" && spawnerScript.racer4Lap > 3)
        {

            spawnerScript.finishPosition++;
            spawnerScript.racer4PositionInRace = spawnerScript.finishPosition;
            whistlefinishsound.Play();
            switch (spawnerScript.finishPosition)
            {
                case 1:
                    StartCoroutine(showFinishNumbers("1", 5));
                    break;
                case 2:
                    StartCoroutine(showFinishNumbers("2", 5));
                    break;
                case 3:
                    StartCoroutine(showFinishNumbers("3", 5));
                    break;
                case 4:
                    StartCoroutine(showFinishNumbers("4", 5));
                    break;
                
            }
           // Debug.Log(spawnerScript.racer4PositionInRace);
        }*/
 }

    IEnumerator showFinishNumbers(string position, int racer) {
        switch (racer)
        {
            case 1:
                txtFinish2.text = position;
                yield return new WaitForSeconds(1);
                txtFinish2.text = "";
                break;
            case 2:
                txtFinish3.text = position;
                yield return new WaitForSeconds(1);
                txtFinish3.text = "";
                break;
            case 3:
                txtFinish1.text = position;
                yield return new WaitForSeconds(1);
                txtFinish1.text = "";
                break;
            case 4:
                txtFinish4.text = position;
                yield return new WaitForSeconds(1);
                txtFinish4.text = "";
                break;
        }
     }
}
