﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SimpleJSON;
public class PoolSpawnerScript : MonoBehaviour
{
    public GameObject CountDownAudio;
    public GameObject WhistleAudio;
    public GameObject TurnSplashAudio;

    private AudioSource countdownsound;
    private AudioSource whistlesound;
    private AudioSource turnsplashsound;
    public Button btnStart;
    public Button btnMainMenu;
    public Text infoText;
    public GameObject[] spawnPos;
    public GameObject[] turnPos;

    public Text FinishText;

    private Transform turnPos1;
    private Transform turnPos2;
    private Transform turnPos3;
    private Transform turnPos4;


    public GameObject[] germ;

    public GameObject FinishLine;
    private float finishLineSpeed = 3.0f;
    private float finishLineStep;

    private GameObject racer1;
    private Transform racer1Transform;
    private Animator racer1EyeAnim;
    private Animator racer1MouthAnim;
    private SpriteRenderer racer1MouthRenderer;
    private SpriteRenderer racer1EyesRenderer;
    public float racer1Speed = 1.2f;
    private float racer1Step;
    private bool racer1TopSpeed = false;
   


    private GameObject racer2;
    private Transform racer2Transform;
    private Animator racer2EyeAnim;
    private Animator racer2MouthAnim;
    private SpriteRenderer racer2MouthRenderer;
    private SpriteRenderer racer2EyesRenderer;
    public float racer2Speed = 1.2f;
    private float racer2Step;
    private bool racer2TopSpeed = false;
   

    private GameObject racer3;
    private Transform racer3Transform;
    private Animator racer3EyeAnim;
    private Animator racer3MouthAnim;
    private SpriteRenderer racer3MouthRenderer;
    private SpriteRenderer racer3EyesRenderer;
    public float racer3Speed = 1.2f;
    private float racer3Step;
    private bool racer3TopSpeed = false;
   

    public GameObject endPos1;
	public Transform endPos1Transform;
    public GameObject endPos2;
	public Transform endPos2Transform;
    public GameObject endPos3;
    public Transform endPos3Transform;
    public GameObject endPos4;
	public Transform endPos4Transform;


    public GameObject playerObject;
    private GameObject player;
    private Transform playerTransform;
    private Animator playerEyeAnim;
    private Animator playerMouthAnim;
    private SpriteRenderer playerMouthRenderer;
    private SpriteRenderer playerEyesRenderer;
    public float playerSpeed = 1.2f;
    private float playerStep;
    private AudioSource playerswimAudio;
    private bool playerswimstartAudio = true;
    
    public int playerPositionInRace = 0;

    private float playerTopMaxVeer = 8.41f;
    private float playerBottomMaxVeer = 6.46f;
    public float playerveerSpeed = 0.15f;
    private Vector3 start;
    private Vector3 target;
    private Vector3 playerCurrentPosition;
    private bool Veer = true;


    public Sprite mouthSmile;
    public Sprite mouthCrazy;
    public Sprite mouthSad;
    public Sprite mouthHappy;
    public Sprite mouthSerious;
    public Sprite eyesNormal;
    public Sprite eyesHappy;


    public int racer1PositionInRace = 0;
    public int racer2PositionInRace = 0;
    public int racer3PositionInRace = 0;
    public int racer4PositionInRace = 0;



    public int finishPosition = 0;

    public Text txtCountDown;


    private int Body1Order = -12;
    private int Eyes1Order = -11;
    private int Mouth1Order = -11;
    private int Tentacle1Order = -12;
    private int Glare1Order = -10;

    private int Body2Order = -8;
    private int Eyes2Order = -7;
    private int Mouth2Order = -7;
    private int Tentacle2Order = -8;
    private int Glare2Order = -6;

    private int Body3Order = -4;
    private int Eyes3Order = -3;
    private int Mouth3Order = -3;
    private int Tentacle3Order = -4;
    private int Glare3Order = -2;


    private int i = 0;
    private int j = 0;
    private int[] randomGerm = new int[3];
    List<int> iList = new List<int>();

    private float racer1Distance = 0;
    private  Vector3 racer1lastPosition;
    private float racer2Distance = 0;
    private Vector3 racer2lastPosition;
    private float playerDistance = 0;
    private Vector3 playerlastPosition;
    private float racer3Distance = 0;
    private Vector3 racer3lastPosition;
    

    private bool racer1Finish = false;
    private bool racer2Finish = false;
    private bool racer3Finish = false;
    private bool racer4Finish = false;

    public bool startGame = false;


    public int racer1Lap = 0;
    public int racer2Lap = 0;
    public int racer3Lap = 0;
    public int racer4Lap = 0;


    private bool finishMsg = false;


    float durationInv;
    float duration = 0.5f;
    float timer = 0f;

    private GameObject mainmusicObject;

    private bool veerOf = false;

    public bool infoTxtShow = false;


    private float SpeedMultiplyer = 1.0f;
    private int bSpeedValue = 0;

    public Slider speedIndicatorSlider;

    public Button btnConnect;
    public Text txtConnect;
    private bool blink = false;
   
    private int brojac = 0;

    public GameObject exitPanel;
    public Button exitYes;
    public Button exitNo;

    public Sprite spriteSoundOn;
   // private Color32 colorSoundOn;
    public Sprite spriteSoundOff;
  //  private Color32 colorSoundOff;
    public Button btnSound;
    private bool soundOnOff = true;
    public GameObject mainAudio;

    public GameObject startPanel;

    public GameObject imgCountDown;

    public bool brushiesDemo;
    public GameObject showPlus;
    public GameObject showMinus;

    private float speedKoef = 0.0f;

    public Button btnDevice;
    public Sprite deviceOn;
   // private Color32 colorDeviceOn;
    public Sprite deviceOff;
    // private Color32 colorDeviceOff;

    private string baseURL = "http://pearly01.mybrushies.com";
    private string brushingSessionID = "";



    // Use this for initialization
    void Start()
    {
        /////DEMO OR DEVICE/////
       // colorSoundOn = new Color32(255, 223, 69, 255);
      //  colorSoundOff = new Color32(255, 0, 0, 255);
      //  colorDeviceOn = new Color32(255, 223, 69, 255);
      //  colorDeviceOff = new Color32(255, 0, 0, 255);
        if (PlayerPrefs.GetInt("DemoGame") == 0)
        {
            btnDevice.GetComponent<Image>().sprite = deviceOn;
           // btnDevice.GetComponent<Image>().color = colorDeviceOn;
            brushiesDemo = false;
            showPlus.SetActive(true);
            showMinus.SetActive(true);

        }
        else
        {
            btnDevice.GetComponent<Image>().sprite = deviceOff;
           // btnDevice.GetComponent<Image>().color = colorDeviceOff;
            brushiesDemo = true;
            showPlus.SetActive(false);
            showMinus.SetActive(false);

        }

        Debug.Log(PlayerPrefs.GetInt("DemoGame"));
        Screen.orientation = ScreenOrientation.Landscape;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        speedIndicatorSlider.value = 0.0f;
        //sliderTransform = slider.transform;
        veerOf = false;
        /*mainmusicObject = GameObject.FindGameObjectWithTag("Music");
        if (mainmusicObject != null)
        {
            mainmusicObject.GetComponent<AudioSource>().volume = 0.2f;
        }*/
        if (!infoTxtShow)
        {
            infoText.text = "";
        }

        if (brushiesDemo)
        {
            speedKoef = 0.0f;
            //showScore.SetActive(false);
            showPlus.SetActive(true);
            showMinus.SetActive(true);

        }
        else
        {
            speedKoef = 0.0f;
            //showScore.SetActive(true);
            showPlus.SetActive(false);
            showMinus.SetActive(false);
        }


        initializeRace();
        btnConnect.interactable = false;
        txtConnect.text = "";
       
    }
   
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            BtnPlus();
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            BtnMinus();
        }
        if (!brushiesDemo)
        {

        }
        // StartCoroutine(captureScr());

        if (startGame)
        {
            if (!brushiesDemo)
            {
                if (PurpleContainer.instance.bleState != PurpleToothbrush.CONNECTED)
                {
                    btnConnect.interactable = true;
                    txtConnect.color = new Color(1, 0, 0);
                    txtConnect.text = "Please connect brush device";
                    infoText.text = PurpleContainer.instance.GetLog();
                    playerSpeed = 0.0f;
                    speedIndicatorSlider.value = 0.0f;
                    //sliderTransform.position = new Vector3(-15.4f, sliderTransform.position.y, sliderTransform.position.z);
                    blink = true;
                }
                else
                {
                    btnConnect.interactable = false;
                    txtConnect.color = new Color(0, 1, 0);
                    if (blink)
                    {
                        StartCoroutine(connectTextBlink());
                    }
                    var sensorReading = PurpleContainer.instance.GetLastReading();
                     speedIndicatorSlider.value = sensorReading.velocity* SpeedMultiplyer;
                    // if (infoTxtShow) { infoText.text = string.Format("Velocity:\t{0}\nQuality:\t{1}", sensorReading.velocity, 0.0f); }

                    if (sensorReading.velocity* SpeedMultiplyer <= 0.001f)
                     {
                         speedIndicatorSlider.value = 0.0f;

                     }
                     else if (sensorReading.velocity* SpeedMultiplyer >= 1.0f)
                     {
                         speedIndicatorSlider.value = 1.0f;

                     }

                    if (sensorReading.velocity * SpeedMultiplyer < 0.1f)
                    {
                        playerSpeed = 0.0f;
                        veerOf = false;

                    }
                    else if (sensorReading.velocity * SpeedMultiplyer > 0.6f)
                    {
                        veerOf = true;
                    }
                    else
                    {
                        veerOf = false;
                        playerSpeed = 1.0f + sensorReading.velocity * SpeedMultiplyer;
                    }
                }
            }else
            {
                speedIndicatorSlider.value = speedKoef;
                if (speedKoef < 0.1f)
                {
                    playerSpeed = 0.0f;
                    veerOf = false;
                }
                else if (speedKoef > 0.6f)
                {
                    veerOf = true;
                }
                else
                {
                    veerOf = false;
                    playerSpeed = 1.0f + speedKoef;
                }
            }
            #region RACER TURNS
            #region RACER1
            racer1Step = racer1Speed * Time.deltaTime;
            if (racer1Lap == 0) { racer1Transform.position = Vector3.MoveTowards(racer1Transform.position, turnPos1.position, racer1Step); }
            if (racer1Lap == 1) { racer1Transform.position = Vector3.MoveTowards(racer1Transform.position, endPos2Transform.position, racer1Step); }
            if (racer1Lap == 2) { racer1Transform.position = Vector3.MoveTowards(racer1Transform.position, turnPos1.position, racer1Step); }
            if (racer1Lap == 3) { racer1Transform.position = Vector3.MoveTowards(racer1Transform.position, endPos2Transform.position, racer1Step); }
            if (racer1Lap == 3 && racer1Transform.position.x == endPos2Transform.position.x){ racer1Finish = true; }
                
            
            if (racer1Transform.position.x > -10.05f && racer1Transform.position.x < -10.0f)
            {
                if (racer1TopSpeed == false)
                {
                    // MoveableScript.startMove = true;
                    racer1Speed = Random.Range(1.0f, 1.6f);
                  //  StartCoroutine(setEyesAndMouth(1, 1));
                    // Debug.Log("R1: " + racer1Speed);
                }
            }
            if (racer1Transform.position.x > -5.05f && racer1Transform.position.x < -5.0f)
            {
                if (racer1TopSpeed == false)
                {
                    // MoveableScript.startMove = true;
                    racer1Speed = Random.Range(1.0f, 1.6f);
                   // StartCoroutine(setEyesAndMouth(1, 2));
                    //  Debug.Log("R1: " + racer1Speed);
                }
            }
            if (racer1Transform.position.x > 0.0f && racer1Transform.position.x < 0.05f)
            {
                if (racer1TopSpeed == false)
                {
                    // MoveableScript.startMove = true;
                    racer1Speed = Random.Range(1.0f, 1.6f);
                  //  StartCoroutine(setEyesAndMouth(1, 0));
                    //   Debug.Log("R1: " + racer1Speed);
                }
            }
            if (racer1Transform.position.x > 7.0f && racer1Transform.position.x < 7.05f)
            {
                if (racer1TopSpeed == false)
                {
                    // MoveableScript.startMove = true;
                    racer1Speed = Random.Range(1.0f, 1.6f);
                  //  StartCoroutine(setEyesAndMouth(1, 1));
                    //   Debug.Log("R1: " + racer1Speed);
                }
            }


            if (racer1Lap == 0)
            {
                if (racer1Transform.position.x >= turnPos1.position.x)
                {
                    if (racer1TopSpeed == false)
                    {
                        // MoveableScript.startMove = true;
                        racer1Speed = Random.Range(1.0f, 1.6f);
                     //   StartCoroutine(setEyesAndMouth(1, 0));
                        //   Debug.Log("R1: " + racer1Speed);
                    }
                    racer1Transform.position = new Vector3(turnPos1.position.x - 0.1f, racer1Transform.position.y, racer1Transform.position.z);
                    racer1Lap++;
                    turnsplashsound.Play();
                }
            }
            if (racer1Lap == 1)
            {
                if (racer1Transform.position.x <= endPos2Transform.position.x)
                {
                    if (racer1TopSpeed == false)
                    {
                        // MoveableScript.startMove = true;
                        racer1Speed = Random.Range(1.0f, 1.6f);
                    //    StartCoroutine(setEyesAndMouth(1, 1));
                        //   Debug.Log("R1: " + racer1Speed);
                    }
                    racer1Transform.position = new Vector3(endPos2Transform.position.x + 0.1f, racer1Transform.position.y, racer1Transform.position.z);
                    racer1Lap++;
                    turnsplashsound.Play();
                }
            }
            if (racer1Lap == 2)
            {
                if (racer1Transform.position.x >= turnPos1.position.x)
                {
                    if (racer1TopSpeed == false)
                    {
                        // MoveableScript.startMove = true;
                        racer1Speed = Random.Range(1.0f, 1.6f);
                    //    StartCoroutine(setEyesAndMouth(1, 0));
                        //   Debug.Log("R1: " + racer1Speed);
                    }
                    racer1Transform.position = new Vector3(turnPos1.position.x - 0.1f, racer1Transform.position.y, racer1Transform.position.z);
                    racer1Lap++;
                    turnsplashsound.Play();
                }
            }
            #endregion
            #region RACER2
            racer2Step = racer2Speed * Time.deltaTime;
            if (racer2Lap == 0) { racer2Transform.position = Vector3.MoveTowards(racer2Transform.position, turnPos2.position, racer2Step); }
            if (racer2Lap == 1) { racer2Transform.position = Vector3.MoveTowards(racer2Transform.position, endPos4Transform.position, racer2Step); }
            if (racer2Lap == 2) { racer2Transform.position = Vector3.MoveTowards(racer2Transform.position, turnPos2.position, racer2Step); }
            if (racer2Lap == 3) { racer2Transform.position = Vector3.MoveTowards(racer2Transform.position, endPos4Transform.position, racer2Step); }
            if (racer2Lap == 3 && racer2Transform.position.x == endPos4Transform.position.x) { racer2Finish = true; }
            

            if (racer2Transform.position.x > -10.05f && racer2Transform.position.x < -10.0f)
            {
                if (racer2TopSpeed == false)
                {
                    //  MoveableScript.startMove = true;
                    racer2Speed = Random.Range(1.0f, 1.6f);
                 //   StartCoroutine(setEyesAndMouth(2, 1));
                    //  Debug.Log("R2: " + racer2Speed);
                }
            }
            if (racer2Transform.position.x > -5.05f && racer2Transform.position.x < -5.0f)
            {
                if (racer2TopSpeed == false)
                {
                    //  MoveableScript.startMove = true;
                    racer2Speed = Random.Range(1.0f, 1.6f);
                //    StartCoroutine(setEyesAndMouth(2, 0));
                    // Debug.Log("R2: " + racer2Speed);
                }
            }
            if (racer2Transform.position.x > 0.0f && racer2Transform.position.x < 0.05f)
            {
                if (racer2TopSpeed == false)
                {
                    //  MoveableScript.startMove = true;
                    racer2Speed = Random.Range(1.0f, 1.6f);
                //    StartCoroutine(setEyesAndMouth(2, 1));
                    // Debug.Log("R2: " + racer2Speed);
                }
            }
            if (racer2Transform.position.x > 7.0f && racer2Transform.position.x < 7.05f)
            {
                if (racer2TopSpeed == false)
                {
                    //  MoveableScript.startMove = true;
                    racer2Speed = Random.Range(1.0f, 1.6f);
                //    StartCoroutine(setEyesAndMouth(2, 0));
                    // Debug.Log("R2: " + racer2Speed);
                }
            }


            if (racer2Lap == 0)
            {
                if (racer2Transform.position.x >= turnPos2.position.x)
                {
                    if (racer2TopSpeed == false)
                    {
                        // MoveableScript.startMove = true;
                        racer2Speed = Random.Range(1.0f, 1.6f);
                 //       StartCoroutine(setEyesAndMouth(2, 1));
                        //   Debug.Log("R1: " + racer1Speed);
                    }
                    racer2Transform.position = new Vector3(turnPos2.position.x - 0.1f, racer2Transform.position.y, racer2Transform.position.z);
                    racer2Lap++;
                    turnsplashsound.Play();
                }
            }
            if (racer2Lap == 1)
            {
                if (racer2Transform.position.x <= endPos4Transform.position.x)
                {
                    if (racer2TopSpeed == false)
                    {
                        // MoveableScript.startMove = true;
                        racer2Speed = Random.Range(1.0f, 1.6f);
                 //       StartCoroutine(setEyesAndMouth(2, 2));
                        //   Debug.Log("R1: " + racer1Speed);
                    }
                    racer2Transform.position = new Vector3(endPos4Transform.position.x + 0.1f, racer2Transform.position.y, racer2Transform.position.z);
                    racer2Lap++;
                    turnsplashsound.Play();
                }
            }
            if (racer2Lap == 2)
            {
                if (racer2Transform.position.x >= turnPos2.position.x)
                {
                    if (racer2TopSpeed == false)
                    {
                        // MoveableScript.startMove = true;
                        racer2Speed = Random.Range(1.0f, 1.6f);
                  //      StartCoroutine(setEyesAndMouth(2, 1));
                        //   Debug.Log("R1: " + racer1Speed);
                    }
                    racer2Transform.position = new Vector3(turnPos2.position.x - 0.1f, racer2Transform.position.y, racer2Transform.position.z);
                    racer2Lap++;
                    turnsplashsound.Play();
                }
            }
            #endregion
            #region PLAYER

            playerStep = playerSpeed * Time.deltaTime;
            if (racer3Lap == 0) { playerTransform.position = Vector3.MoveTowards(playerTransform.position, turnPos3.position, playerStep); }
            if (racer3Lap == 1) { playerTransform.position = Vector3.MoveTowards(playerTransform.position, endPos3Transform.position, playerStep); }
            if (racer3Lap == 2) { playerTransform.position = Vector3.MoveTowards(playerTransform.position, turnPos3.position, playerStep); }
            if (racer3Lap == 3) { playerTransform.position = Vector3.MoveTowards(playerTransform.position, endPos3Transform.position, playerStep); }
            if (racer3Lap == 3 && playerTransform.position.x == endPos3Transform.position.x) { racer3Finish = true; }
            

            // Debug.Log("Speed "+playerSpeed);
            // playerCurrentPosition = playerTransform.position;
            if (veerOf)
            {
                //firstVeer = true;
                playerSpeed = 0.0f;
                // playerLookCrazy();
                StartCoroutine(setEyesAndMouth(0, 1));
                if (Veer == false)
                {
                    playerTransform.Translate(Vector3.down * Time.deltaTime * playerveerSpeed, Space.Self);
                    if (playerTransform.localPosition.y <= playerBottomMaxVeer)
                    {
                        Veer = true;
                    }
                }
                if (Veer)
                {
                    playerTransform.Translate(Vector3.up * Time.deltaTime * playerveerSpeed, Space.Self);
                    if (playerTransform.localPosition.y >= playerTopMaxVeer)
                    {
                        Veer = false;
                    }
                }

            }



            if (playerSpeed > 0.0f && playerswimstartAudio)
            {

                playerswimAudio.Play();
                playerswimstartAudio = false;

            }
            if (playerSpeed <= 0.0f && playerswimAudio.isPlaying)
            {
                playerswimAudio.Stop();
                playerswimstartAudio = true;
            }


            if (racer3Lap == 0)
            {
                if (playerTransform.position.x >= turnPos3.position.x)
                {
                    playerTransform.position = new Vector3(turnPos3.position.x - 0.1f, playerTransform.position.y, playerTransform.position.z);
                    racer3Lap++;
                    turnsplashsound.Play();
                }
            }
            if (racer3Lap == 1)
            {
                if (playerTransform.position.x <= endPos3Transform.position.x)
                {
                    playerTransform.position = new Vector3(endPos3Transform.position.x + 0.1f, playerTransform.position.y, playerTransform.position.z);
                    racer3Lap++;
                    turnsplashsound.Play();
                }
            }
            if (racer3Lap == 2)
            {
                if (playerTransform.position.x >= turnPos3.position.x)
                {
                    playerTransform.position = new Vector3(turnPos3.position.x - 0.1f, playerTransform.position.y, playerTransform.position.z);
                    racer3Lap++;
                    turnsplashsound.Play();
                }
            }
            #endregion
            #region RACER3
            racer3Step = racer3Speed * Time.deltaTime;
            if (racer4Lap == 0) { racer3Transform.position = Vector3.MoveTowards(racer3Transform.position, turnPos4.position, racer3Step); }
            if (racer4Lap == 1) { racer3Transform.position = Vector3.MoveTowards(racer3Transform.position, endPos1Transform.position, racer3Step); }
            if (racer4Lap == 2) { racer3Transform.position = Vector3.MoveTowards(racer3Transform.position, turnPos4.position, racer3Step); }
            if (racer4Lap == 3) { racer3Transform.position = Vector3.MoveTowards(racer3Transform.position, endPos1Transform.position, racer3Step); }
            if (racer4Lap == 3 && racer3Transform.position.x == endPos1Transform.position.x) { racer4Finish = true; }
            if (racer3Transform.position.x > -10.0f && racer3Transform.position.x < -10.05f)
            {
                if (racer3TopSpeed == false)
                {
                    //  MoveableScript.startMove = true;
                    racer3Speed = Random.Range(1.0f, 1.6f);
                //    StartCoroutine(setEyesAndMouth(3, 0));
                    // Debug.Log("R3: " + racer3Speed);
                }
            }
            if (racer3Transform.position.x > -5.0f && racer3Transform.position.x < -5.05f)
            {
                if (racer3TopSpeed == false)
                {
                    //  MoveableScript.startMove = true;
                    racer3Speed = Random.Range(1.0f, 1.6f);
                //    StartCoroutine(setEyesAndMouth(3, 1));
                    // Debug.Log("R3: " + racer3Speed);
                }
            }
            if (racer3Transform.position.x > 0.0f && racer3Transform.position.x < 0.05f)
            {
                if (racer3TopSpeed == false)
                {
                    //  MoveableScript.startMove = true;
                    racer3Speed = Random.Range(1.0f, 1.6f);
                //    StartCoroutine(setEyesAndMouth(3, 0));
                    // Debug.Log("R3: " + racer3Speed);
                }
            }
            if (racer3Transform.position.x > 7.0f && racer3Transform.position.x < 7.05f)
            {
                if (racer3TopSpeed == false)
                {
                    //  MoveableScript.startMove = true;
                    racer3Speed = Random.Range(1.0f, 1.6f);
                //    StartCoroutine(setEyesAndMouth(3, 1));
                    // Debug.Log("R3: " + racer3Speed);
                }
            }


            if (racer4Lap == 0)
            {
                if (racer3Transform.position.x >= turnPos4.position.x)
                {
                    if (racer3TopSpeed == false)
                    {
                        // MoveableScript.startMove = true;
                        racer3Speed = Random.Range(1.0f, 1.6f);
                 //       StartCoroutine(setEyesAndMouth(3, 0));
                        //   Debug.Log("R1: " + racer1Speed);
                    }
                    racer3Transform.position = new Vector3(turnPos4.position.x - 0.1f, racer3Transform.position.y, racer3Transform.position.z);
                    racer4Lap++;
                    turnsplashsound.Play();
                }
            }
            if (racer4Lap == 1)
            {
                if (racer3Transform.position.x <= endPos1Transform.position.x)
                {
                    if (racer3TopSpeed == false)
                    {
                        // MoveableScript.startMove = true;
                        racer3Speed = Random.Range(1.0f, 1.6f);
                //        StartCoroutine(setEyesAndMouth(3, 1));
                        //   Debug.Log("R1: " + racer1Speed);
                    }
                    racer3Transform.position = new Vector3(endPos1Transform.position.x + 0.1f, racer3Transform.position.y, racer3Transform.position.z);
                    racer4Lap++;
                    turnsplashsound.Play();
                }
            }
            if (racer4Lap == 2)
            {
                if (racer3Transform.position.x >= turnPos4.position.x)
                {
                    if (racer3TopSpeed == false)
                    {
                        // MoveableScript.startMove = true;
                        racer3Speed = Random.Range(1.0f, 1.6f);
                 //       StartCoroutine(setEyesAndMouth(3, 0));
                        //   Debug.Log("R1: " + racer1Speed);
                    }
                    racer3Transform.position = new Vector3(turnPos4.position.x - 0.1f, racer3Transform.position.y, racer3Transform.position.z);
                    racer4Lap++;
                    turnsplashsound.Play();
                }
            }
            #endregion
            #endregion

            #region CALCULATE DISTANCE

            racer1Distance += Vector3.Distance(racer1Transform.position, racer1lastPosition);
            racer1lastPosition = racer1Transform.position;

            racer2Distance += Vector3.Distance(racer2Transform.position, racer2lastPosition);
            racer2lastPosition = racer2Transform.position;

            if (!veerOf)
            {
                playerDistance += Vector3.Distance(playerTransform.position, playerlastPosition);
                playerlastPosition = playerTransform.position;
            }

            racer3Distance += Vector3.Distance(racer3Transform.position, racer3lastPosition);
            racer3lastPosition = racer3Transform.position;
            #endregion

            /*Debug.Log("R1 :" + racer1Distance);
            Debug.Log("R2 :" + racer2Distance);
            Debug.Log("P :" + playerDistance);
            Debug.Log("R3 :" + racer3Distance);*/

            #region RACER POSITIONS    
            #region RACER1Pos

            if (racer1Distance > racer2Distance && racer1Distance > playerDistance && racer1Distance > racer3Distance)
                    {
                        racer1PositionInRace = 1;
                        StartCoroutine(setEyesAndMouth(1, 3));

                    }
                    else if (racer1Distance < racer2Distance && racer1Distance > playerDistance && racer1Distance > racer3Distance)
                    {
                        racer1PositionInRace = 2;
                        StartCoroutine(setEyesAndMouth(1, 0));
                    }
                    else if (racer1Distance > racer2Distance && racer1Distance < playerDistance && racer1Distance > racer3Distance)
                    {
                        racer1PositionInRace = 2;
                        StartCoroutine(setEyesAndMouth(1, 0));
                    }
                    else if (racer1Distance > racer2Distance && racer1Distance > playerDistance && racer1Distance < racer3Distance)
                    {
                        racer1PositionInRace = 2;
                        StartCoroutine(setEyesAndMouth(1, 0));
                    }
                    else if (racer1Distance < racer2Distance && racer1Distance < playerDistance && racer1Distance > racer3Distance)
                    {
                        racer1PositionInRace = 3;
                        StartCoroutine(setEyesAndMouth(1, 4));
                    }
                    else if (racer1Distance < racer2Distance && racer1Distance > playerDistance && racer1Distance < racer3Distance)
                    {
                        racer1PositionInRace = 3;
                        StartCoroutine(setEyesAndMouth(1, 4));
                    }
                    else if (racer1Distance > racer2Distance && racer1Distance < playerDistance && racer1Distance < racer3Distance)
                    {
                        racer1PositionInRace = 3;
                        StartCoroutine(setEyesAndMouth(1, 4));
                    }
                    else if (racer1Distance < racer2Distance && racer1Distance < playerDistance && racer1Distance < racer3Distance)
                    {
                        racer1PositionInRace = 4;
                        StartCoroutine(setEyesAndMouth(1, 2));
                    }
                #endregion
            #region RACER2Pos

                    if (racer2Distance > racer1Distance && racer2Distance > playerDistance && racer2Distance > racer3Distance)
                    {
                        racer2PositionInRace = 1;
                        StartCoroutine(setEyesAndMouth(2, 3));

                    }
                    else if (racer2Distance < racer1Distance && racer2Distance > playerDistance && racer2Distance > racer3Distance)
                    {
                        racer2PositionInRace = 2;
                        StartCoroutine(setEyesAndMouth(2, 0));
                    }
                    else if (racer2Distance > racer1Distance && racer2Distance < playerDistance && racer2Distance > racer3Distance)
                    {
                        racer2PositionInRace = 2;
                        StartCoroutine(setEyesAndMouth(2, 0));
                    }
                    else if (racer2Distance > racer1Distance && racer2Distance > playerDistance && racer2Distance < racer3Distance)
                    {
                        racer2PositionInRace = 2;
                        StartCoroutine(setEyesAndMouth(2, 0));
                    }
                    else if (racer2Distance < racer1Distance && racer2Distance < playerDistance && racer2Distance > racer3Distance)
                    {
                        racer2PositionInRace = 3;
                        StartCoroutine(setEyesAndMouth(2, 4));
                    }
                    else if (racer2Distance < racer1Distance && racer2Distance > playerDistance && racer2Distance < racer3Distance)
                    {
                        racer2PositionInRace = 3;
                        StartCoroutine(setEyesAndMouth(2, 4));
                    }
                    else if (racer2Distance > racer1Distance && racer2Distance < playerDistance && racer2Distance < racer3Distance)
                    {
                        racer2PositionInRace = 3;
                        StartCoroutine(setEyesAndMouth(2, 4));
                    }
                    else if (racer2Distance < racer1Distance && racer2Distance < playerDistance && racer2Distance < racer3Distance)
                    {
                        racer2PositionInRace = 4;
                        StartCoroutine(setEyesAndMouth(2, 2));
                    }
                #endregion
            #region RACER3PosPLAYER
                if (!veerOf)
                {
                   
                        if (playerDistance > racer1Distance && playerDistance > racer2Distance && playerDistance > racer3Distance)
                        {
                            racer3PositionInRace = 1;
                            StartCoroutine(setEyesAndMouth(0, 3));

                        }
                        else if (playerDistance < racer1Distance && playerDistance > racer2Distance && playerDistance > racer3Distance)
                        {
                            racer3PositionInRace = 2;
                            StartCoroutine(setEyesAndMouth(0, 0));
                        }
                        else if (playerDistance > racer1Distance && playerDistance < racer2Distance && playerDistance > racer3Distance)
                        {
                            racer3PositionInRace = 2;
                            StartCoroutine(setEyesAndMouth(0, 0));
                        }
                        else if (playerDistance > racer1Distance && playerDistance > racer2Distance && playerDistance < racer3Distance)
                        {
                            racer3PositionInRace = 2;
                            StartCoroutine(setEyesAndMouth(0, 0));
                        }
                        else if (playerDistance < racer1Distance && playerDistance < racer2Distance && playerDistance > racer3Distance)
                        {
                            racer3PositionInRace = 3;
                            StartCoroutine(setEyesAndMouth(0, 4));
                        }
                        else if (playerDistance < racer1Distance && playerDistance > racer2Distance && playerDistance < racer3Distance)
                        {
                            racer3PositionInRace = 3;
                            StartCoroutine(setEyesAndMouth(0, 4));
                        }
                        else if (playerDistance > racer1Distance && playerDistance < racer2Distance && playerDistance < racer3Distance)
                        {
                            racer3PositionInRace = 3;
                            StartCoroutine(setEyesAndMouth(0, 4));
                        }
                        else if (playerDistance < racer1Distance && playerDistance < racer2Distance && playerDistance < racer3Distance)
                        {
                            racer3PositionInRace = 4;
                            StartCoroutine(setEyesAndMouth(0, 2));
                        }
                   
                }
                else
                {
                    StartCoroutine(setEyesAndMouth(0, 1));
                }
                #endregion
            #region RACER4Pos
                if (racer3Distance > racer1Distance && racer3Distance > racer2Distance && racer3Distance > playerDistance)
                    {
                        racer4PositionInRace = 1;
                        StartCoroutine(setEyesAndMouth(3, 3));

                    }
                    else if (racer3Distance < racer1Distance && racer3Distance > racer2Distance && racer3Distance > playerDistance)
                    {
                        racer4PositionInRace = 2;
                        StartCoroutine(setEyesAndMouth(3, 0));
                    }
                    else if (racer3Distance > racer1Distance && racer3Distance < racer2Distance && racer3Distance > playerDistance)
                    {
                        racer4PositionInRace = 2;
                        StartCoroutine(setEyesAndMouth(3, 0));
                    }
                    else if (racer3Distance > racer1Distance && racer3Distance > racer2Distance && racer3Distance < playerDistance)
                    {
                        racer4PositionInRace = 2;
                        StartCoroutine(setEyesAndMouth(3, 0));
                    }
                    else if (racer3Distance < racer1Distance && racer3Distance < racer2Distance && racer3Distance > playerDistance)
                    {
                        racer4PositionInRace = 3;
                        StartCoroutine(setEyesAndMouth(3, 4));
                    }
                    else if (racer3Distance < racer1Distance && racer3Distance > racer2Distance && racer3Distance < playerDistance)
                    {
                        racer4PositionInRace = 3;
                        StartCoroutine(setEyesAndMouth(3, 4));
                    }
                    else if (racer3Distance > racer1Distance && racer3Distance < racer2Distance && racer3Distance < playerDistance)
                    {
                        racer4PositionInRace = 3;
                        StartCoroutine(setEyesAndMouth(3, 4));
                    }
                    else if (racer3Distance < racer1Distance && racer3Distance < racer2Distance && racer3Distance < playerDistance)
                    {
                        racer4PositionInRace = 4;
                        StartCoroutine(setEyesAndMouth(3, 2));
                    }
            #endregion
            #endregion
            ////Drop finish line////
            if (racer1Lap > 2 || racer2Lap > 2 || racer3Lap > 2 || racer4Lap > 2)
            {
                StartCoroutine(DropFinish());
            }
        }
      
        ////END RACE/////
        if (playerTransform.position == endPos3Transform.position)
        {
            if (!finishMsg)
            {
                StartCoroutine(FinishMessage());
            }
        }

        //if (Input.GetKeyDown(KeyCode.Escape)) { exitRace(); }
    }

   
    /// <summary>
    /// Set face expression for racers
    /// </summary>
    /// <param name="racer"></param>
    /// <param name="mood"></param>
    /// <returns></returns>
    IEnumerator setEyesAndMouth(int racer, int mood)
    {
        switch (racer)
        {
            case 0://PLAYER
                switch (mood)
                {
                    case 0://nornmal
                        playerMouthRenderer.sprite = mouthSmile;
                        playerEyesRenderer.sprite = eyesNormal;
                        break;
                    case 1://crazy
                        playerMouthRenderer.sprite = mouthCrazy;
                        playerEyesRenderer.sprite = eyesNormal;
                        break;
                    case 2://sad
                        playerMouthRenderer.sprite = mouthSad;
                        playerEyesRenderer.sprite = eyesNormal;
                        break;
                    case 3://happy
                        playerMouthRenderer.sprite = mouthHappy;
                        playerEyesRenderer.sprite = eyesHappy;
                        break;
                    case 4://serious
                        playerMouthRenderer.sprite = mouthSerious;
                        playerEyesRenderer.sprite = eyesNormal;
                        break;
                }
                break;
            case 1://RACER1
                switch (mood)
                {
                    case 0://nornmal
                        racer1MouthRenderer.sprite = mouthSmile;
                        racer1EyesRenderer.sprite = eyesNormal;
                        break;
                    case 1://crazy
                        racer1MouthRenderer.sprite = mouthCrazy;
                        racer1EyesRenderer.sprite = eyesNormal;
                        break;
                    case 2://sad
                        racer1MouthRenderer.sprite = mouthSad;
                        racer1EyesRenderer.sprite = eyesNormal;
                        break;
                    case 3://happy
                        racer1MouthRenderer.sprite = mouthHappy;
                        racer1EyesRenderer.sprite = eyesHappy;
                        break;
                    case 4://serious
                        racer1MouthRenderer.sprite = mouthSerious;
                        racer1EyesRenderer.sprite = eyesNormal;
                        break;
                }
                break;
            case 2://RACER2
                switch (mood)
                {
                    case 0://nornmal
                        racer2MouthRenderer.sprite = mouthSmile;
                        racer2EyesRenderer.sprite = eyesNormal;
                        break;
                    case 1://crazy
                        racer2MouthRenderer.sprite = mouthCrazy;
                        racer2EyesRenderer.sprite = eyesNormal;
                        break;
                    case 2://sad
                        racer2MouthRenderer.sprite = mouthSad;
                        racer2EyesRenderer.sprite = eyesNormal;
                        break;
                    case 3://happy
                        racer2MouthRenderer.sprite = mouthHappy;
                        racer2EyesRenderer.sprite = eyesHappy;
                        break;
                    case 4://serious
                        racer2MouthRenderer.sprite = mouthSerious;
                        racer2EyesRenderer.sprite = eyesNormal;
                        break;
                }
                break;
            case 3://RACER3
                switch (mood)
                {
                    case 0://nornmal
                        racer3MouthRenderer.sprite = mouthSmile;
                        racer3EyesRenderer.sprite = eyesNormal;
                        break;
                    case 1://crazy
                        racer3MouthRenderer.sprite = mouthCrazy;
                        racer3EyesRenderer.sprite = eyesNormal;
                        break;
                    case 2://sad
                        racer3MouthRenderer.sprite = mouthSad;
                        racer3EyesRenderer.sprite = eyesNormal;
                        break;
                    case 3://happy
                        racer3MouthRenderer.sprite = mouthHappy;
                        racer3EyesRenderer.sprite = eyesHappy;
                        break;
                    case 4://serious
                        racer3MouthRenderer.sprite = mouthSerious;
                        racer3EyesRenderer.sprite = eyesNormal;
                        break;
                }
                break;
        }
        yield return null;
    }

       
    /// <summary>
    /// Random top speed for racers
    /// </summary>
    /// <param name="racer"></param>
    private void TopSpeed(int racer)
    {
        switch (racer)
        {
            case 1:
                racer1TopSpeed = true;
                racer2TopSpeed = false;
                racer3TopSpeed = false;
                racer1Speed = 1.45f;
                racer2Speed = Random.Range(1.0f, 1.4f);
                racer3Speed = Random.Range(1.0f, 1.4f);
                break;
            case 2:
                racer1TopSpeed = false;
                racer2TopSpeed = true;
                racer3TopSpeed = false;
                racer2Speed = 1.45f;
                racer1Speed = Random.Range(1.0f, 1.4f);
                racer3Speed = Random.Range(1.0f, 1.4f);
                break;
            case 3:
                racer1TopSpeed = false;
                racer2TopSpeed = false;
                racer3TopSpeed = true;
                racer3Speed = 1.45f;
                racer1Speed = Random.Range(1.0f, 1.4f);
                racer2Speed = Random.Range(1.0f, 1.4f);
                break;

        }

    }


    /// <summary>
    /// initialize race, set up every variable and object
    /// </summary>
    private void initializeRace()
    {
        if (PlayerPrefs.HasKey("BrushSpeed"))
        {
            bSpeedValue = PlayerPrefs.GetInt("BrushSpeed");
            if (bSpeedValue == 0)
            {
                SpeedMultiplyer = 1.0f;

            }
            else if (bSpeedValue > 0)
            {
                SpeedMultiplyer = 1.0f + bSpeedValue / 10;
            }
            else
            {
                SpeedMultiplyer = (0.0f - bSpeedValue / 10) * -1;
            }
        }

        durationInv = 1f / (duration != 0f ? duration : 1f);

        endPos1Transform = endPos1.transform;
        endPos2Transform = endPos2.transform;
		endPos3Transform = endPos3.transform;
        endPos4Transform = endPos4.transform;
		//print ("sdfsdf");

        turnPos1 = turnPos[1].transform;//germ1
        turnPos2 = turnPos[3].transform;//germ2
        turnPos3 = turnPos[2].transform;//playerTurn
        turnPos4 = turnPos[0].transform;//germ3

        player = Instantiate(playerObject);
        //player = Instantiate(germ[5]);
        player.tag = "player";
        playerTransform = player.transform;
        playerTransform.position = spawnPos[2].transform.position;
        playerswimAudio = player.GetComponent<AudioSource>();

        start = new Vector3(playerTransform.position.x, playerTopMaxVeer, playerTransform.position.z);
        target = new Vector3(playerTransform.position.x, playerBottomMaxVeer, playerTransform.position.z);

        foreach (Transform child in playerTransform.GetChild(0))
        {
            if (child.CompareTag("Eyes"))
            {
                playerEyeAnim = child.GetComponent<Animator>();
                playerEyesRenderer = child.GetComponent<SpriteRenderer>();
            }
            if (child.CompareTag("Mouth"))
            {
                playerMouthAnim = child.GetComponent<Animator>();
                playerMouthRenderer = child.GetComponent<SpriteRenderer>();
            }
        }


        iList.Insert(0, 6);
        iList.Insert(1, 6);
        iList.Insert(2, 6);

        for (i = 0; i < 3; i++)
        {
            j = Random.Range(0, 4);
            if (!iList.Contains(j))
            {
                iList.Insert(i, j);
            }
            else
            {
                i--;
            }
        }
        randomGerm = iList.ToArray();

        for (i = 0; i < 3; i++)
        {

            if (i == 0)
            {

                racer1 = Instantiate(germ[randomGerm[i]]);
                racer1.tag = "racer1";
                racer1Transform = racer1.transform;
                racer1Transform.position = spawnPos[1].transform.position;
                foreach (Transform child in racer1Transform.GetChild(0))
                {
                    if (child.CompareTag("Body"))
                    {
                        child.GetComponent<Renderer>().sortingOrder = Body1Order;
                    }
                    if (child.CompareTag("Eyes"))
                    {
                        child.GetComponent<Renderer>().sortingOrder = Eyes1Order;
                        racer1EyeAnim = child.GetComponent<Animator>();
                        racer1EyesRenderer = child.GetComponent<SpriteRenderer>();

                    }
                    if (child.CompareTag("Mouth"))
                    {
                        child.GetComponent<Renderer>().sortingOrder = Mouth1Order;
                        racer1MouthAnim = child.GetComponent<Animator>();
                        racer1MouthRenderer = child.GetComponent<SpriteRenderer>();
                    }
                    if (child.CompareTag("Tentacle"))
                    {
                        child.GetComponent<Renderer>().sortingOrder = Tentacle1Order;
                    }

                }
                racer1Transform.GetChild(1).GetComponent<Renderer>().sortingOrder = Glare1Order;
            }

            if (i == 1)
            {
                racer2 = Instantiate(germ[randomGerm[i]]);
                racer2.tag = "racer2";
                racer2Transform = racer2.transform;
                racer2Transform.position = spawnPos[3].transform.position;
                foreach (Transform child in racer2Transform.GetChild(0))
                {
                    if (child.CompareTag("Body"))
                    {
                        child.GetComponent<Renderer>().sortingOrder = Body2Order;
                    }
                    if (child.CompareTag("Eyes"))
                    {
                        child.GetComponent<Renderer>().sortingOrder = Eyes2Order;
                        racer2EyeAnim = child.GetComponent<Animator>();
                        racer2EyesRenderer = child.GetComponent<SpriteRenderer>();
                    }
                    if (child.CompareTag("Mouth"))
                    {
                        child.GetComponent<Renderer>().sortingOrder = Mouth2Order;
                        racer2MouthAnim = child.GetComponent<Animator>();
                        racer2MouthRenderer = child.GetComponent<SpriteRenderer>();
                    }
                    if (child.CompareTag("Tentacle"))
                    {
                        child.GetComponent<Renderer>().sortingOrder = Tentacle2Order;
                    }

                }
                racer2Transform.GetChild(1).GetComponent<Renderer>().sortingOrder = Glare2Order;
            }

            if (i == 2)
            {
                racer3 = Instantiate(germ[randomGerm[i]]);
                racer3.tag = "racer3";
                racer3Transform = racer3.transform;
                racer3Transform.position = spawnPos[0].transform.position;
                foreach (Transform child in racer3Transform.GetChild(0))
                {
                    if (child.CompareTag("Body"))
                    {
                        child.GetComponent<Renderer>().sortingOrder = Body3Order;
                    }
                    if (child.CompareTag("Eyes"))
                    {
                        child.GetComponent<Renderer>().sortingOrder = Eyes3Order;
                        racer3EyeAnim = child.GetComponent<Animator>();
                        racer3EyesRenderer = child.GetComponent<SpriteRenderer>();
                    }
                    if (child.CompareTag("Mouth"))
                    {
                        child.GetComponent<Renderer>().sortingOrder = Mouth3Order;
                        racer3MouthAnim = child.GetComponent<Animator>();
                        racer3MouthRenderer = child.GetComponent<SpriteRenderer>();
                    }
                    if (child.CompareTag("Tentacle"))
                    {
                        child.GetComponent<Renderer>().sortingOrder = Tentacle3Order;
                    }

                }
                racer3Transform.GetChild(1).GetComponent<Renderer>().sortingOrder = Glare3Order;
            }

        }

        TopSpeed(Random.Range(1, 5));
        finishMsg = false;
        countdownsound = CountDownAudio.GetComponent<AudioSource>();
        whistlesound = WhistleAudio.GetComponent<AudioSource>();
        turnsplashsound = TurnSplashAudio.GetComponent<AudioSource>();
        ////////////////TEMPORARY START LOOK/////////////////////////////////////////////////////
        StartCoroutine(setEyesAndMouth(0, 0));
        StartCoroutine(setEyesAndMouth(1, 1));
        StartCoroutine(setEyesAndMouth(2, 0));
        StartCoroutine(setEyesAndMouth(3, 1));
       
        racer1Transform.position = spawnPos[1].transform.position;
        racer1lastPosition = racer1Transform.position;
        racer2Transform.position = spawnPos[3].transform.position;
        racer2lastPosition = racer2Transform.position;
        playerTransform.position = spawnPos[2].transform.position;
        playerlastPosition = playerTransform.position;
        racer3Transform.position = spawnPos[0].transform.position;
        racer3lastPosition = racer3Transform.position;
        racer1Distance = 0.0f;
        racer2Distance = 0.0f;
        playerDistance = 0.0f;
        racer3Distance = 0.0f;
        racer1Finish = false;
        racer2Finish = false;
        racer3Finish = false;
        racer4Finish = false;
        startPanel.SetActive(true);
    }

    /// <summary>
    /// Cout down to begin the race
    /// </summary>
    /// <returns></returns>
    IEnumerator StartCountdown()
    {
        imgCountDown.SetActive(true);
        //txtCountDown.color = new Color(255, 0, 0);
        txtCountDown.text = "3";
        countdownsound.Play();
        yield return new WaitForSeconds(1);
       // txtCountDown.color = new Color(255, 255, 0);
        txtCountDown.text = "2";
        countdownsound.Play();
        yield return new WaitForSeconds(1);
        //txtCountDown.color = new Color(0, 255, 0);
        txtCountDown.text = "1";
        countdownsound.Play();
        yield return new WaitForSeconds(1);
        txtCountDown.text = "Go";
        whistlesound.Play();
        yield return new WaitForSeconds(0.5f);
        imgCountDown.SetActive(false);
        startGame = true;
        yield return null;
    }

    /// <summary>
    /// Drop Finish Line
    /// </summary>
    /// <returns></returns>
    IEnumerator DropFinish()
    {
        Vector3 finish = new Vector3(-17.3f, 0.0f, 0.0f);
        finishLineStep = finishLineSpeed * Time.deltaTime;
        FinishLine.transform.position = Vector3.MoveTowards(FinishLine.transform.position, finish, finishLineStep);
        //dropFinishLine = false;
        yield return null;

    }

    /// <summary>
    /// Show Finish Message, reset values
    /// </summary>
    /// <returns></returns>
    IEnumerator FinishMessage()
    {
        finishMsg = true;
        // FinishText.enabled = true;
        if (playerswimAudio.isPlaying)
        {
            playerswimAudio.Stop();
        }
        int pos = finishPosition;
        FinishText.text = "" + pos;
        yield return new WaitForSeconds(0.5f);
        FinishText.text = "";
        yield return new WaitForSeconds(0.2f);
        FinishText.text = "" + pos;
        yield return new WaitForSeconds(0.5f);
        FinishText.text = "";
        yield return new WaitForSeconds(0.2f);
        FinishText.text = "" + pos;
        yield return new WaitForSeconds(0.5f);
        FinishText.text = "";
        yield return new WaitForSeconds(0.2f);
        FinishText.text = "" + pos;
        yield return new WaitForSeconds(0.5f);
        FinishText.text = "";
        yield return new WaitForSeconds(0.2f);
        FinishText.text = "" + pos;
        yield return new WaitForSeconds(0.5f);
        FinishText.text = "";


        btnStart.interactable = true;
        btnStart.gameObject.SetActive(true);
        btnMainMenu.interactable = true;
        btnMainMenu.gameObject.SetActive(true);
        racer1Transform.position = spawnPos[1].transform.position;
        racer1lastPosition = racer1Transform.position;
        racer2Transform.position = spawnPos[3].transform.position;
        racer2lastPosition = racer2Transform.position;
        playerTransform.position = spawnPos[2].transform.position;
        racer3lastPosition = playerTransform.position;
        racer3Transform.position = spawnPos[0].transform.position;
        racer3lastPosition = racer3Transform.position;
        finishPosition = 0;
        startGame = false;
        racer1Lap = 0;
        racer2Lap = 0;
        racer3Lap = 0;
        racer4Lap = 0;
        racer1Finish = false;
        racer2Finish = false;
        racer3Finish = false;
        racer4Finish = false;
        StartCoroutine(setEyesAndMouth(0, 0));
        StartCoroutine(setEyesAndMouth(1, 1));
        StartCoroutine(setEyesAndMouth(2, 0));
        StartCoroutine(setEyesAndMouth(3, 1));
        FinishLine.transform.position = new Vector3(-17.3f, 28.0f, 0.0f);
        speedIndicatorSlider.value = 0.0f;
        //sliderTransform.position = new Vector3(-15.4f, sliderTransform.position.y, sliderTransform.position.z);
        //btnStart.onClick.Invoke();////////////////vrti utrku stalno
        yield return null;

    }

    /// <summary>
    /// Return to Global Menu
    /// </summary>
    private void exitRace()
    {
        SceneManager.LoadScene("home", LoadSceneMode.Single);
    }

    private void endRace()
    {
        SceneManager.LoadScene("prize", LoadSceneMode.Single);
    }

    /// <summary>
    /// Start the race
    /// </summary>
    public void BtnStart()
    {
        startPanel.SetActive(false);
        StartCoroutine(StartCountdown());
        //btnStart.interactable = false;
        //btnStart.gameObject.SetActive(false);
        //btnMainMenu.interactable = false;
       // btnMainMenu.gameObject.SetActive(false);
        finishMsg = false;
        // startGame = true;
    }

    /// <summary>
    /// Return to Global Menu
    /// </summary>
    public void BtnMainMenu()
    {
        Debug.Log("EXIT");
        exitPanel.SetActive(true);
    }
 
    /// <summary>
    /// Show device connected
    /// </summary>
    /// <returns></returns>
           
    IEnumerator connectTextBlink()
    {
        blink = false;
        txtConnect.text = "Device connected";
        yield return new WaitForSeconds(3.5f);
        txtConnect.text = "";
    }
    /// <summary>
    /// Capture Screen
    /// </summary>
    /// <returns></returns>
    IEnumerator captureScr()
    {
        Application.CaptureScreenshot("D:\\Img\\PoolRace" + brojac + ".png", 2);
        brojac++;
        yield return null;
    }

    public void SoundOnOff()
    {
        if (soundOnOff)
        {
            ////Turn Sound Off
            btnSound.GetComponent<Image>().sprite = spriteSoundOff;
           // btnSound.GetComponent<Image>().color = colorSoundOff;
            AudioListener.volume = 0;
            soundOnOff = false;
        }
        else
        {
            ////Turn Sound On
            btnSound.GetComponent<Image>().sprite = spriteSoundOn;
            //btnSound.GetComponent<Image>().color = colorSoundOn;
            mainAudio.GetComponent<AudioListener>().enabled = true;
            AudioListener.volume = 1;
            soundOnOff = true;
        }
    }

    /// <summary>
    /// Simulated Speed up
    /// </summary>
    public void BtnPlus()
    {
        if (speedKoef < 0.99f)
            speedKoef += 0.1f;

        // Debug.Log("B: " + speedKoef);

    }
    /// <summary>
    /// Simulated Speed down
    /// </summary>
    public void BtnMinus()
    {
        if (speedKoef > 0.05f)
            speedKoef -= 0.1f;

        //  Debug.Log("B: " + speedKoef);
    }
    public void DemoOrDevice()
    {
        if (brushiesDemo)
        {
            btnDevice.GetComponent<Image>().sprite = deviceOn;
            //btnDevice.GetComponent<Image>().color = colorDeviceOn;
            brushiesDemo = false;
            PlayerPrefs.SetInt("DemoGame", 0);
            PlayerPrefs.Save();
            showPlus.SetActive(false);
            showMinus.SetActive(false);

        }
        else
        {
            btnDevice.GetComponent<Image>().sprite = deviceOff;
            //btnDevice.GetComponent<Image>().color = colorDeviceOff;
            brushiesDemo = true;
            PlayerPrefs.SetInt("DemoGame", 1);
            PlayerPrefs.Save();
            showPlus.SetActive(true);
            showMinus.SetActive(true);

        }
    }

    /// <summary>
    /// Panel button exit Yes
    /// </summary>
    public void BtnExitYes()
    {
       StartCoroutine(exitGame());
    }
    /// <summary>
    /// Panel button exit No
    /// </summary>
    public void BtnExitNo()
    {
        exitPanel.SetActive(false);
    }
    /// <summary>
    /// Go to GLobalMenu
    /// </summary>
    IEnumerator exitGame()
    {
       
        exitPanel.SetActive(false);
        yield return StartCoroutine(WStopBrushing());
        SceneManager.LoadScene("home", LoadSceneMode.Single);
    }
    IEnumerator endGame()
    {
        yield return StartCoroutine(WStopBrushing());
        SceneManager.LoadScene("prize", LoadSceneMode.Single);
    }

    public string GetActiveProfileID()
    {
        string val = "";
        if (PlayerPrefs.HasKey("ActiveProfile"))
        {
            val = PlayerPrefs.GetString("ActiveProfile");
        }
        else
        {
            GameObject profile = gameObject.GetComponent<Manage_HomeProfiles>().profiles[0];
            val = profile.GetComponent<Manage_Profile>().personal_data[0];
        }
        return val;
    }

    IEnumerator WStartBrushig()
    {

        Debug.Log("BBB STARTBRUSHING");
        string postScoreURL = baseURL + "/v1/brushing/begin";
        WWWForm form = new WWWForm();
        //tring timestamp = "" + System.DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss tt");



        // GetActiveProfileID(): this returns actived profile id.
        // GetBatteryLevel(): this returns the current battery level percentage.
        // GetUniqueID(): this returns the phone unique id
        // GetAdapterID(); this returns the connected BLE device Mac Address

        var requestBody = "{ \"brushing_event\": { \"id\": \"" + " " + "\", \"profile_id\": \"" + " " + "\" }}";
        //   Debug.Log("BBB requestLOG" + requestBody);
        var encoding = new System.Text.UTF8Encoding();
        var postHeader = form.headers;

        postHeader["Content-Type"] = "application/json";
        postHeader["Accept"] = "application/json";
        postHeader["Authorization"] = "Bearer " + PlayerPrefs.GetString("token");
        postHeader["Current-Profile-Id"] = GetActiveProfileID();

        WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
        yield return request;

        if (request.error != null)
        {
            // loading.SetActive(false);
            //  UIStatus.Show(Localization.Get("Battery Level Updated Failed!"));

            Debug.Log("BBB request error: " + request.error);
            Debug.Log("BBB request error: " + request.responseHeaders);
        }
        else
        {
            JSONNode jNodes = JSON.Parse(request.text);
            brushingSessionID = jNodes["brushing_event"]["id"];
            Debug.Log("BBB success: " + brushingSessionID);

        }
    }

    //Calling Start Brushing Session API
    IEnumerator WStopBrushing()
    {

        Debug.Log("BBB STOPBRUSHING");
        string postScoreURL = baseURL + "/v1/brushing/done";
        WWWForm form = new WWWForm();

        //tring timestamp = "" + System.DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss tt");



        // GetActiveProfileID(): this returns actived profile id.
        // GetBatteryLevel(): this returns the current battery level percentage.
        // GetUniqueID(): this returns the phone unique id
        // GetAdapterID(); this returns the connected BLE device Mac Address
        var requestBody = "{ \"id\": \"" + brushingSessionID + "\" }";
        Debug.Log("BBB requestLOG" + requestBody);
        var encoding = new System.Text.UTF8Encoding();
        var postHeader = form.headers;

        postHeader["Content-Type"] = "application/json";
        postHeader["Accept"] = "application/json";
        postHeader["Authorization"] = "Bearer " + PlayerPrefs.GetString("token");
        postHeader["Current-Profile-Id"] = GetActiveProfileID();

        WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
        yield return request;

        if (request.error != null)
        {
            // loading.SetActive(false);
            //  UIStatus.Show(Localization.Get("Battery Level Updated Failed!"));

            Debug.Log("BBB request error: " + request.error);
            Debug.Log("BBB request error: " + request.responseHeaders);
        }
        else
        {
            Debug.Log("BBB success: " + request.text);

        }
    }

}