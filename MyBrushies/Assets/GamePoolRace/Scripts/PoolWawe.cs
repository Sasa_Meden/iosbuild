﻿using UnityEngine;
using System.Collections;

public class PoolWawe : MonoBehaviour {
    private Transform transformThis;
    private Vector3 startPos;
    public float speed = 1.0f;
	// Use this for initialization
	void Start () {
        transformThis = transform;
        startPos = transformThis.position;
	}
	// Update is called once per frame
	void Update () {

        if(transformThis.position.x < -20.0f)
        {
            transformThis.position = new Vector3(20.0f, startPos.y, startPos.z);
        }
        transformThis.Translate(Vector3.left * Time.deltaTime * speed);
    }
}
