﻿using UnityEngine;
using System.Collections;

public class PoolRacerBobbing : MonoBehaviour
{
    public GameObject Bob;
    private Transform thisTransform;
   // private Vector3 Bobposition;
    private int bobStartDelay;
   // private float bobTO = -0.35f;
    private bool BobLimit = false;
    private float bobSpeed = 0.3f;
    // Use this for initialization
    void Start()
    {
        thisTransform = Bob.GetComponent<Transform>();
      //  Bobposition = thisTransform.localPosition;
        bobStartDelay = Random.Range(0, 2);
       // Debug.Log(bobStartDelay);
        // Debug.Log(thisTransform.localPosition.y);
    }

    // Update is called once per frame
    void Update()
    {
       // Debug.Log(BobLimit);
       // Debug.Log(thisTransform.localPosition.y);
        if (bobStartDelay == 0)
        {
            if (Time.time > 1.1f)
            {
                if (BobLimit == false)
                {
                    thisTransform.Translate(Vector3.down * Time.deltaTime * bobSpeed, Space.Self);
                    if (thisTransform.localPosition.y <= -0.35f)
                    {
                        BobLimit = true;
                    }
                }
                if (BobLimit)
                {
                    thisTransform.Translate(Vector3.up * Time.deltaTime * bobSpeed, Space.Self);
                    if (thisTransform.localPosition.y >= 0.0f)
                    {
                        BobLimit = false;
                    }
                }
            }
        }
        if (bobStartDelay == 1)
        {
            if (BobLimit == false)
            {
                thisTransform.Translate(Vector3.down * Time.deltaTime * bobSpeed, Space.Self);
                if (thisTransform.localPosition.y <= -0.35f)
                {
                    BobLimit = true;
                }
            }
            if (BobLimit)
            {
                thisTransform.Translate(Vector3.up * Time.deltaTime * bobSpeed, Space.Self);
                if (thisTransform.localPosition.y >= 0.0f)
                {
                    BobLimit = false;
                }
            }
        }
    }
}
