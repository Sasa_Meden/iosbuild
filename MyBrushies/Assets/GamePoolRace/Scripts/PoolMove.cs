﻿using UnityEngine;
using System.Collections;

public class PoolMove : MonoBehaviour {

    private Transform transformThis;
    private float speed = 0.2f;
    public bool startMove = false;
	// Use this for initialization
	void Start () {
        transformThis = this.transform;
	}
	// Update is called once per frame
	void Update () {
        if (startMove)
        {
            transformThis.Translate(Vector3.left * speed * Time.deltaTime);
        }
	}
}
