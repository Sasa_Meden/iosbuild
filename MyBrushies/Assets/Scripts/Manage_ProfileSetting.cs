﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;
using UnityEngine.SceneManagement;
public class Manage_ProfileSetting : MonoBehaviour {


	public string[] personal_data;
	public Sprite avatar;

	public GameObject name;
	public GameObject birthday_month;
	public GameObject birthday_day;
	public GameObject birthday_year;
	public GameObject photo;
	public GameObject empty_photo;
	public GameObject option_boy;
	public GameObject option_girl;
	public GameObject option_right;
	public GameObject option_left;
	public GameObject loading;
	public GameObject back_bt;
	public GameObject state_back_bt;
	public GameObject home;
	public GameObject delete_bt;
	public GameObject changebt_dialog;

	public Sprite[] childrens;
	public Sprite[] avators;
	public Texture2D[] avators_textures;
	public bool setting_flag=false;
	public bool new_flag=false;

	public GameObject _window;
	public GameObject _AvatorWindow;
	public GameObject _homeWindow;
	public GameObject _stateWindow;
	public GameObject _listWindow;
	public GameObject photo_loading;

	public GameObject file_browser;

	//when change picture with avator image, this avator_index will be changed.
	public int avator_index;

	public int personal_index;

	private string baseURL="http://pearly01.mybrushies.com";
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (setting_flag) {
			init ();
			setting_flag = false;
		}

		if (personal_index == -1) {
			delete_bt.SetActive (false);
		} else {
			delete_bt.SetActive (true);
		}

	}

	public void init(){
		avator_index = -1;
		if (!new_flag) {
			name.GetComponent<UIInput> ().value = personal_data [1];
			name.transform.FindChild("title_label").gameObject.SetActive (true);
			string birthday = personal_data [2];
			string[] days = birthday.Split ('-');
			birthday_year.GetComponent<UIInput> ().value = days [0];
			birthday_month.GetComponent<UIInput> ().value = days [1];
			birthday_day.GetComponent<UIInput> ().value = days [2];

			if (personal_data [3] == "male") {
				option_boy.GetComponent<UIToggle> ().value = true;
			} else {
				option_girl.GetComponent<UIToggle> ().value = true;
			}

			if (personal_data [5] == "") {
				if (personal_data [3] == "male") {
					empty_photo.GetComponent<UI2DSprite> ().sprite2D = childrens [0];
				} else {
					empty_photo.GetComponent<UI2DSprite> ().sprite2D = childrens [1];
				}
			} else {
				//StartCoroutine (DisplayPhoto(empty_photo, personal_data [5]));
				empty_photo.GetComponent<UI2DSprite> ().sprite2D=avatar;
			}

			if (personal_data [4] == "right") {
				option_right.GetComponent<UIToggle> ().value = true;
			} else {
				option_left.GetComponent<UIToggle> ().value = true;
			}
		} else {
			name.GetComponent<UIInput> ().value = "";
			name.transform.FindChild("Label").GetComponent<UILabel> ().text = "NAME OF CHILD";
			name.transform.FindChild("title_label").gameObject.SetActive (false);

			birthday_year.GetComponent<UIInput> ().value = "";
			birthday_month.GetComponent<UIInput> ().value = "";
			birthday_day.GetComponent<UIInput> ().value = "";
			birthday_year.transform.FindChild("Label").GetComponent<UILabel> ().text = "YEAR";
			birthday_month.transform.FindChild("Label").GetComponent<UILabel> ().text = "MONTH";
			birthday_day.transform.FindChild("Label").GetComponent<UILabel> ().text = "DAY";
			option_boy.GetComponent<UIToggle> ().value = true;
			option_right.GetComponent<UIToggle> ().value = true;
			empty_photo.GetComponent<UI2DSprite> ().sprite2D = childrens [0];
		}
	}

	public void setSexChange(){
		if (new_flag && (empty_photo.GetComponent<UI2DSprite> ().sprite2D==childrens[0] || empty_photo.GetComponent<UI2DSprite> ().sprite2D==childrens[1])) {
			if (option_boy.GetComponent<UIToggle> ().value) {
				empty_photo.GetComponent<UI2DSprite> ().sprite2D = childrens [0];
			} else {
				empty_photo.GetComponent<UI2DSprite> ().sprite2D = childrens [1];
			}
		}
	}

	public void Change_avator(){
		avator_index = PlayerPrefs.GetInt ("profile_avator_"+personal_index, -1);
		Debug.Log (avator_index);
		empty_photo.GetComponent<UI2DSprite> ().sprite2D = avators [avator_index];
	}

	public IEnumerator DisplayPhoto(GameObject obj, string photo_url) {
		photo_loading.SetActive (true);
		WWW file = new WWW(photo_url);
		yield return file;

		Texture2D sourceTex = file.texture;
		Sprite ppp = new Sprite();

        ppp = Sprite.Create(sourceTex, new Rect(0, 0, sourceTex.width, sourceTex.height), new Vector2(0, 0), 100f);
		obj.GetComponent<UI2DSprite> ().sprite2D = ppp;
		photo_loading.SetActive (false);
	}

	public void SaveProfile(){
		if(!new_flag)
			StartCoroutine (WProfileUpdateRequest ());
		else
			StartCoroutine (WCreateProfile_Request ());
	}

	public void DeleteProfile(){
		StartCoroutine (WProfileDeleteRequest ());
	}

	public void change_photo(){
		PlayerPrefs.SetInt ("temp_avator_index", personal_index);
        PlayerPrefs.Save();
		changebt_dialog.SetActive (true);
	}

	public void show_photo_dialog(){
		changebt_dialog.SetActive (true);
	}

	public void take_photo(){
		changebt_dialog.SetActive (false);
		PlayerPrefs.SetInt ("selected_tkphoto_index", PlayerPrefs.GetInt ("temp_avator_index"));
        PlayerPrefs.Save();
        //Application.LoadLevel ("camera");
        SceneManager.LoadScene("camera", LoadSceneMode.Single);
    }

	public void choose_photo_library(){
		file_browser.SetActive (true);
		changebt_dialog.SetActive (false);
	}

	public void choose_avator(){
		changebt_dialog.SetActive (false);
	}

	public void cancel(){
		changebt_dialog.SetActive (false);
	}

	IEnumerator WProfileDeleteRequest() {
		loading.SetActive (true);
		string postScoreURL= baseURL + "/v1/profiles/"+personal_data[0];
		WWWForm form = new WWWForm();
		string sex="male";
		if(option_girl.GetComponent<UIToggle>().value)
			sex="female";
		string handed="right";
		if(option_left.GetComponent<UIToggle>().value)
			handed="left";
		var requestBody = "{ \"profile\": { \"name\": \""+name.GetComponent<UIInput>().value+"\", \"date_of_birth\": \""+birthday_year.GetComponent<UIInput>().value+"-"+birthday_month.GetComponent<UIInput>().value+"-"+birthday_day.GetComponent<UIInput>().value+"\", \"gender\": \""+sex+"\", \"dominant_hand\": \""+handed+"\" }}";
		var encoding = new System.Text.UTF8Encoding();
		var postHeader = form.headers;

		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"]="application/json";
		postHeader["X-HTTP-Method-Override"]="DELETE";
		postHeader["Authorization"]="Bearer "+PlayerPrefs.GetString("token");

		WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
		yield return request;

		if (request.error != null)
		{
			loading.SetActive (false);
			UIStatus.Show (Localization.Get ("Profile Delete Failed!"));

			Debug.Log("request error: " + request.error);
			Debug.Log("request error: " + request.responseHeaders);
		}
		else
		{
			JSONNode jNodes = JSON.Parse (request.text);

			int counter = PlayerPrefs.GetInt ("profile_counter");

			for (int i = personal_index; i <= counter-1; i++) {
				PlayerPrefs.SetString ("profile_id_" + i, PlayerPrefs.GetString("profile_id_"+(i+1)));
				PlayerPrefs.SetString ("profile_name_" + i, PlayerPrefs.GetString("profile_name_"+(i+1)));
				PlayerPrefs.SetString ("profile_birthday_" + i, PlayerPrefs.GetString("profile_birthday_"+(i+1)));
				PlayerPrefs.SetString ("profile_gender_" + i, PlayerPrefs.GetString("profile_gender_"+(i+1)));
				PlayerPrefs.SetString ("profile_dominant_" + i, PlayerPrefs.GetString("profile_dominant_"+(i+1)));
				PlayerPrefs.SetString ("profile_picture_" + i, PlayerPrefs.GetString("profile_picture_"+(i+1)));
				PlayerPrefs.SetString("profile_battery_reporting_period_" + i, jNodes["profiles"][i+1]["settings"]["battery_reporting_period"]);
                PlayerPrefs.Save();
			}

			PlayerPrefs.SetInt ("profile_counter", counter-1);
            PlayerPrefs.Save();
            PlayerPrefs.DeleteKey ("profile_id_" + counter);
			PlayerPrefs.DeleteKey ("profile_name_" + counter);
			PlayerPrefs.DeleteKey ("profile_birthday_" + counter);
			PlayerPrefs.DeleteKey ("profile_gender_" + counter);
			PlayerPrefs.DeleteKey ("profile_dominant_" + counter);
			PlayerPrefs.DeleteKey ("profile_picture_" + counter);
			PlayerPrefs.DeleteKey ("profile_battery_reporting_period_" + counter);

			Debug.Log("request success");
			Debug.Log("returned data" + request.text);
			loading.SetActive (false);
			//home.GetComponent<Manage_HomeProfiles> ().reinitialize_flag=true;
			home.GetComponent<Manage_HomeProfiles> ().remove_specific_profile(personal_index);
			_listWindow.GetComponent<Manage_Profile_List> ().reinitialize_flag = true;

			back_bt.GetComponent<UIPlayTween> ().Play (true);
			state_back_bt.GetComponent<UIPlayTween> ().Play (true);
		}
	}

	public Texture2D textureFromSprite(Sprite sprite)
	{
		if(sprite.rect.width != sprite.texture.width){
			Texture2D newText = new Texture2D((int)sprite.rect.width,(int)sprite.rect.height);
            Debug.Log("SIZE " + (int)sprite.rect.width + "..." + (int)sprite.rect.height);
            Color[] newColors = sprite.texture.GetPixels((int)sprite.rect.x, 
				(int)sprite.rect.y, 
				(int)sprite.rect.width, 
				(int)sprite.rect.height );
			newText.SetPixels(newColors);
			newText.Apply();
			return newText;
		} else
			return sprite.texture;
	}

	IEnumerator WProfileUpdateRequest() {
		loading.SetActive (true);
		string postScoreURL= baseURL + "/v1/profiles/"+personal_data[0];
		WWWForm form = new WWWForm();
		string sex="male";
		if(option_girl.GetComponent<UIToggle>().value)
			sex="female";
		string handed="right";
		if(option_left.GetComponent<UIToggle>().value)
			handed="left";

		var requestBody = "";
		if (avator_index != -1) {
			Texture2D avatar_text = textureFromSprite(avators [avator_index]);
			byte[] bytes = avatar_text.EncodeToPNG ();
			string enc = System.Convert.ToBase64String (bytes);
			requestBody = "{ \"profile\": { \"name\": \""+name.GetComponent<UIInput>().value+"\", \"date_of_birth\": \""+birthday_year.GetComponent<UIInput>().value+"-"+birthday_month.GetComponent<UIInput>().value+"-"+birthday_day.GetComponent<UIInput>().value+"\", \"gender\": \""+sex+"\", \"dominant_hand\": \""+handed+"\", \"picture\": \"data:image/png;base64,"+enc+"\"}}";
		} else {
			requestBody = "{ \"profile\": { \"name\": \""+name.GetComponent<UIInput>().value+"\", \"date_of_birth\": \""+birthday_year.GetComponent<UIInput>().value+"-"+birthday_month.GetComponent<UIInput>().value+"-"+birthday_day.GetComponent<UIInput>().value+"\", \"gender\": \""+sex+"\", \"dominant_hand\": \""+handed+"\" }}";
		}

		var encoding = new System.Text.UTF8Encoding();
		var postHeader = form.headers;

		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"]="application/json";
		postHeader["X-HTTP-Method-Override"]="PUT";
		postHeader["Authorization"]="Bearer "+PlayerPrefs.GetString("token");

		WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
		yield return request;

		if (request.error != null)
		{
			loading.SetActive (false);
			UIStatus.Show (Localization.Get ("Profile Update Failed!"));

			Debug.Log("request error: " + request.error);
			Debug.Log("request error: " + request.responseHeaders);
		}
		else
		{
			JSONNode jNodes = JSON.Parse (request.text);
			string[] p_data = new string[6];
			PlayerPrefs.SetString ("profile_id_"+personal_index, jNodes["profile"]["id"]);
			PlayerPrefs.SetString ("profile_name_"+personal_index, jNodes["profile"]["name"]);
			PlayerPrefs.SetString ("profile_birthday_"+personal_index, jNodes["profile"]["date_of_birth"]);
			PlayerPrefs.SetString ("profile_gender_"+personal_index, jNodes["profile"]["gender"]);
			PlayerPrefs.SetString ("profile_dominant_"+personal_index, jNodes["profile"]["dominant_hand"]);
            PlayerPrefs.SetString("profile_battery_reporting_period_" + personal_index, jNodes["profile"]["settings"]["battery_reporting_period"]);
            PlayerPrefs.Save();
			p_data [0] = jNodes ["profile"] ["id"];
			p_data [1] = jNodes ["profile"] ["name"];
			p_data [2] = jNodes ["profile"] ["date_of_birth"];
			p_data [3] = jNodes ["profile"] ["gender"];
			p_data [4] = jNodes ["profile"] ["dominant_hand"];

            

            if (avator_index != -1)
            {
                PlayerPrefs.SetString("profile_picture_" + personal_index, jNodes["profile"]["picture"]);
				p_data [4] = jNodes ["profile"] ["picture"];
                PlayerPrefs.Save();
            }
            Debug.Log("request success");
			Debug.Log("returned data" + request.text);
			loading.SetActive (false);

			_homeWindow.GetComponent<Manage_HomeProfiles> ().init_specific_profile (personal_index);
			_homeWindow.GetComponent<Manage_HomeProfiles> ().SetAvatar_SpecificProfile (empty_photo.GetComponent<UI2DSprite> ().sprite2D, personal_index);
			_listWindow.GetComponent<Manage_Profile_List> ().SetAvatar_SpecificProfile (empty_photo.GetComponent<UI2DSprite> ().sprite2D, personal_index, p_data);
			_stateWindow.GetComponent<Manage_StateWindow> ().avatar = empty_photo.GetComponent<UI2DSprite> ().sprite2D;
			_stateWindow.GetComponent<Manage_StateWindow> ().init ();

			back_bt.GetComponent<UIPlayTween> ().Play (true);
		}
	}

	IEnumerator WCreateProfile_Request() {
		loading.SetActive (true);
      
        string postScoreURL= baseURL + "/v1/profiles";
		string pwd = PlayerPrefs.GetString ("password");
		WWWForm form = new WWWForm();

		var encoding = new System.Text.UTF8Encoding();
		var postHeader = form.headers;
		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"]="application/json";
		postHeader["Authorization"]="Bearer "+PlayerPrefs.GetString("token");
        Debug.Log (postHeader);
        var requestBody="{\"profile\": {";
		string name_str=name.GetComponent<UIInput>().value;
		string date_month=birthday_month.GetComponent<UIInput>().value;
		string date_day=birthday_day.GetComponent<UIInput>().value;
		string date_year=birthday_year.GetComponent<UIInput>().value;
		string sex="male";
		if(option_girl.GetComponent<UIToggle>().value){
			sex="female";
		}
		if(option_boy.GetComponent<UIToggle>().value){
			sex="male";
		}
		string handed="right";
		if(option_left.GetComponent<UIToggle>().value){
			handed="left";
		}
		if(option_right.GetComponent<UIToggle>().value){
			handed="right";
		}
		if(name_str==""){
			loading.SetActive (false);
			UIStatus.Show (Localization.Get ("Name Of Child field could not be empty!"));
		}else if(date_month==""){
			loading.SetActive (false);
			Debug.Log ("Date of Month: "+date_month);
			UIStatus.Show (Localization.Get ("Month of Birthday field could not be empty!"));
		}else if(date_day==""){
			loading.SetActive (false);
			UIStatus.Show (Localization.Get ("Day of Birthday field could not be empty!"));
		}else if(date_year==""){
			loading.SetActive (false);
			UIStatus.Show (Localization.Get ("Year of Birthday field could not be empty!"));
		}else{
			requestBody+="\"name\": \""+name_str+"\", \"date_of_birth\": "+"\""+date_year+"-"+date_month+"-"+date_day+"\", \"gender\": \""+sex+"\", \"dominant_hand\": \""+handed+"\"";
			if (avator_index != -1) {
                

                byte[] bytes = textureFromSprite(avators[avator_index]).EncodeToPNG();//avators_textures [avator_index].EncodeToPNG ();
                string enc = System.Convert.ToBase64String (bytes);
				requestBody += ", \"picture\": \"data:image/png;base64," + enc + "\"}}";
			} else {
				requestBody+="}}";
			}
            

            WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
			yield return request;

			if (request.error != null)
			{
				loading.SetActive (false);
				UIStatus.Show (Localization.Get ("SignUp Failed When Create Profile!"));

				Debug.Log("request error: " + request.error);
				Debug.Log("request error: " + request.responseHeaders);
			}
			else
			{
				int profile_counter = PlayerPrefs.GetInt ("profile_counter");
				profile_counter+=1;
				JSONNode jNodes = JSON.Parse (request.text);
				string[] data = new string[6];
				data [0] = jNodes ["profile"] ["id"];
				data [1] = jNodes ["profile"] ["name"];
				data [2] = jNodes ["profile"] ["date_of_birth"];
				data [3] = jNodes ["profile"] ["gender"];
				data [4] = jNodes ["profile"] ["dominant_hand"];
				data [5] = jNodes ["profile"] ["picture"];

                //data[6] = jNodes["profile"]["settings"]["battery_reporting_period"];

                //Debug.Log("requestXXX_MPSCreate " + jNodes["profile"]["settings"]["battery_reporting_period"]);


                PlayerPrefs.SetString ("profile_id_"+profile_counter, jNodes["profile"]["id"]);
				PlayerPrefs.SetString ("profile_name_"+profile_counter, jNodes["profile"]["name"]);
				PlayerPrefs.SetString ("profile_birthday_"+profile_counter, jNodes["profile"]["date_of_birth"]);
				PlayerPrefs.SetString ("profile_gender_"+profile_counter, jNodes["profile"]["gender"]);
				PlayerPrefs.SetString ("profile_dominant_"+profile_counter, jNodes["profile"]["dominant_hand"]);
				PlayerPrefs.SetString ("profile_picture_"+profile_counter, jNodes["profile"]["picture"]);
                ////////////////////////////////////////
                PlayerPrefs.SetString("profile_battery_reporting_period_" + profile_counter, jNodes["profile"]["settings"]["battery_reporting_period"]);

                ////////////////////////////////////////
                PlayerPrefs.SetInt("profile_counter", profile_counter);
				PlayerPrefs.Save();
				Debug.Log("request success");
				Debug.Log("returned data" + request.text);

				home.GetComponent<Manage_HomeProfiles> ().add_profile (data);
				if (avator_index != -1) {
					home.GetComponent<Manage_HomeProfiles> ().add_profile_image (empty_photo.GetComponent<UI2DSprite>().sprite2D);
				}
				_listWindow.GetComponent<Manage_Profile_List> ().reinitialize_flag = true;
				loading.SetActive (false);
				back_bt.GetComponent<UIPlayTween> ().Play (true);
			}
		}
	}
}
