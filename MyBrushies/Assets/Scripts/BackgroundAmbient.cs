﻿using UnityEngine;
using System.Collections;

public class BackgroundAmbient : MonoBehaviour {
	public AudioSource[] BirdsAudio;
	int index = 0;
	// Use this for initialization
	void Start () {
		StartCoroutine (PlayBirds ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	IEnumerator PlayBirds(){
		index = Random.Range (1, 4);

		if (!BirdsAudio [index].isPlaying) {
			BirdsAudio [index].Play ();
		}
		yield return new WaitForSeconds(Random.Range(4.0f,7.0f));
		StartCoroutine (PlayBirds ());
	
	}
}
