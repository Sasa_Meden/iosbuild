﻿using UnityEngine;
using System.Collections;

public class Manage_ChooseAvator_Window : MonoBehaviour {

	public GameObject dialog;
	public GameObject avator_window;
	public GameObject profile_window;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void save_avator(){
//		dialog.SetActive (false);
//		avator_window.GetComponent<Manage_AvatorPage> ().avator_index = PlayerPrefs.GetInt ("temp_avator_index");
		PlayerPrefs.SetInt ("avator_index", PlayerPrefs.GetInt("temp_avator_index"));
        PlayerPrefs.Save();
	}

	public void save_avator_home(){
		profile_window.GetComponent<Manage_ProfileSetting> ().Change_avator ();
	}
}
