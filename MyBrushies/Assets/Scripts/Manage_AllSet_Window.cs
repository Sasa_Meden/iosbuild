﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manage_AllSet_Window : MonoBehaviour {

	public GameObject back_bt;
	// Use this for initialization
	void Start () {
		Screen.orientation = ScreenOrientation.Portrait;
		if (PlayerPrefs.GetInt ("setting_flag", 0) == 1) {
			back_bt.GetComponent<UIPlayTween> ().Play (true);
			PlayerPrefs.SetInt ("setting_flag", 0);
            PlayerPrefs.Save();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
