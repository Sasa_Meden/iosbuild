using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SampleWebView : MonoBehaviour
{
    public string Url;
//    public GUIText status;
	public WebViewObject webViewObject;
	public bool webview_flag = false;
    private bool game_flag = false;
    
    void Start(){
        game_flag = false;
        }

    void Update()
    {
       // Debug.Log("CCCC WW " + game_flag);

        /* if (Input.GetKeyDown(KeyCode.Escape) && game_flag)
            SceneManager.LoadScene("home", LoadSceneMode.Single);*/
    }





    public void goto_webview(){
        game_flag = true;
        Application.OpenURL ("http://ec2-35-165-32-215.us-west-2.compute.amazonaws.com/reggieballoonsv2/reggie/index.html");
        
        //		StartCoroutine (ShowWebView ());
    }

    public void goto_buyview()
    {

        Application.OpenURL("http://www.mybrushies.com/checkout.html");
    }


    IEnumerator ShowWebView()
    {
        webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();

        webViewObject.Init(
            cb: (msg) =>
            {
                Debug.Log(string.Format("CallFromJS[{0}]", msg));
//                status.text = msg;
//                status.GetComponent<Animation>().Play();
            },
            err: (msg) =>
            {
                Debug.Log(string.Format("CallOnError[{0}]", msg));
//                status.text = msg;
//                status.GetComponent<Animation>().Play();
            },
            ld: (msg) =>
            {
                Debug.Log(string.Format("CallOnLoaded[{0}]", msg));
#if !UNITY_ANDROID
                webViewObject.EvaluateJS(@"
                  window.Unity = {
                    call: function(msg) {
                      var iframe = document.createElement('IFRAME');
                      iframe.setAttribute('src', 'unity:' + msg);
                      document.documentElement.appendChild(iframe);
                      iframe.parentNode.removeChild(iframe);
                      iframe = null;
                    }
                  }
                ");
#endif
            },
            enableWKWebView: false);
        //webViewObject.SetMargins(0, 0, 5, Screen.height / 4);
        webViewObject.SetVisibility(true);

#if !UNITY_WEBPLAYER
//		webViewObject.LoadURL("http://ec2-35-165-32-215.us-west-2.compute.amazonaws.com/reggieballoons/index.html");
		webViewObject.LoadURL("https://www.google.com");
//        if (Url.StartsWith("http")) {
//            webViewObject.LoadURL(Url.Replace(" ", "%20"));
//        } else {
//            var exts = new string[]{
//                ".jpg",
//                ".html"  // should be last
//            };
//            foreach (var ext in exts) {
//                var url = Url.Replace(".html", ext);
//                var src = System.IO.Path.Combine(Application.streamingAssetsPath, url);
//                var dst = System.IO.Path.Combine(Application.persistentDataPath, url);
//                byte[] result = null;
//                if (src.Contains("://")) {  // for Android
//                    var www = new WWW(src);
//                    yield return www;
//                    result = www.bytes;
//                } else {
//                    result = System.IO.File.ReadAllBytes(src);
//                }
//                System.IO.File.WriteAllBytes(dst, result);
//                if (ext == ".html") {
//                    webViewObject.LoadURL("file://" + dst.Replace(" ", "%20"));
//                    break;
//                }
//            }
//        }
#else
//        if (Url.StartsWith("http")) {
//            webViewObject.LoadURL(Url.Replace(" ", "%20"));
//        } else {
//            webViewObject.LoadURL("StreamingAssets/" + Url.Replace(" ", "%20"));
//        }
		//webViewObject.LoadURL("http://ec2-35-165-32-215.us-west-2.compute.amazonaws.com/reggieballoons/index.html");
		webViewObject.LoadURL("http://35.160.6.79:3000");
        webViewObject.EvaluateJS(
            "parent.$(function() {" +
            "   window.Unity = {" +
            "       call:function(msg) {" +
            "           parent.unityWebView.sendMessage('WebViewObject', msg)" +
            "       }" +
            "   };" +
            "});");
#endif
		webview_flag = true;
        yield break;
    }

#if !UNITY_WEBPLAYER
    void OnGUI()
    {
//        GUI.enabled = webViewObject.CanGoBack();
//        if (GUI.Button(new Rect(10, 10, 80, 80), "<")) {
//            webViewObject.GoBack();
//        }
//        GUI.enabled = true;
//
//        GUI.enabled = webViewObject.CanGoForward();
//        if (GUI.Button(new Rect(100, 10, 80, 80), ">")) {
//            webViewObject.GoForward();
//        }
//        GUI.enabled = true;
    }
#endif
}
