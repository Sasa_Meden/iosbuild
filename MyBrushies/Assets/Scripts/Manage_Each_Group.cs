﻿using UnityEngine;
using System.Collections;

public class Manage_Each_Group : MonoBehaviour {

	public int trigger_flag=-1;
	public bool flag=false;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("MemberPosition:" + transform.position);
		if (flag) {
			switch (trigger_flag) {
				case 0:
					transform.GetComponent<Animator> ().SetTrigger ("non");
					transform.GetComponent<Animator> ().enabled = true;
					flag = false;
					//trigger_flag = -1;
					break;
				case 1:
					transform.GetComponent<Animator> ().SetTrigger ("zoom out");
					transform.GetComponent<Animator> ().enabled = true;
					flag = false;
					//trigger_flag = -1;
					break;
				case 2:
					transform.GetComponent<Animator> ().SetTrigger ("zoom in");
					transform.GetComponent<Animator> ().enabled = true;
					flag = false;
					//trigger_flag = -1;
					break;
			}
		}
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "friend")
            Debug.Log("Friend Conflict");
    }
}
