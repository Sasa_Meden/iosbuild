﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goto_Profile : MonoBehaviour {

	public int index;
	public string[] personal_data;
	public GameObject profile;
	public GameObject home_window;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Goto (){
		profile.GetComponent<Manage_ProfileSetting> ().personal_data = personal_data;
		profile.GetComponent<Manage_ProfileSetting> ().personal_index = index;
		profile.GetComponent<Manage_ProfileSetting> ().setting_flag = true;
		profile.GetComponent<Manage_ProfileSetting> ().avatar = home_window.GetComponent<Manage_HomeProfiles> ().profiles [index-1].GetComponent<Manage_Profile> ().avatar;
		GetComponent<UIPlayTween> ().Play (true);
	}
}
