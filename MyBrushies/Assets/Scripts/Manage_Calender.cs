﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;
using SimpleJSON;

public class Manage_Calender : MonoBehaviour {

	public string[] Months;
	public string[] iMonths;
	private DateTime iMonth;
	public DateTime Today;
	public int sizeofmonth;
	public int w_index;
	public int t_day;
	public int t_month;
	public int t_year;

	public int from_day;// first day of this week
	public int to_day;// last day of this week
	public int from_index;// first day position of this week
	public int to_index;// last day position of this week

	public int to_month_end;// the size of this month

	public bool month_end_flag=false;//if true, then this week is the last week of this month
	public bool month_start_flag=false;//if true, then this week is the first week of this month
	public bool year_end_flag=false;//if true, then this month is the last month of this year
	public bool year_start_flag=false;//it true, then this month is the first month of this year

	public GameObject title;
	public GameObject[] days;
	public GameObject[] am_boxes;
	public GameObject[] pm_boxes;

	public Sprite active;
	public Sprite inactive;
	public Sprite empty;

	public JSONNode jStatics;
	public bool init_flag;
	// Use this for initialization
	void Start () {
		CreateMonths ();
		initialize ();
		init_flag = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (init_flag) {
			setup ();
			init_flag = false;
		}
	}

	void initialize(){
		// get information of current day
		Today = System.DateTime.Now;
		w_index = GetDays(Today.DayOfWeek);
		t_day = Today.Day;
		t_month = Today.Month;
		t_year = Today.Year;

		// get size of this month
		get_month_end (t_year, t_month);

		// get first day of this week
		from_day = t_day - w_index + 1;
		from_index = 0;
		if (from_day < 1) {
			DateTime tmp = new DateTime (t_year, t_month, 1);
			from_day = 1;
			from_index = GetDays (tmp.DayOfWeek)-1;
			month_start_flag = true;
			if (t_month == 1) {
				year_start_flag = true;
			}
		}

		//get last day of this week
		to_day = from_day + 6;
		to_index = 6;
		if (to_day >= sizeofmonth) {
			to_day = sizeofmonth;
			DateTime tmp = new DateTime (t_year, t_month, to_day);
			to_index = GetDays (tmp.DayOfWeek)-1;
			month_end_flag = true;
			if (t_month == 12) {
				year_end_flag = true;
			}
		}

		setup ();
	}

	public void next(){
		get_month_end (t_year, t_month);
		if (month_end_flag) {
			t_month += 1;
			month_end_flag = false;
			month_start_flag = true;
			if (year_end_flag) {
				t_month = 1;
				t_year += 1;
				year_end_flag = false;
				year_start_flag = true;
			}

			DateTime tmp = new DateTime (t_year, t_month, 1);
			from_day = 1;
			from_index = GetDays (tmp.DayOfWeek)-1;
			to_day = from_day + (6-from_index);
			to_index = 6;
			month_start_flag = true;
			month_end_flag = false;
		} else {
			month_start_flag = false;
			year_start_flag = false;
			from_day += (7-from_index);
			from_index=0;
			//get last day of this week
			to_day = from_day + 6;
			to_index = 6;
			if (to_day >= sizeofmonth) {
				to_day = sizeofmonth;
				DateTime tmp = new DateTime (t_year, t_month, to_day);
				to_index = GetDays (tmp.DayOfWeek)-1;
				month_end_flag = true;
				if (t_month == 12) {
					year_end_flag = true;
				}
			}
		}
		setup ();
	}

	public void previous(){
		if (month_start_flag) {
			t_month -= 1;
			month_start_flag = false;
			month_end_flag = true;
			if (year_start_flag) {
				t_month = 12;
				t_year -= 1;
				year_start_flag = false;
				year_end_flag = true;
			}
			get_month_end (t_year, t_month);
			to_day = sizeofmonth;
			DateTime tmp = new DateTime (t_year, t_month, to_day);
			to_index = GetDays (tmp.DayOfWeek) - 1;
			from_day = to_day-to_index;
			from_index = 0;
		} else {
			to_day -= (to_index+1);
			to_index = 6;
			from_day = to_day - 6;
			from_index = 0;
			if (from_day < 1) {
				DateTime tmp = new DateTime (t_year, t_month, 1);
				from_day = 1;
				from_index = GetDays (tmp.DayOfWeek)-1;
				month_start_flag = true;
				if (t_month == 1) {
					year_start_flag = true;
				}
			}
			if (month_end_flag) {
				month_end_flag = false;
			}
			if (year_end_flag) {
				year_end_flag = false;
			}
		}
		setup ();
	}

	void get_month_end(int year, int month){
		DateTime startofmonth = new DateTime (year, month, 1);
		DateTime EndOfThisDay = startofmonth;
		int curDays = GetDays(EndOfThisDay.DayOfWeek);
		int index = 0;

		if(curDays > 0)
			index = (curDays - 1);
		else
			index = curDays;

		int counter = 0;
		while(EndOfThisDay.Month == startofmonth.Month)
		{
			counter++;
			EndOfThisDay = EndOfThisDay.AddDays(1);
			index++;
		}
		sizeofmonth = counter;
	}

	void setup(){
		string f_day_str;
		if (from_day < 10)
			f_day_str = "0" + from_day;
		else
			f_day_str = "" + from_day;

		string t_day_str;
		if (to_day < 10)
			t_day_str = "0" + to_day;
		else
			t_day_str = "" + to_day;

		title.GetComponent<UILabel> ().text = Months [t_month-1] + " "+f_day_str+"-"+t_day_str+", "+t_year;
		for (int j = 0; j < 7; j++) {
			days [j].GetComponent<UILabel> ().text = "";
			am_boxes [j].transform.GetComponent<UIPlayTween> ().Play(true);
			pm_boxes [j].transform.GetComponent<UIPlayTween> ().Play(true);
			am_boxes [j].SetActive (false);
			pm_boxes [j].SetActive (false);
		}
		int counter = 0;
		for (int i = from_index; i <=to_index; i++) {
			days [i].GetComponent<UILabel> ().text = ""+(from_day + counter);
			am_boxes [i].GetComponent<UI2DSprite> ().sprite2D = empty;
			pm_boxes [i].GetComponent<UI2DSprite> ().sprite2D = empty;
			am_boxes [i].SetActive (true);
			pm_boxes [i].SetActive (true);
			am_boxes [i].transform.GetComponent<UIPlayTween> ().Play (true);
			pm_boxes [i].transform.GetComponent<UIPlayTween> ().Play (true);

			string date;
			if((from_day+counter)>=10)
				date = t_year + "-" + iMonths [t_month-1]+"-"+(from_day+counter);
			else
				date = t_year + "-" + iMonths [t_month-1]+"-0"+(from_day+counter);
			DateTime _dDate = DateTime.Parse (date);
			DateTime _now=System.DateTime.Now;
			if (_dDate.Ticks <= _now.Ticks) {
				if (jStatics != null) {
					if (jStatics ["brushing_statistics"] [date] ["am"].AsBool) {
						am_boxes [i].GetComponent<UI2DSprite> ().sprite2D = active;
					} else {
						am_boxes [i].GetComponent<UI2DSprite> ().sprite2D = inactive;
					}
				}
			}

			if (_dDate.Ticks < _now.Ticks) {
				if (jStatics != null) {
					if (jStatics ["brushing_statistics"] [date] ["pm"].AsBool) {
						pm_boxes [i].GetComponent<UI2DSprite> ().sprite2D = active;
					} else {
						pm_boxes [i].GetComponent<UI2DSprite> ().sprite2D = inactive;
					}
				}
			}

			if (date == _now.ToString("yyyy-MM-dd")) {
				if (jStatics != null) {
					if (_now.ToString ("tt", CultureInfo.InvariantCulture) == "PM") {
						Debug.Log (_dDate);
						Debug.Log (_now);
						Debug.Log (_now.ToString ("tt", CultureInfo.InvariantCulture));
						if (jStatics ["brushing_statistics"] [date] ["pm"].AsBool) {
							pm_boxes [i].GetComponent<UI2DSprite> ().sprite2D = active;
						} else {
							pm_boxes [i].GetComponent<UI2DSprite> ().sprite2D = inactive;
						}
					} else {
						pm_boxes [i].GetComponent<UI2DSprite> ().sprite2D = empty;
					}
				}
			}

			counter+=1;
		}
	}

	void GetAllBrushieDays(){
		
	}

	void CreateMonths(){
		Months = new string[12];
		iMonth = new DateTime(2009, 1, 1);

		for(int i = 0; i < 12; ++i)
		{
			iMonth = new DateTime(2009, i+1, 1);
			Months[i] = iMonth.ToString("MMMM").ToUpper();
		}
	}

	private int GetDays(DayOfWeek day)
	{
		switch(day)
		{
		case DayOfWeek.Monday:      return 1;
		case DayOfWeek.Tuesday:     return 2;
		case DayOfWeek.Wednesday:   return 3;
		case DayOfWeek.Thursday:    return 4;
		case DayOfWeek.Friday:      return 5;
		case DayOfWeek.Saturday:    return 6;
		case DayOfWeek.Sunday:      return 7;
		default:                    throw new Exception("Unexpected DayOfWeek: " + day);
		}
	}
}
