﻿using UnityEngine;
using System.Collections;

public class Manage_Drag_Object : MonoBehaviour {

    public int type;
    public bool flag = false;
    public GameObject friends_scroll;
    public GameObject clone_parent;

    public float gap_time = 0.5f;
    public bool down_flag = false;
    public bool longclick_flag = false;

    float gap_init_time = 0.5f;

	public string name;
	public string id;

    // Use this for initialization
    void Start () {
		Screen.fullScreen = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (down_flag && !longclick_flag)
            gap_time -= Time.deltaTime;

        if (gap_time < 0)
        {
            if (down_flag)
            {
				Handheld.Vibrate ();
                longclick_flag = true;
                gap_time = gap_init_time;
				clone_parent.SetActive (true);

                GameObject clone_friend = GameObject.Instantiate(gameObject);
                
                clone_parent.GetComponent<Manage_Mouse_Drag>().flag = true;

                clone_friend.transform.parent = clone_parent.transform;
                clone_friend.GetComponent<Manage_Drag_Object>().enabled = false;
                clone_friend.GetComponent<UI2DSprite>().depth=15;
               clone_friend.GetComponent<SpringPosition>().enabled = false;
                clone_friend.transform.localScale = new Vector3(2f, -2f, 2f);
				clone_friend.GetComponent<Animator> ().enabled = true;
                clone_parent.GetComponent<Manage_Mouse_Drag>().drag_friend = clone_friend;

                friends_scroll.GetComponent<UIScrollView>().enabled = false;
            }
        }

    }

    void OnPress(bool isDown)
    {
        if (isDown)
        {
            down_flag = true;
            Debug.Log("Down");
        }
        else {
            down_flag = false;
            friends_scroll.GetComponent<UIScrollView>().enabled = true;
            longclick_flag = false;
            clone_parent.GetComponent<Manage_Mouse_Drag>().flag = false;

			if (clone_parent.GetComponent<Manage_Mouse_Drag>().match_group_id != -1 && !clone_parent.GetComponent<Manage_Mouse_Drag>().exist_flag)
            {
                clone_parent.GetComponent<Manage_Mouse_Drag>().groups[clone_parent.GetComponent<Manage_Mouse_Drag>().match_group_id].GetComponent<Manage_Members>().add_flag = true;
				clone_parent.GetComponent<Manage_Mouse_Drag>().groups [clone_parent.GetComponent<Manage_Mouse_Drag>().match_group_id].GetComponent<Manage_Each_Group> ().trigger_flag = 2;
				clone_parent.GetComponent<Manage_Mouse_Drag>().groups [clone_parent.GetComponent<Manage_Mouse_Drag>().match_group_id].GetComponent<Manage_Each_Group> ().flag = true;
				clone_parent.GetComponent<Manage_Mouse_Drag>().match_group_id = -1;
            }

            Destroy(clone_parent.GetComponent<Manage_Mouse_Drag>().drag_friend);
            gap_time = gap_init_time;

			clone_parent.SetActive (false);
            Debug.Log("Up");
        }
    }

    void OnDrag(Vector2 delta)
    {
        if (enabled && !longclick_flag)
        {
            down_flag = false;
            friends_scroll.GetComponent<UIScrollView>().disableDragIfFits = false;
            longclick_flag = false;
            clone_parent.GetComponent<Manage_Mouse_Drag>().flag = true;
            gap_time = gap_init_time;
            Debug.Log("Mouse Draged");
        }
    }

    void OnPan(Vector2 delta)
    {
        if (enabled)
        {
            Debug.Log("Mouse Pan");
        }
    }
}
