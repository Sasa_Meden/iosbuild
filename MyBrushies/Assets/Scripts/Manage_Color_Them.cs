﻿using UnityEngine;
using System.Collections;

public class Manage_Color_Them : MonoBehaviour {

    public Color output;
    public Color temp_output;

    public int type;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        string color_string;
        color_string = PlayerPrefs.GetString("th_Color");
        color_string = color_string.Replace("RGBA(", "");
        color_string = color_string.Replace(")", "");
        ParseColor(color_string, 0);

        if (output != null)
        {
            // this.GetComponent<UILabel>().color = output;
            switch (type)
            {
                case 0://label
                    UILabel lb = GetComponent<UILabel>();
                    if (lb != null)
                    {
                        lb.color = output;
                    }
                    break;
                case 1:
                    UI2DSprite obj = GetComponent<UI2DSprite>();
                    if (obj != null)
                    {
                        obj.color = output;
                    }
                    break;
                case 2:
                    UILabel lb2 = GetComponent<UILabel>();
                    if (lb2 != null)
                    {
                        lb2.color = output;
                    }
                    UI2DSprite obj2 = GetComponent<UI2DSprite>();
					if (obj2 != null && output!=null)
	                {
						output.a = obj2.color.a;
						obj2.color = output;
					}
                    break;
				case 6:
					UITexture tx = GetComponent<UITexture>();
					if (tx != null)
					{
						tx.color = output;
					}
					break;
				case 7:
					UIInput input = GetComponent<UIInput>();
					if (input != null)
					{
					input.activeTextColor = output;
					}
					break;
            }
        }

        string tmp_color_string;
        tmp_color_string = PlayerPrefs.GetString("temp_theme_Color");
        tmp_color_string = tmp_color_string.Replace("RGBA(", "");
        tmp_color_string = tmp_color_string.Replace(")", "");
        ParseColor(tmp_color_string, 1);

        if (temp_output != null)
        {
            // this.GetComponent<UILabel>().color = output;
            switch (type)
            {
                case 3://label
                    UILabel lb = GetComponent<UILabel>();
                    if (lb != null)
                    {
                        lb.color = temp_output;
                    }
                    break;
                case 4:
                    UI2DSprite obj = GetComponent<UI2DSprite>();
                    if (obj != null)
                    {
                        obj.color = temp_output;
                    }
                    break;
                case 5:
                    UILabel lb2 = GetComponent<UILabel>();
                    if (lb2 != null)
                    {
                        lb2.color = temp_output;
                    }
                    UI2DSprite obj2 = GetComponent<UI2DSprite>();
                    if (obj2 != null)
                    {
                        obj2.color = temp_output;
                    }
                    break;
            }
        }
    }

    public void ParseColor(string col, int t)
    {
         var strings = col.Split(","[0]);

         for (var i = 0; i< 4; i++) {
            if (t == 0)
            {
                output[i] = System.Single.Parse(strings[i]);
            }
            else{
                temp_output[i] = System.Single.Parse(strings[i]);
            }
        }
     }
}
