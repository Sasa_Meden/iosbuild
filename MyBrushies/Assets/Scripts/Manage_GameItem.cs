﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manage_GameItem : MonoBehaviour {

	public GameObject parent;
	public int index;
	public float pos_x;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		pos_x = transform.position.x;
		if (transform.position.x < (0.23f) && transform.position.x > (-0.23f)) {
			if (index != -1) {
				parent.GetComponent<Manage_HomeWindow> ().game_index = index;
				GetComponent<UI2DSprite> ().color =new Color(0.5f, 0.5f, 0.5f);
			} 
		} else {
			GetComponent<UI2DSprite> ().color = Color.white;
		}
	}
}
