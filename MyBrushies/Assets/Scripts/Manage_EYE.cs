﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manage_EYE : MonoBehaviour {

	public GameObject input_obj;
	public UILabel uilab;
	public int show_flag=1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (show_flag == 0) {
			GetComponent<UILabel> ().text = "HIDE";
			input_obj.GetComponent<UIInput> ().inputType = UIInput.InputType.Password;
		} else {
			GetComponent<UILabel> ().text = "SHOW";
			input_obj.GetComponent<UIInput> ().inputType = UIInput.InputType.Standard;
		}
	}

	public void click_bt(){
		if (show_flag == 1) {
			show_flag = 0;
			GetComponent<UILabel> ().text = "HIDE";
			input_obj.GetComponent<UIInput> ().inputType = UIInput.InputType.Password;
			input_obj.GetComponent<UILabel> ().text = input_obj.GetComponent<UIInput>().value;
		} else {
			show_flag = 1;
			GetComponent<UILabel> ().text = "SHOW";
			input_obj.GetComponent<UIInput> ().inputType = UIInput.InputType.Standard;
			input_obj.GetComponent<UILabel> ().text = input_obj.GetComponent<UIInput>().value;
		}

	}
}
