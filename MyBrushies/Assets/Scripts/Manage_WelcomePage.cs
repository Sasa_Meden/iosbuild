﻿using UnityEngine;

using UnityEngine.UI;

using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

using SimpleJSON;

public class Manage_WelcomePage : MonoBehaviour {

	public GameObject window_obj;
	public GameObject create_bt;
	public GameObject grid;
	public GameObject children;
	private int sex_counter;
	private int hand_counter;

	public GameObject loading;
	public GameObject email;
	public GameObject signup_bt;
	public GameObject phonenumber;
	public GameObject name;
	public GameObject signin_form;
	public GameObject[] child_sets;

	public int profile_counter=0;

	public GameObject back_bt;
	// Use this for initialization
	void Start () {
        Debug.Log("SCN WelcomePage");
        sex_counter = 1;
		hand_counter = 2;
		GetComponent<UI2DSprite> ().width = Screen.width;
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape) && (PlayerPrefs.GetString("current_page", "None")=="Welcome")) {
			PlayerPrefs.SetString ("current_page", "SignUp");
            PlayerPrefs.Save();
            UIPlayTween[] all_Components = back_bt.gameObject.GetComponents<UIPlayTween>();
			for (int i = 0; i < all_Components.Length; i++) {
				all_Components [i].enabled = true;
				all_Components [i].Play (true);
			}
		}
	}

	public void add_child(){
		GameObject new_child=(GameObject)GameObject.Instantiate(children, children.transform.localPosition, Quaternion.identity);

		new_child.transform.parent = grid.transform;
		new_child.transform.localScale = new Vector3(0.57f, 0.57f, 1f);
		children = new_child;
		sex_counter += 2;
		hand_counter += 2;
//		new_child.transform.Find ("Birthday_Month").Find("Label").GetComponent<UILabel> ().text = "MONTH";
//		new_child.transform.Find ("Birthday_Day").Find("Label").GetComponent<UILabel> ().text = "DAY";
//		new_child.transform.Find ("Birthday_Year").Find("Label").GetComponent<UILabel> ().text = "YEAR";
//		new_child.transform.Find ("input_nameofchild").GetComponent<UILabel> ().text = "NAME OF CHILD";
//		new_child.transform.Find ("input_nameofchild").Find("Label").gameObject.SetActive(false);
		new_child.transform.Find ("option_boy").GetComponent<UIToggle> ().group = sex_counter;
		new_child.transform.Find ("option_girl").GetComponent<UIToggle> ().group = sex_counter;

		new_child.transform.Find ("option_lefthand").GetComponent<UIToggle> ().group = hand_counter;
		new_child.transform.Find ("option_righthand").GetComponent<UIToggle> ().group = hand_counter;

		new_child.transform.Find ("bottom_line_short").gameObject.SetActive (false);
		new_child.transform.Find ("bottom_line_long").gameObject.SetActive (true);

		new_child.SetActive (true);
		grid.GetComponent<UIGrid> ().enabled = true;
	}

	public void goto_signup(){
        SceneManager.LoadScene("setting", LoadSceneMode.Single);
    }

	public void create_account(){
		string phone_txt = phonenumber.GetComponent<UIInput> ().value.Trim ();
		string name_txt = name.GetComponent<UIInput> ().value.Trim ();

		if(name_txt == "" || name_txt == "NAME"){
			UIStatus.Show (Localization.Get ("Name can not be empty!"));
		}else{
			StartCoroutine (WSignupRequest());	
		}
	}

	//first signup
	IEnumerator WSignupRequest() {
		loading.SetActive (true);
		string postScoreURL= Config.baseURL + "/v1/user";
		string email_str = PlayerPrefs.GetString ("email");
		string pwd = PlayerPrefs.GetString ("password");
		WWWForm form = new WWWForm();
		var requestBody = "{ \"user\": { \"email\": \""+email_str+"\", \"password\": \""+pwd+"\", \"phone_number\": \""+phonenumber.GetComponent<UIInput> ().value.Trim()+"\" }}";
        Debug.Log("PPP SignUpRequest " + requestBody);
        var encoding = new System.Text.UTF8Encoding();
		var postHeader = form.headers;
		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"]="application/json";

		WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
		yield return request;

		if (request.error != null)
		{
			loading.SetActive (false);
			if ( request.error== "422 Unprocessable Entity\r") {
				JSONNode jNodes = JSON.Parse (request.text);
                
                UIStatus.Show (Localization.Get (jNodes ["validation_errors"][0]));
			} else {
                JSONNode jNodes = JSON.Parse(request.text);
                string err = jNodes["validation_errors"][0];
             
                UIStatus.Show (Localization.Get (err));
                Debug.Log("PPP SignUpFAIL " + err);
            }

		}
		else
		{
			StartCoroutine(WSigninRequest());
			//signin_form.GetComponent<Manage_SigninPage> ().signup_success = 1;
			Debug.Log("PPP request success");
			Debug.Log("PPP returned data" + request.text);
		}
	}

	//second signin
	IEnumerator WSigninRequest() {
		loading.SetActive (true);
		string postScoreURL= Config.baseURL + "/oauth/token";
		string email_str = PlayerPrefs.GetString ("email");
		string pwd = PlayerPrefs.GetString ("password");
		WWWForm form = new WWWForm();
		var requestBody = "{ \"client_id\": \"e5d92e3f799987a9489b0edd6a00d78018f2abbf2a8aa98124d9f2d913e8e515\", \"client_secret\": \"4eb4a4532538864c6000f06f4fce5fc1e2d6fb12705cef5dc6891bdd68aa9eae\", \"grant_type\": \"password\", \"email\": \""+email_str+"\", \"password\": \""+pwd+"\", \"username\": \""+email_str+"\", \"scope\": \"mobile\" }";
		Debug.Log ("PPP SignInRequest "+requestBody);
		var encoding = new System.Text.UTF8Encoding();
		var postHeader = form.headers;
		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"]="application/json";

		WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
		yield return request;

		if (request.error != null)
		{
			loading.SetActive (false);

			Debug.Log("request error: " + request.error);
			Debug.Log("request errorstring: " + request.error.ToString());
			Debug.Log("request error: " + request.responseHeaders);

			if (request.error == "422 Unprocessable Entity") {
				JSONNode jNodes = JSON.Parse (request.text);
				UIStatus.Show (Localization.Get (jNodes ["validation_errors"]));
			} else {
				UIStatus.Show (Localization.Get ("SIGNIN FAILED!"));
				JSONNode jNodes = JSON.Parse (request.text);
				Debug.Log (jNodes);
			}
		}
		else
		{
			//loading.SetActive (false);
			JSONNode jNodes = JSON.Parse (request.text);
			PlayerPrefs.SetString ("token", jNodes["access_token"]);
			PlayerPrefs.SetString ("refresh_token", jNodes["refresh_token"]);
			PlayerPrefs.SetString ("expires_in", jNodes["expires_in"]);
            PlayerPrefs.Save();
            Create_Profiles ();
			//Application.LoadLevel("setting");
			Debug.Log("request success");
			Debug.Log("returned data" + request.text);
		}
	}

	private void Create_Profiles(){
		child_sets = GameObject.FindGameObjectsWithTag ("child");
		for (int i = 0; i < child_sets.Length; i++) {
			StartCoroutine (WCreateProfile_Request (child_sets[i]));
		}

	}

	//third create profiles while for all profiles, when done with creating, then go to next scene
	IEnumerator WCreateProfile_Request(GameObject child) {
		string postScoreURL= Config.baseURL + "/v1/profiles";
		string pwd = PlayerPrefs.GetString ("password");
		WWWForm form = new WWWForm();

		var encoding = new System.Text.UTF8Encoding();
		var postHeader = form.headers;
		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"]="application/json";
		postHeader["Authorization"]="Bearer "+PlayerPrefs.GetString("token");
		Debug.Log (postHeader);
		var requestBody="{\"profile\": {";
		string name=child.transform.FindChild("input_nameofchild").GetComponent<UIInput>().value;
		string date_month=child.transform.FindChild("input_monthofbirthday").GetComponent<UIInput>().value;
		string date_day=child.transform.FindChild("input_dayofbirthday").GetComponent<UIInput>().value;
		string date_year=child.transform.FindChild("input_yearofbirthday").GetComponent<UIInput>().value;
		string sex="male";
		if(child.transform.FindChild("option_girl").GetComponent<UIToggle>().value){
			sex="female";
		}
		if(child.transform.FindChild("option_boy").GetComponent<UIToggle>().value){
			sex="male";
		}
		string handed="right";
		if(child.transform.FindChild("option_lefthand").GetComponent<UIToggle>().value){
			handed="left";
		}
		if(child.transform.FindChild("option_righthand").GetComponent<UIToggle>().value){
			handed="right";
		}
        if (name == "")
        {
            UIStatus.Show(Localization.Get("Name Of Child field could not be empty!"));
        }
        else if (date_month == "")
        {
            UIStatus.Show(Localization.Get("Month of Birthday field could not be empty!"));
        }
        else if (date_day == "")
        {
            UIStatus.Show(Localization.Get("Day of Birthday field could not be empty!"));
        }
        else if (date_year == "")
        {
            UIStatus.Show(Localization.Get("Year of Birthday field could not be empty!"));
        }
        else
        {
            requestBody += "\"name\": \"" + name + "\", \"date_of_birth\": " + "\"" + date_year + "-" + date_month + "-" + date_day + "\", \"gender\": \"" + sex + "\", \"dominant_hand\": \"" + handed + "\" }}";
        }
       
        Debug.Log("PPP ProfileRequest " + requestBody);
        WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
		yield return request;

		if (request.error != null)
		{
			loading.SetActive (false);
			UIStatus.Show (Localization.Get ("SignUp Failed When Create Profile!"));

			Debug.Log("PPP request error: " + request.error);
			Debug.Log("PPP request error: " + request.responseHeaders);
		}
		else
		{
			
			profile_counter+=1;
			JSONNode jNodes = JSON.Parse (request.text);
			PlayerPrefs.SetString ("profile_id_"+profile_counter, jNodes["profile"]["id"]);
			PlayerPrefs.SetString ("profile_name_"+profile_counter, jNodes["profile"]["name"]);
			PlayerPrefs.SetString ("profile_birthday_"+profile_counter, jNodes["profile"]["date_of_birth"]);
			PlayerPrefs.SetString ("profile_gender_"+profile_counter, jNodes["profile"]["gender"]);
			PlayerPrefs.SetString ("profile_dominant_"+profile_counter, jNodes["profile"]["dominant_hand"]);
			PlayerPrefs.SetString ("profile_picture_"+profile_counter, jNodes["profile"]["picture"]);
			PlayerPrefs.SetInt("profile_counter", profile_counter);
            PlayerPrefs.Save();
            Debug.Log("PPP request success");
			Debug.Log("PPP returned data" + request.text);

            if (profile_counter == child_sets.Length){

                SceneManager.LoadScene("setting", LoadSceneMode.Single);
                // Application.LoadLevel ("setting");
            }
		}
	}
		
}
