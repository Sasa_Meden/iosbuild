﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class NV_Window : MonoBehaviour {

	public GameObject window;
	public GameObject close_bt;
	public GameObject loading;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void goto_home(){
		loading.SetActive (true);
        SceneManager.LoadScene("home", LoadSceneMode.Single);
        
	}
}
