﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.SceneManagement;
public class WebCamPhotoCamera : MonoBehaviour 
{
	WebCamTexture webCamTexture;

	IEnumerator Start() {
		yield return Application.RequestUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone);
		if (Application.HasUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone)) {
			webCamTexture = new WebCamTexture();
			GetComponent<Renderer> ().material.mainTexture = webCamTexture;
			//GetComponent<UI2DSprite> ().material.mainTexture= webCamTexture;
			//renderer.material.mainTexture = webCamTexture;
			webCamTexture.Play();
		} else {
		}
	}
		
	public void photo_click(){
		Debug.Log (System.DateTime.Now.ToFileTime());
		Debug.Log (PlayerPrefs.GetInt ("selected_tkphoto_index", -5));
		StartCoroutine (TakePhoto ());
	}

	IEnumerator TakePhoto()
	{

		// NOTE - you almost certainly have to do this here:

		yield return new WaitForEndOfFrame(); 

		// it's a rare case where the Unity doco is pretty clear,
		// http://docs.unity3d.com/ScriptReference/WaitForEndOfFrame.html
		// be sure to scroll down to the SECOND long example on that doco page 

		int selected_index = PlayerPrefs.GetInt ("selected_tkphoto_index", -1);

		Texture2D photo = new Texture2D(webCamTexture.width, webCamTexture.height);
		photo.SetPixels(webCamTexture.GetPixels());
		photo.Apply();

		//Encode to a PNG
		byte[] bytes = photo.EncodeToPNG();

		//photo directory path
		string path=Application.persistentDataPath+"/photos";
		if (!Directory.Exists(path))
		{
			Directory.CreateDirectory(path);
		}

		//Write out the PNG. Of course you have to substitute your_path for something sensible
		string photo_path=path+"/photo_"+System.DateTime.Now.ToFileTime()+".png";
		File.WriteAllBytes(photo_path, bytes);

		if (selected_index != -1) {
			PlayerPrefs.SetString ("selected_tkphoto_link_" + selected_index, photo_path);
            PlayerPrefs.Save();
        }

		//On setting scene, this will hide the All_Set Window
		PlayerPrefs.SetInt ("setting_flag", 1);
        PlayerPrefs.Save();
        SceneManager.LoadScene("setting", LoadSceneMode.Single);
    }
}
