﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIResponsive : MonoBehaviour {

	public GameObject header;
	public GameObject body;
	public int pos_type;
	public bool flag;
	// Use this for initialization
	void Start () {
		float s_width = Screen.width*1920/Screen.height;
//		if (s_width > 1080) {
			if (pos_type == 0) {
				if(!flag)	
					transform.localPosition = new Vector3 (0.0f, 0f, 0f);
				GetComponent<TweenPosition> ().from = new Vector3 (0f, 0f, 0f);
				GetComponent<TweenPosition> ().to = new Vector3 (-s_width, 0f, 0f);
			} else if (pos_type == 1) {
				if (!flag) {
					transform.localPosition = new Vector3 (s_width, 0f, 0f);
					GetComponent<TweenPosition> ().to = new Vector3 (0f, 0f, 0f);
					GetComponent<TweenPosition> ().from = new Vector3 (s_width, 0f, 0f);
				} else {
					GetComponent<TweenPosition> ().from = new Vector3 (0f, 0f, 0f);
					GetComponent<TweenPosition> ().to = new Vector3 (s_width, 0f, 0f);
				}
			} else if (pos_type == -1) {
				if(!flag)
					transform.localPosition = new Vector3 (-s_width, 0f, 0f);
				GetComponent<TweenPosition> ().to = new Vector3 (0f, 0f, 0f);
				GetComponent<TweenPosition> ().from = new Vector3 (-s_width, 0f, 0f);
			}
			GetComponent<UIWidget> ().width = (int)s_width;
//		} else {
//			if (pos_type == 0) {
//				transform.localPosition = new Vector3 (0.0f, 0f, 0f);
//				GetComponent<TweenPosition> ().from = new Vector3 (0f, 0f, 0f);
//				GetComponent<TweenPosition> ().to = new Vector3 (-1080f, 0f, 0f);
//			} else if (pos_type == 1) {
//				transform.localPosition = new Vector3 (1080f, 0f, 0f);
//				GetComponent<TweenPosition> ().to = new Vector3 (0f, 0f, 0f);
//				GetComponent<TweenPosition> ().from = new Vector3 (1080f, 0f, 0f);
//			} else if (pos_type == -1) {
//				transform.localPosition = new Vector3 (-1080f, 0f, 0f);
//				GetComponent<TweenPosition> ().to = new Vector3 (0f, 0f, 0f);
//				GetComponent<TweenPosition> ().from = new Vector3 (-1080f, 0f, 0f);
//			}
//		}
//		GetComponent<UIWidget> ().height = (int)Screen.height;
//		header.GetComponent<UI2DSprite> ().width = (int)s_width;
//		body.GetComponent<UI2DSprite> ().width = (int)s_width;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
