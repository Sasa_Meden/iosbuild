﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manage_InputBox : MonoBehaviour {

	public GameObject title_label;
	public int type=0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (type == 0) {
			if (transform.GetComponent<UIInput> ().value == "") {
				title_label.SetActive (false);
			} else {
				title_label.SetActive (true);
			}
		} else if (type == 1) {//optional phone number field
			string val=transform.GetComponent<UILabel>().text;
			transform.GetComponent<UILabel>().text=string.Format("{0:(###) ###-####}", val);
		}
	}
}
