﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class Manage_Device_Test : MonoBehaviour
{

	////CONNECTION/STATUS TEXT/////
	public string infoText;
	/////GYRO///////////
	public string GyroX;
	public string GyroY;
	public string GyroZ;

	/////MAGNETOMETER/////////
	public string MagX;
	public string MagY;
	public string MagZ;

	/////ACCELEROMETER////////
	public string AccX;
	public string AccY;
	public string AccZ;

	////TIME TICS IN ms///////
	public string TS;
	////DEVICE MAC///////
	public string MACAddress;
	////Flag for update when btnStart or btnEnd  is clicked/////
	public bool started = false;

	////Tooth Brush Script / controler/////
	private PurpleToothbrush purple;

	public GameObject start_bt;
	public GameObject stop_bt;
	public GameObject[] _gyros;
	public GameObject[] _magns;
	public GameObject[] _acces;
	public GameObject info_text;

	public GameObject[] brushies_states;
	public GameObject[] better_brushies_states;
	public GameObject[] jaw_states;
	public GameObject[] side_states;
	public GameObject[] surface_out_states;
	public GameObject[] surface_in_states;
	public GameObject[] surface_flat_states;

	public GameObject ts_obj;

	private string[][] dummy_test_data;
	private string[][] _bData;
	private string[][] _betterData;
	private int _bCounter;
	public int _idCounter;

	public bool brush_flag;
	public bool better_brush_flag;
	public bool jaw_flag;
	public bool side_flag;
	public bool surface_flag;
	public bool testing_flag;

	public GameObject _brush_start_bt;
	public GameObject _brush_stop_bt;
	public GameObject _better_brush_start_bt;
	public GameObject _better_brush_stop_bt;
	public GameObject _jaw_start_bt;
	public GameObject _jaw_stop_bt;
	public GameObject _side_start_bt;
	public GameObject _side_stop_bt;
	public GameObject _surface_start_bt;
	public GameObject _surface_stop_bt;
	public GameObject _testing_start_bt;
	public GameObject _testing_stop_bt;

	public int success_counter;
	public int error_counter;
	public int _iTiming;
	public float _fTiming;

	public GameObject timing_label;
	public GameObject success_label;
	public GameObject error_label;

	private float _updateTime;

	// Use this for initialization
	void Start()
	{
        Debug.Log("SCN DeviceTest");
        success_counter = 0;
		error_counter = 0;
		_iTiming = 0;
		_fTiming = 0f;

		dummy_test_data = new string[5][];
		dummy_test_data[0] = new string[] { "TS.1", "9.719", "-0.086", "-5.58", "-142.352", "-42.836", "-126.625", "26.7", "-34.8", "17.1" };
		dummy_test_data[1] = new string[] { "TS.2", "4.658", "-6.233", "5.061", "28.344", "-12.539", "-28.32", "3.1", "-41.1", "26.1" };
		dummy_test_data[2] = new string[] { "TS.3", "-6.886", "-4.658", "5.762", "30.203", "53.641", "-7.602", "-42.2", "-2.2", "28.5" };
		dummy_test_data[3] = new string[] { "TS.4", "-9.623", "0.346", "-6.473", "-47.047", "-2.82", "41.844", "-38.7", "-8.1", "-16" };
		dummy_test_data[4] = new string[] { "TS.5", "6.752", "-9.604", "3.918", "7.742", "15.589", "122.539", "29.6", "-34.4", "13.9" };

		_bData = new string[5][];
		_betterData = new string[5][];
		_bCounter = 0;
		_idCounter = 0;
		_updateTime = 0.04f;
	}

	// Update is called once per frame
	void Update()
	{

		if (testing_flag)
		{
			_fTiming += Time.deltaTime;
			_iTiming = (int)_fTiming;
		}
		timing_label.GetComponent<UILabel>().text = "TIMING: " + _iTiming;
		success_label.GetComponent<UILabel>().text = "SUCCESS: " + success_counter;
		error_label.GetComponent<UILabel>().text = "ERROR: " + error_counter;

		_updateTime -= Time.deltaTime;
		if (_updateTime <= 0.0f) {
			//Dummy_Test ();
			Actual_Test();///This runs every 40 ms
			_updateTime=0.04f;
		}

	}

	void Actual_Test()
	{
        if (started)
        {
            Debug.Log("GGG DeviceTestUpdate");

		

			if (PurpleContainer.instance.bleState!= PurpleToothbrush.CONNECTED)
			{
				infoText = PurpleContainer.instance.GetLog();
				info_text.GetComponent<UILabel>().text = infoText;
			}
			else
			{
                /////HERE WE PICK UP DATA FROM DEVICE LIKE IN MY PREVIOUS EXAMPLE///////////////////
                var sensorReading = PurpleContainer.instance.GetLastReading();

                MACAddress = PurpleContainer.instance.GetConnectedMAC();
                TS = sensorReading.tickStamp.ToString();
                GyroX = sensorReading.gyro_x.ToString();
                GyroY = sensorReading.gyro_y.ToString();
                GyroZ = sensorReading.gyro_z.ToString();
                MagX = sensorReading.mag_x.ToString();
                MagY = sensorReading.mag_y.ToString();
                MagZ = sensorReading.mag_z.ToString();
                AccX = (sensorReading.acc_x * 10.0f).ToString();
                AccY = (sensorReading.acc_y * 10.0f).ToString();
                AccZ = (sensorReading.acc_z * 10.0f).ToString();
                Debug.Log("VVV TS: " + TS + " " + GyroX + " " + GyroY + " " + GyroZ + " "
                    + MagX + " " + MagY + " " + MagZ + " "
                    + AccX + " " + AccY + " " + AccZ + " " + _bCounter);
           
                
                ///////DATA HAS BEEN STORED IN VARIABLES////////////////////////////////////
                ///////YOU WUOLD FORMAT THIS DATA (IN JSON PROBABLY) AND STREAM IT AS YOU SEE FIT HERE///////////////////////
                    
                
                
                
         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////       
                ////Show MAC adresss so we know we are connected (this works)
                info_text.GetComponent<UILabel>().text = MACAddress;
            ////////////HERE JOAO SETS AND SENDS DATA IN PACKETS OF 5////////////////////////////////
                if (_bCounter < 5)
				{
					_gyros[0].GetComponent<UILabel>().text = GyroX;
					_gyros[1].GetComponent<UILabel>().text = GyroY;
					_gyros[2].GetComponent<UILabel>().text = GyroZ;
					_magns[0].GetComponent<UILabel>().text = MagX;
					_magns[1].GetComponent<UILabel>().text = MagY;
					_magns[2].GetComponent<UILabel>().text = MagZ;
					_acces[0].GetComponent<UILabel>().text = AccX;
					_acces[1].GetComponent<UILabel>().text = AccY;
					_acces[2].GetComponent<UILabel>().text = AccZ;


					_bData[_bCounter] = new string[10];
					_bData[_bCounter][0] = "" + _idCounter;
					_bData[_bCounter][1] = GyroX;
					_bData[_bCounter][2] = GyroY;
					_bData[_bCounter][3] = GyroZ;
					_bData[_bCounter][4] = MagX;
					_bData[_bCounter][5] = MagY;
					_bData[_bCounter][6] = MagZ;
					_bData[_bCounter][7] = AccX;
					_bData[_bCounter][8] = AccY;
					_bData[_bCounter][9] = AccZ;

					_betterData[_bCounter] = new string[7];
					_betterData[_bCounter][0] = "" + _idCounter;
					_betterData[_bCounter][1] = GyroX;
					_betterData[_bCounter][2] = GyroY;
					_betterData[_bCounter][3] = GyroZ;
					_betterData[_bCounter][4] = AccX;
					_betterData[_bCounter][5] = AccY;
					_betterData[_bCounter][6] = AccZ;

					_bCounter++;

				}
				else
				{
					StartCoroutine(WConnectedAPI(0, _bData));
					StartCoroutine(WConnectedAPI(1, _bData));
					StartCoroutine(WConnectedAPI(2, _bData));
					StartCoroutine(WConnectedAPI(3, _bData));
					StartCoroutine(WConnectedAPI(4, _betterData));
					_bCounter = 0;
				}
				_idCounter++;
			}


		}
		else
		{
			_idCounter = 0;
			info_text.GetComponent<UILabel>().text = "Stoped device scan...";
			for (int i = 0; i < 3; i++)
			{
				_gyros[i].GetComponent<UILabel>().text = "...";
				_magns[i].GetComponent<UILabel>().text = "...";
				_acces[i].GetComponent<UILabel>().text = "...";

				surface_out_states[i].GetComponent<UILabel>().text = "...";
				surface_in_states[i].GetComponent<UILabel>().text = "...";
				surface_flat_states[i].GetComponent<UILabel>().text = "...";
			}

			for (int j = 0; j < 2; j++)
			{
				brushies_states[j].GetComponent<UILabel>().text = "...";
				side_states[j].GetComponent<UILabel>().text = "...";
				jaw_states[j].GetComponent<UILabel>().text = "...";
				better_brushies_states[j].GetComponent<UILabel>().text = "...";
			}
		}
	}

	void Dummy_Test()
	{
		if (started)
		{
			if (_bCounter < 5)
			{

				GyroX = "" + Random.Range(-20.0f, 20.0f);
				GyroY = "" + Random.Range(-20.0f, 20.0f);
				GyroZ = "" + Random.Range(-20.0f, 20.0f);
				_gyros[0].GetComponent<UILabel>().text = GyroX;
				_gyros[1].GetComponent<UILabel>().text = GyroY;
				_gyros[2].GetComponent<UILabel>().text = GyroZ;

				MagX = "" + Random.Range(-20.0f, 20.0f);
				MagY = "" + Random.Range(-20.0f, 20.0f);
				MagZ = "" + Random.Range(-20.0f, 20.0f);
				_magns[0].GetComponent<UILabel>().text = MagX;
				_magns[1].GetComponent<UILabel>().text = MagY;
				_magns[2].GetComponent<UILabel>().text = MagZ;

				AccX = "" + Random.Range(-20.0f, 20.0f);
				AccY = "" + Random.Range(-20.0f, 20.0f);
				AccZ = "" + Random.Range(-20.0f, 20.0f);
				_acces[0].GetComponent<UILabel>().text = AccX;
				_acces[1].GetComponent<UILabel>().text = AccY;
				_acces[2].GetComponent<UILabel>().text = AccZ;

				_bData[_bCounter] = new string[10];
				_bData[_bCounter][0] = "" + _idCounter;
				_bData[_bCounter][1] = GyroX;
				_bData[_bCounter][2] = GyroY;
				_bData[_bCounter][3] = GyroZ;
				_bData[_bCounter][4] = MagX;
				_bData[_bCounter][5] = MagY;
				_bData[_bCounter][6] = MagZ;
				_bData[_bCounter][7] = AccX;
				_bData[_bCounter][8] = AccY;
				_bData[_bCounter][9] = AccZ;

				_betterData[_bCounter] = new string[7];
				_betterData[_bCounter][0] = "" + _idCounter;
				_betterData[_bCounter][1] = GyroX;
				_betterData[_bCounter][2] = GyroY;
				_betterData[_bCounter][3] = GyroZ;
				_betterData[_bCounter][4] = AccX;
				_betterData[_bCounter][5] = AccY;
				_betterData[_bCounter][6] = AccZ;

				_bCounter++;
			}
			else
			{
				StartCoroutine(WConnectedAPI(0, _bData));
				StartCoroutine(WConnectedAPI(4, _betterData));
				StartCoroutine(WConnectedAPI(1, _bData));
				StartCoroutine(WConnectedAPI(2, _bData));
				StartCoroutine(WConnectedAPI(3, _bData));
				_bCounter = 0;
			}
			_idCounter++;
		}
		else
		{
			_idCounter = 0;
			info_text.GetComponent<UILabel>().text = "Stoped device scan...";
			for (int i = 0; i < 3; i++)
			{
				_gyros[i].GetComponent<UILabel>().text = "...";
				_magns[i].GetComponent<UILabel>().text = "...";
				_acces[i].GetComponent<UILabel>().text = "...";

				surface_out_states[i].GetComponent<UILabel>().text = "...";
				surface_in_states[i].GetComponent<UILabel>().text = "...";
				surface_flat_states[i].GetComponent<UILabel>().text = "...";
			}

			for (int j = 0; j < 2; j++)
			{
				brushies_states[j].GetComponent<UILabel>().text = "...";
				side_states[j].GetComponent<UILabel>().text = "...";
				jaw_states[j].GetComponent<UILabel>().text = "...";
				better_brushies_states[j].GetComponent<UILabel>().text = "...";
			}
		}
	}

	IEnumerator WConnectedAPI(int type, string[][] values)
	{

		WWWForm form = new WWWForm();
		string value_str = "";
		for (int i = 0; i < values.Length; i++)
		{
			value_str += "[";
			for (int j = 0; j < values[i].Length; j++)
			{
				value_str += "\"" + values[i][j] + "\",";
			}
			value_str = value_str.Remove(value_str.Length - 1);
			value_str += "],";
		}
		value_str = value_str.Remove(value_str.Length - 1);
		var requestBody = "{" +
			"\"Inputs\": {" +
			"\"input\": {" +
			"\"ColumnNames\": [" +
			"\"ID\", \"gyroX\", \"gyroY\", \"gyroZ\", \"magntoX\", \"magntoY\", \"magntoZ\", \"accX\", \"accY\", \"accZ\"" +
			"]," +
			"\"Values\": [" + value_str + "]," +
			"\"GlobalParameters\": {}" +
			"}" +
			"}" +
			"}";

		string postURL = "";
		string apiKEY = "";
		if (type == 0)
		{//Brushing or not
			postURL = "https://ussouthcentral.services.azureml.net/workspaces/6180c4e0ad88428dbbcfb19a592771c7/services/480b58b1f5dd4b36b32b269d681363ea/execute?api-version=2.0&details=true";
			apiKEY = "xjDELZVVVVytx/CVrBx8g/GtFoN0RF1919Nl4OLT488nyvZizFd6Uw49NvlsWYUD6LxvPoa2uxC8xn17Vv45HQ==";
		}
		else if (type == 1)
		{//Jaw, upper or lower
			postURL = "https://ussouthcentral.services.azureml.net/workspaces/6180c4e0ad88428dbbcfb19a592771c7/services/11ec0b7021304fef896ada58e38b9be9/execute?api-version=2.0&details=true";
			apiKEY = "Bcz2tOZw0SbJi5xf1+ChDAfDhVWvT7KlDfiMl40qYcOUjjfLdYx6ZQKs81Kf1nO9YBncR9zNxHYoC2vPok1Eaw==";
		}
		else if (type == 2)
		{//Side, left or right
			postURL = "https://ussouthcentral.services.azureml.net/workspaces/6180c4e0ad88428dbbcfb19a592771c7/services/a486d88accfd49d080109fdf77628d0b/execute?api-version=2.0&details=true";
			apiKEY = "vBdtz/8w9kOJrz8SRKJseXesXEgiSLMhiAz+cRIqLylcFUPY5ctYTbojwMk3UVpNR7f931aIbNkxs51wLgFO1w==";
		}
		else if (type == 3)
		{//Surface, inside, outside or flat
			postURL = "https://ussouthcentral.services.azureml.net/workspaces/6180c4e0ad88428dbbcfb19a592771c7/services/5944e677f3d2420185b0c31ba176bd9b/execute?api-version=2.0&details=true";
			apiKEY = "we4wjyKqJU4oN0eLYA0+Y0rh2fzAtPRGEW2KuLk15tS+ZxcaBmrnqXywGfTKUBq/tkEXlsT4Ue7VgNQrOw6E9Q==";
		}
		else
		{//Better Brushing or not
			postURL = "https://ussouthcentral.services.azureml.net/workspaces/6180c4e0ad88428dbbcfb19a592771c7/services/0ab9f1e8abf54a20b5e5cda71ed91717/execute?api-version=2.0&details=true";
			apiKEY = "aCzTcI8SBwX9CmHZMsNWb+ao6a68mj8/AI7gizlcwkfs6sR4rLY5BCQO+ieQO39DDeGV6epSHhIU/XItkcJrKQ==";
			requestBody = "{" +
				"\"Inputs\": {" +
				"\"input\": {" +
				"\"ColumnNames\": [" +
				"\"ID\", \"gyroX\", \"gyroY\", \"gyroZ\", \"accX\", \"accY\", \"accZ\"" +
				"]," +
				"\"Values\": [" + value_str + "]," +
				"\"GlobalParameters\": {}" +
				"}" +
				"}" +
				"}";
		}

		Debug.Log(requestBody);
		var encoding = new System.Text.UTF8Encoding();
		var postHeader = form.headers;

		//		postHeader ["Content-Length"] = ""+byte_content.Length;
		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"] = "application/json";
		postHeader["Authorization"] = "Bearer " + apiKEY;

		WWW request = new WWW(postURL, encoding.GetBytes(requestBody), postHeader);
		yield return request;
		Debug.Log("VVVC: REQUEST " + _bCounter);
		if (request.error != null)
		{
			//UIStatus.Show (Localization.Get ("Signin Failed!"));

			Debug.Log("request error: " + request.error);
			Debug.Log("request error: " + request.responseHeaders);
		}
		else
		{

			Debug.Log("request success");
			Debug.Log("returned data" + request.text);
			JSONNode jNodes = JSON.Parse(request.text);
			JSONNode vals;

			if (started)
			{
				if (type == 0)
				{//Brushing or not
					vals = jNodes["Results"]["Brush or not"]["value"]["Values"];
					for (int i = 0; i < vals.Count; i++)
					{
						if (vals[i][1].Value == "No brush")
						{
							brushies_states[1].GetComponent<UILabel>().text = "" + vals[i][2];
							if (testing_flag)
							{
								if (!brush_flag)
								{
									success_counter++;
								}
								else
								{
									error_counter++;
								}
							}
						}
						else
						{
							brushies_states[0].GetComponent<UILabel>().text = "" + vals[i][2];
							if (testing_flag)
							{
								if (brush_flag)
								{
									success_counter++;
								}
								else
								{
									error_counter++;
								}
							}
						}
					}
				}
				else if (type == 1)
				{//Jaw, upper or lower
					vals = jNodes["Results"]["Jaw"]["value"]["Values"];
					for (int i = 0; i < vals.Count; i++)
					{
						if (vals[i][1].Value == "Lower jaw")
						{
							jaw_states[1].GetComponent<UILabel>().text = "" + vals[i][2];
							if (testing_flag)
							{
								if (!jaw_flag)
								{
									success_counter++;
								}
								else
								{
									error_counter++;
								}
							}
						}
						else
						{
							jaw_states[0].GetComponent<UILabel>().text = "" + vals[i][2];
							if (testing_flag)
							{
								if (jaw_flag)
								{
									success_counter++;
								}
								else
								{
									error_counter++;
								}
							}
						}

					}
				}
				else if (type == 2)
				{//Side, left or right
					vals = jNodes["Results"]["Side"]["value"]["Values"];
					for (int i = 0; i < vals.Count; i++)
					{
						if (vals[i][1].Value == "Left side")
						{
							side_states[0].GetComponent<UILabel>().text = "" + vals[i][2];
							if (testing_flag)
							{
								if (!side_flag)
								{
									success_counter++;
								}
								else
								{
									error_counter++;
								}
							}
						}
						else
						{
							side_states[1].GetComponent<UILabel>().text = "" + vals[i][2];
							if (testing_flag)
							{
								if (side_flag)
								{
									success_counter++;
								}
								else
								{
									error_counter++;
								}
							}
						}
					}
				}
				else if (type == 3)
				{//Surface, inside, outside or flat
					vals = jNodes["Results"]["Surface"]["value"]["Values"];
					Debug.Log("Surface Count:" + vals.Count);
					for (int i = 0; i < vals.Count; i++)
					{
						Debug.Log("Surface Vals:" + vals[i][3]);
						if (vals[i][4].Value == "Outside")
						{
							surface_out_states[0].GetComponent<UILabel>().text = "" + vals[i][1];
							surface_out_states[1].GetComponent<UILabel>().text = "" + vals[i][2];
							surface_out_states[2].GetComponent<UILabel>().text = "" + vals[i][3];
							if (testing_flag)
							{
								if (surface_flag)
								{
									success_counter++;
								}
								else
								{
									error_counter++;
								}
							}
						}
						else if (vals[i][4].Value == "Inside")
						{
							surface_in_states[0].GetComponent<UILabel>().text = "" + vals[i][1];
							surface_in_states[1].GetComponent<UILabel>().text = "" + vals[i][2];
							surface_in_states[2].GetComponent<UILabel>().text = "" + vals[i][3];
							if (testing_flag)
							{
								if (!surface_flag)
								{
									success_counter++;
								}
								else
								{
									error_counter++;
								}
							}
						}
						else
						{
							surface_flat_states[0].GetComponent<UILabel>().text = "" + vals[i][1];
							surface_flat_states[1].GetComponent<UILabel>().text = "" + vals[i][2];
							surface_flat_states[2].GetComponent<UILabel>().text = "" + vals[i][3];
						}
					}
				}
				else
				{//Better Brushing or not
					vals = jNodes["Results"]["output"]["value"]["Values"];
					for (int i = 0; i < vals.Count; i++)
					{
						if (vals[i][1].Value == "No brush")
						{
							better_brushies_states[1].GetComponent<UILabel>().text = "" + vals[i][2];
							if (testing_flag)
							{
								if (!better_brush_flag)
								{
									success_counter++;
								}
								else
								{
									error_counter++;
								}
							}
						}
						else
						{
							better_brushies_states[0].GetComponent<UILabel>().text = "" + vals[i][2];
							if (testing_flag)
							{
								if (better_brush_flag)
								{
									success_counter++;
								}
								else
								{
									error_counter++;
								}
							}
						}
					}
				}
			}
			//Application.LoadLevel ("setting");
			//StartCoroutine(WGetProfileRequest());
		}
	}
	public void _start()
	{
		start_bt.SetActive(false);
		stop_bt.SetActive(true);
		started = false;
       // purple.StopPurpleToothBrush();
       // purple = null;
	}

	public void _stop()
	{
		start_bt.SetActive(true);
		stop_bt.SetActive(false);
		started = true;
	}

	public void _Brush_start()
	{
		_brush_start_bt.SetActive(false);
		_brush_stop_bt.SetActive(true);
		brush_flag = false;
	}

	public void _Brush_stop()
	{
		_brush_start_bt.SetActive(true);
		_brush_stop_bt.SetActive(false);
		brush_flag = true;
	}

	public void _Better_Brush_start()
	{
		_better_brush_start_bt.SetActive(false);
		_better_brush_stop_bt.SetActive(true);
		better_brush_flag = false;
	}

	public void _Better_Brush_stop()
	{
		_better_brush_start_bt.SetActive(true);
		_better_brush_stop_bt.SetActive(false);
		better_brush_flag = true;
	}

	public void _Jaw_start()
	{
		_jaw_start_bt.SetActive(false);
		_jaw_stop_bt.SetActive(true);
		jaw_flag = false;
	}

	public void _Jaw_stop()
	{
		_jaw_start_bt.SetActive(true);
		_jaw_stop_bt.SetActive(false);
		jaw_flag = true;
	}

	public void _Side_start()
	{
		_side_start_bt.SetActive(false);
		_side_stop_bt.SetActive(true);
		side_flag = false;
	}

	public void _Side_stop()
	{
		_side_start_bt.SetActive(true);
		_side_stop_bt.SetActive(false);
		side_flag = true;
	}

	public void _Surface_start()
	{
		_surface_start_bt.SetActive(false);
		_surface_stop_bt.SetActive(true);
		surface_flag = false;
	}

	public void _Surface_stop()
	{
		_surface_start_bt.SetActive(true);
		_surface_stop_bt.SetActive(false);
		surface_flag = true;
	}

	public void _Test_start()
	{
		_testing_start_bt.SetActive(false);
		_testing_stop_bt.SetActive(true);
		testing_flag = false;

	}

	public void _Test_stop()
	{
		_testing_start_bt.SetActive(true);
		_testing_stop_bt.SetActive(false);
		testing_flag = true;
		_fTiming = 0f;
		_iTiming = 0;
		success_counter = 0;
		error_counter = 0;
	}
}