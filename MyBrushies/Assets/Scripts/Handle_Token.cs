﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class Handle_Token : MonoBehaviour {

	public float time=7200f;
	private string baseURL="http://pearly01.mybrushies.com";
	public string token_str;
	// Use this for initialization
	void Start () {
		time = 7200f;
	}
	
	// Update is called once per frame
	void Update () {
		time -= Time.deltaTime;
		if (time < 0) {
			StartCoroutine (WRefreshToken ());
		}
	}

	IEnumerator WRefreshToken() {
		string postScoreURL= baseURL + "/oauth/token";
		WWWForm form = new WWWForm();
		var requestBody = "{ \"client_id\": \"e5d92e3f799987a9489b0edd6a00d78018f2abbf2a8aa98124d9f2d913e8e515\", \"client_secret\": \"4eb4a4532538864c6000f06f4fce5fc1e2d6fb12705cef5dc6891bdd68aa9eae\", \"grant_type\": \"refresh_token\", \"refresh_token\": \""+PlayerPrefs.GetString("refresh_token")+"\" }";
		var encoding = new System.Text.UTF8Encoding();
		var postHeader = form.headers;

		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"]="application/json";

		WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
		yield return request;

		if (request.error != null)
		{
			Debug.Log("request error: " + request.error);
			Debug.Log("request error: " + request.responseHeaders);
		}
		else
		{
			JSONNode jNodes = JSON.Parse (request.text);
			token_str = jNodes ["access_token"];
			PlayerPrefs.SetString ("token", jNodes["access_token"]);
			PlayerPrefs.SetString ("refresh_token", jNodes["refresh_token"]);
			PlayerPrefs.SetString ("expires_in", jNodes["expires_in"]);
            PlayerPrefs.Save();
			time = 7200f;
			Debug.Log("request success");
			Debug.Log("returned data" + request.text);
		}
	}
}
