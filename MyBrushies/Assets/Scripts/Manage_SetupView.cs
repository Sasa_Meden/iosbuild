﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manage_SetupView : MonoBehaviour {

	public GameObject[] activies;
	public int index;
	public Vector3 localposition;
	public bool active_flag;
	// Use this for initialization
	void Start () {
		active_flag = false;
	}
	
	// Update is called once per frame
	void Update () {
		localposition = transform.position;
		//-0.01144046
		if (transform.position.x <(-0.01f) && transform.position.x >(-0.012f) && !active_flag) {
			active_flag=true;
			for (int i = 0; i < activies.Length; i++) {
				activies [i].SetActive (false);
			}
			activies [index].SetActive (true);
		}

		if ((transform.position.x >= (-0.01f) || transform.position.x <= (-0.012f)) && active_flag) {
			active_flag = false;
		}
	}
}
