﻿using UnityEngine;
using System.Collections;

public class Manage_Color_Tile : MonoBehaviour {

    public GameObject border;
    public Color color;
    public GameObject[] other_borders;
	// Use this for initialization
	void Start () {
        if (PlayerPrefs.GetString("temp_theme_Color") != "" && PlayerPrefs.GetString("temp_theme_Color") == ""+color)
        {
            border.SetActive(true);
        }
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    public void ButtonClick()
    {
        if (border.activeSelf)
        {
            //border.active = false;
        }else
        {
            for(int i=0; i<other_borders.Length; i++)
            {
                other_borders[i].SetActive(false);
                PlayerPrefs.SetString("temp_theme_Color", ""+color);
                PlayerPrefs.Save();
            }
            border.SetActive(true);
        }
    }
}
