﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manage_PrizeWindow : MonoBehaviour {
	public float time=3f;
	public GameObject this_obj;
	public GameObject map_obj;
	public bool init_flag;
	public GameObject hope_window;
	public GameObject _window;
	// Use this for initialization
	void Start () {
		Screen.orientation = ScreenOrientation.Portrait;
		int random = Random.Range (1, 10);
		if (random % 3 == 0) {
			hope_window.transform.position = new Vector3 (0f, 0f, 0f);	
			hope_window.GetComponent<UIResponsive> ().flag = true;
			_window.SetActive (false);
		} else {
			init_flag = true;	
		}
	}
	
	// Update is called once per frame
	void Update () {

      //  Debug.Log("CCCC ManagePrizeWindow");

        time -= Time.deltaTime;
		if (time < 0 && init_flag) {
			this_obj.GetComponent<UIPlayTween> ().enabled = true;
			this_obj.GetComponent<UIPlayTween> ().Play (true);
			map_obj.GetComponent<UIPlayTween> ().enabled = true;
			map_obj.GetComponent<UIPlayTween> ().Play (true);
			init_flag = false;
		}
	}

}
