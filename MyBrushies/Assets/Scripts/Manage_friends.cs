﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.IO;

public class Manage_friends : MonoBehaviour {

    //define public variables
    public GameObject clone_row;
    public GameObject clone_friend;

    public GameObject[] friends;
    public GameObject[] friends_rows;
    int friends_count = 0;
    int pos_count = 0;

    // Use this for initialization
    void Start () {
		PlayerPrefs.SetInt ("friends_counter", 0);
		PlayerPrefs.SetInt ("friends_loaded", 0);
        PlayerPrefs.Save();
        //string friends_str = Read();
        StartCoroutine(AnalyticsJSON());
    }

    //analytics json
    public IEnumerator AnalyticsJSON()
    {
        WWW file = new WWW("http://dev999.com/higherstar/friends.json");
        yield return file;

        JSONNode friend_data = JSON.Parse(file.text);
        friends = new GameObject[friend_data["friends"].Count];
        friends_count = friend_data["friends"].Count;
        friends_rows = new GameObject[getRowCount()];

        for (int j = 0; j < getRowCount(); j++)
        {
            friends_rows[j] = GameObject.Instantiate(clone_row);
            friends_rows[j].transform.parent = transform;

            friends_rows[j].transform.localScale = new Vector3(1, 1, 1);
            friends_rows[j].transform.localPosition = new Vector3(0, j * (-110), 0);
			friends_rows [j].SetActive (true);
        }


        for (int i = 0; i < friend_data["friends"].Count; i++)
        {
            if (friend_data["friends"][i]["photo"] != "null")
				StartCoroutine(DisplayPhoto(i, friend_data["friends"][i]["photo"], friend_data["friends"][i]["id"], friend_data["friends"][i]["name"]));
        }
    }

    void Update()
    {
        for (int j = 0; j < getRowCount(); j++)
        {
            friends_rows[j].transform.GetComponent<UIGrid>().enabled = true;
        }

        if (PlayerPrefs.GetInt("friends_counter") == friends_count && friends_count != 0)
        {
            PlayerPrefs.SetInt("friends_loaded", 1);
            PlayerPrefs.Save();
        }
    }

    public int getRowCount()
    {
        int count = 0;
        if (friends_count != 0) {
            count = friends_count / 4;
            if(friends_count % 4 != 0)
            {
                count += 1;
            }
        }
        return count;
    }

    public int getRowPos(int pos)
    {
        int count = 0;
        if (pos != 0)
        {
            count = pos / 4;
            if (pos % 4 != 0)
            {
                count += 1;
            }
        }
        else
        {
            count = 1;
        }
        return count;
    }

	public IEnumerator DisplayPhoto(int pos, string photo_url, string id, string name) {
        WWW file = new WWW(photo_url);
        yield return file;

        Texture2D sourceTex = file.texture;
        Texture2D circle_texture = new Texture2D(sourceTex.height, sourceTex.width);
        int cx = sourceTex.width/2;
        int cy = sourceTex.height/2;
        int r = sourceTex.height / 2;
        for (int i = (int)(cx - r); i < cx + r; i++)
        {
            for (int j = (int)(cy - r); j < cy + r; j++)
            {
                float dx = i - cx;
                float dy = j - cy;
                float d = Mathf.Sqrt(dx * dx + dy * dy);
                if (d <= r)
                    circle_texture.SetPixel(i - (int)(cx - r), j - (int)(cy - r), sourceTex.GetPixel(i, j));
                else
                    circle_texture.SetPixel(i - (int)(cx - r), j - (int)(cy - r), Color.clear);
            }
        }
        circle_texture.Apply();

        Sprite ppp = new Sprite();
        pos_count += 1;
        ppp = Sprite.Create(circle_texture, new Rect(0, 0, 350, 350), new Vector2(0, 0), 350.0f);
        friends[pos] = (GameObject)GameObject.Instantiate(clone_friend, new Vector3(0, 0, 0), Quaternion.identity);
        friends[pos].GetComponent<UI2DSprite>().sprite2D = ppp;
		friends [pos].GetComponent<Manage_Drag_Object> ().id = id;
		friends [pos].GetComponent<Manage_Drag_Object> ().name = name;
        friends[pos].transform.parent = friends_rows[getRowPos(pos_count) -1].transform;
        friends[pos].transform.localPosition = new Vector3(0, 0, 0);
        friends[pos].GetComponent<UI2DSprite>().width = 120;
        friends[pos].GetComponent<UI2DSprite>().height = 120;
        friends[pos].transform.localScale = new Vector3(1, 1, 1);
		friends [pos].SetActive (true);
        
        friends_rows[getRowPos(pos) - 1].transform.GetComponent<UIGrid>().enabled = true;
		transform.GetComponent<UIGrid>().enabled = true;

		PlayerPrefs.SetInt ("friends_counter", PlayerPrefs.GetInt ("friends_counter") + 1);
        PlayerPrefs.Save();
    }

    
    string Read()
    {
        StreamReader sr;
        if (Application.isMobilePlatform)
        {
            sr = new StreamReader(Application.persistentDataPath + "/resources/friends.json");
        }
        else
        {
            sr = new StreamReader(Application.dataPath + "/resources/friends.json");
        }
        string content = sr.ReadToEnd();
        sr.Close();
        return content;
    }

    void AnalyticsJSON(string data)
    {

    }
	
}
