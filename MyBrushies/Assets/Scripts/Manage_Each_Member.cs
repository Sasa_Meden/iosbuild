﻿using UnityEngine;
using System.Collections;

public class Manage_Each_Member : MonoBehaviour {

    public int type;
    public bool flag = false;
    public GameObject groups_scroll;
    public GameObject clone_parent;

    public float gap_time = 1;
    public bool down_flag = false;
    public bool longclick_flag = false;

    int gap_init_time = 1;

	public string name;
	public string id;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (down_flag && !longclick_flag)
            gap_time -= Time.deltaTime;

        if (gap_time < 0)
        {
            if (down_flag)
            {
                Debug.Log("Long Clicked");
				Handheld.Vibrate ();
                longclick_flag = true;
                gap_time = gap_init_time;
				clone_parent.SetActive (true);
				groups_scroll.GetComponent<UIScrollView>().enabled = false;

                GameObject[] tmp_childs = new GameObject[gameObject.transform.parent.GetComponent<Manage_Members>().childs.Length - 1];
                int counter = 0;
                for (int i = 0; i < gameObject.transform.parent.GetComponent<Manage_Members>().childs.Length; i++)
                {
                    
                    if (gameObject.transform.parent.GetComponent<Manage_Members>().childs[i] != gameObject)
                    {
                        tmp_childs[counter] = gameObject.transform.parent.GetComponent<Manage_Members>().childs[i];
                        counter += 1;
                    }

                }
                gameObject.transform.parent.GetComponent<Manage_Members>().childs = tmp_childs;
                //gameObject.transform.parent.GetComponent<Manage_Members>().add_flag = true;
                clone_parent.transform.GetComponent<Manage_Mouse_Drag>().flag = true;

				gameObject.GetComponent<Animator> ().enabled = false;
				GameObject clone_member = gameObject;
				clone_member.transform.parent = clone_parent.transform;
				clone_member.transform.GetComponent<UI2DSprite>().depth = 15;
				clone_member.transform.localScale = new Vector3(2.1f, -2.1f, 2.1f);
				clone_parent.transform.GetComponent<Manage_Mouse_Drag>().drag_friend = clone_member;
                
                //Destroy(gameObject);
            }
        }
    }

    void OnPress(bool isDown)
    {
        if (isDown)
        {
            down_flag = true;
            Debug.Log("Down");
        }
        else
        {
            down_flag = false;
            groups_scroll.GetComponent<UIScrollView>().disableDragIfFits = false;
            longclick_flag = false;
            clone_parent.GetComponent<Manage_Mouse_Drag>().flag = false;

			if (clone_parent.GetComponent<Manage_Mouse_Drag>().match_group_id != -1 && !clone_parent.GetComponent<Manage_Mouse_Drag>().exist_flag)
            {
                clone_parent.GetComponent<Manage_Mouse_Drag>().groups[clone_parent.GetComponent<Manage_Mouse_Drag>().match_group_id].GetComponent<Manage_Members>().add_flag = true;          
				clone_parent.GetComponent<Manage_Mouse_Drag>().groups [clone_parent.GetComponent<Manage_Mouse_Drag>().match_group_id].GetComponent<Manage_Each_Group> ().trigger_flag = 2;
				clone_parent.GetComponent<Manage_Mouse_Drag>().groups [clone_parent.GetComponent<Manage_Mouse_Drag>().match_group_id].GetComponent<Manage_Each_Group> ().flag = true;
				clone_parent.GetComponent<Manage_Mouse_Drag>().match_group_id = -1;
            }

            Destroy(clone_parent.GetComponent<Manage_Mouse_Drag>().drag_friend);
            gap_time = gap_init_time;
			clone_parent.SetActive (false);
			groups_scroll.GetComponent<UIScrollView>().enabled = true;
            Debug.Log("Up");
        }
    }

    void OnDrag(Vector2 delta)
    {
        if (enabled && !longclick_flag)
        {
            down_flag = false;
            groups_scroll.GetComponent<UIScrollView>().disableDragIfFits = false;
            longclick_flag = false;
            clone_parent.GetComponent<Manage_Mouse_Drag>().flag = true;
            gap_time = gap_init_time;
            Debug.Log("Mouse Draged");
        }

    }

    void OnPan(Vector2 delta)
    {
        if (enabled)
        {
            Debug.Log("Mouse Pan");
        }
    }
}
