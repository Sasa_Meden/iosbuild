﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Manage_SetupDevice : MonoBehaviour
{
    public GameObject[] pages;
    public GameObject[] activies;
    public GameObject btnLockDevice;
    public GameObject lblMAC;
    public GameObject lblButtonLock;
    private string MAC = "";
    private PurpleToothbrush purple;
    // public GameObject home_window;
    public bool devicePurpleRun = false;
    /// <summary>
    /// 0 - device not locked, 1 - device locked
    /// </summary>
    private short state = 0;
    // private short stated = 0;

    public Color colorLock;
	public Color colorUnLock;
    public Color colorSearching;
    // Use this for initialization
    void Start()
    {
        Debug.Log("KKK SD " + "macFilter" + PlayerPrefs.GetString("ActiveProfile") + "..." + PlayerPrefs.GetString("macFilter" + PlayerPrefs.GetString("ActiveProfile")));

        if (!PlayerPrefs.HasKey("macFilter" + PlayerPrefs.GetString("ActiveProfile")))
        {
            state = 0;
            devicePurpleRun = true;
            btnLockDevice.GetComponent<UIButton>().enabled = true;
			btnLockDevice.GetComponent<UI2DSprite>().color = colorSearching;
            lblButtonLock.GetComponent<UILabel>().text = "SEARCHING...";
            lblMAC.GetComponent<UILabel>().text = "Device: ";
        }
        else
        {
            if (!PlayerPrefs.GetString("macFilter" + PlayerPrefs.GetString("ActiveProfile")).Equals(""))
            {
                state = 1;
                devicePurpleRun = false;
                btnLockDevice.GetComponent<UIButton>().enabled = true;
                lblButtonLock.GetComponent<UILabel>().text = "UNLOCK DEVICE";
				btnLockDevice.GetComponent<UI2DSprite>().color = colorUnLock;
                lblMAC.GetComponent<UILabel>().text = "Device: " + PlayerPrefs.GetString("macFilter" + PlayerPrefs.GetString("ActiveProfile"));
            }
            else
            {
                state = 0;
                devicePurpleRun = true;
                btnLockDevice.GetComponent<UIButton>().enabled = true;
                lblButtonLock.GetComponent<UILabel>().text = "SEARCHING...";
				btnLockDevice.GetComponent<UI2DSprite>().color = colorSearching;
                lblMAC.GetComponent<UILabel>().text = "Device: ";
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (devicePurpleRun)
        {

            if (PurpleContainer.instance.bleState != PurpleToothbrush.CONNECTED)
            {
                btnLockDevice.GetComponent<UIButton>().enabled = false;
                lblButtonLock.GetComponent<UILabel>().text = "SEARCHING...";
				btnLockDevice.GetComponent<UI2DSprite>().color = colorSearching;
                lblMAC.GetComponent<UILabel>().text = "Device: ";
                MAC = "";
            }
            else
            {
                MAC = PurpleContainer.instance.GetConnectedMAC();
                lblMAC.GetComponent<UILabel>().text = "Device: " + MAC;
                btnLockDevice.GetComponent<UIButton>().enabled = true;
                lblButtonLock.GetComponent<UILabel>().text = "LOCK DEVICE";
				btnLockDevice.GetComponent<UI2DSprite>().color = colorLock;
            }
        }
    }

    public void LockDevice()
    {
        //StartCoroutine(exitLock());
        if (state == 0)
        {
            PlayerPrefs.SetString("macFilter" + PlayerPrefs.GetString("ActiveProfile"), MAC);
            PlayerPrefs.Save();
            StartCoroutine(exitLock(state));
        }
        else
        {
            PlayerPrefs.SetString("macFilter" + PlayerPrefs.GetString("ActiveProfile"), "");
            PlayerPrefs.Save();
            StartCoroutine(exitLock(state));
        }
    }

    IEnumerator exitLock(short stated)
    {
        int count = 0;
        if(stated == 0)
        {
            yield return new WaitForSeconds(0.5f);
            devicePurpleRun = false;
            btnLockDevice.GetComponent<UI2DSprite>().color = colorSearching;
            lblButtonLock.GetComponent<UILabel>().text = "LOCKING DEVICE";
            btnLockDevice.GetComponent<UIButton>().enabled = false;
            PurpleContainer.instance.StopPurpleToothBrush();
            while (count < 1000 && (PurpleContainer.instance.bleState != PurpleToothbrush.DISCONECTED
            && PurpleContainer.instance.bleState != PurpleToothbrush.DEINITIALIZED))
            {
                yield return new WaitForSeconds(0.01f);
                count++;
            }
                PurpleContainer.instance.StartPurpleToothBrush(false, PlayerPrefs.GetString("macFilter" + PlayerPrefs.GetString("ActiveProfile")));
                Debug.Log("KKK PROFILE1 Start with MAC");
           
            goto_home();

        }
        else
        {
            yield return new WaitForSeconds(0.5f);
            devicePurpleRun = false;
            btnLockDevice.GetComponent<UI2DSprite>().color = colorSearching;
            lblButtonLock.GetComponent<UILabel>().text = "UNLOCKING DEVICE";
            btnLockDevice.GetComponent<UIButton>().enabled = false;
            PurpleContainer.instance.StopPurpleToothBrush();
            while (count < 1000 && (PurpleContainer.instance.bleState != PurpleToothbrush.DISCONECTED
            && PurpleContainer.instance.bleState != PurpleToothbrush.DEINITIALIZED))
            {
                yield return new WaitForSeconds(0.01f);
                count++;
            }
            PurpleContainer.instance.StartPurpleToothBrush();
            Debug.Log("KKK PROFILE1 Start without MAC");
           

            goto_home();


        }
    }

    public void goto_home()
    {
        SceneManager.LoadScene("home", LoadSceneMode.Single);
    }
}
