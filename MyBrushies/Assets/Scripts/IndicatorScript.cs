﻿using UnityEngine;
using System.Collections;

public class IndicatorScript : MonoBehaviour {

    public Vector3 sliderIndicatorPosition = new Vector3();
    private float sliderDefaultRange = 4.4f;
    private Transform sliderIndicatorTransform;
    private Transform sliderTransform;
    private float sliderPosValue = -4.4f;
    private float sliderSpeed;

    // Use this for initialization
    void Start () {

        sliderIndicatorTransform = this.transform;
        sliderIndicatorTransform.position = sliderIndicatorPosition;
        sliderTransform = this.transform.Find("slider").gameObject.GetComponent<Transform>();
        setPos();
        CalculateSliderLimits();
    }
    public void setSpeed(float speed,int direction)
    {
        sliderSpeed = speed;
        sliderTransform.position = new Vector3((sliderPosValue * direction) + sliderSpeed * (sliderDefaultRange *2 ), sliderTransform.position.y, sliderTransform.position.z);
    }
    public void setPos()
    {
        sliderIndicatorTransform.position = sliderIndicatorPosition;
        sliderTransform.position = new Vector3(sliderTransform.position.x, -4.4f, sliderTransform.position.z);
    }
    public void setLimitPos(int limit)
    {
        sliderIndicatorTransform.position = sliderIndicatorPosition;
        sliderTransform.position = new Vector3(sliderPosValue*limit, sliderTransform.position.y, sliderTransform.position.z);
    }
    public void CalculateSliderLimits()
    {
        sliderPosValue = sliderIndicatorTransform.position.x + sliderDefaultRange;
    }
}
