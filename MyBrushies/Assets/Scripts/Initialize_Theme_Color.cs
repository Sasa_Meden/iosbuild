﻿using UnityEngine;
using System.Collections;

public class Initialize_Theme_Color : MonoBehaviour {

    public Color init_color;
	// Use this for initialization
	void Start () {
        if (PlayerPrefs.GetString("th_Color") == "" || PlayerPrefs.GetString("th_Color") ==null)
        {
            PlayerPrefs.SetString("th_Color", "" + init_color);
            PlayerPrefs.SetString("temp_theme_Color", "" + init_color);
            PlayerPrefs.Save();
        }
        else
        {
            PlayerPrefs.SetString("temp_theme_Color", PlayerPrefs.GetString("th_Color"));
            PlayerPrefs.Save();
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
