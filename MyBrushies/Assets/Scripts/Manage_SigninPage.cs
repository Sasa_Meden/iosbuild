﻿using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

using System.Collections.Generic;


using SimpleJSON;

public class Manage_SigninPage : MonoBehaviour {

	public GameObject window;
	public GameObject loading;
	public GameObject email;
	public GameObject signup_bt;
	public GameObject create_bt;
	public GameObject pwd;
	public GameObject back_bt;
	public GameObject pwd_eye;
	public GameObject input_pwd;

	public GameObject signup_title;

	public string page_status;
	public int signup_success=0;
	private static readonly char[] SpecialChars = "!@#$%^&*()~".ToCharArray();
	// Use this for initialization
	void Start () {
		Debug.Log("SCN SignIn");
		Screen.orientation = ScreenOrientation.Portrait;
		PlayerPrefs.SetString ("current_page", "SignIn");
        PlayerPrefs.Save();
        //		if (page_status == "Signin") {
        if (PlayerPrefs.GetString ("refresh_token", "none") != "none") {
			StartCoroutine (WRefreshToken ());
		}
		//		}

	}


	// Update is called once per frame
	void Update () {
		if (signup_success == 1) {
			UIStatus.Show (Localization.Get ("Signed Up Successfully!"));
			signup_success = 0;
		}

		if (Input.GetKeyDown (KeyCode.Escape) && window.transform.localPosition.x == -611 &&  page_status=="SignIn") {
			Application.Quit ();
		}

		if (Input.GetKeyDown (KeyCode.Escape) && window.transform.localPosition.x==-611 && page_status==PlayerPrefs.GetString("current_page", "None") && page_status!="SignIn") {
			UIPlayTween[] all_Components = back_bt.gameObject.GetComponents<UIPlayTween>();
			for (int i = 0; i < all_Components.Length; i++) {
				all_Components [i].enabled = true;
				all_Components [i].Play (true);
			}
			PlayerPrefs.SetString ("current_page", "SignIn");
            PlayerPrefs.Save();
        }
	}

	public void pwd_hideshow(){
		if (input_pwd.GetComponent<UIInput> ().inputType == UIInput.InputType.Password) {
			input_pwd.GetComponent<UIInput> ().inputType=UIInput.InputType.Standard;
			pwd_eye.GetComponent<UILabel>().text="HIDE";
		} else {
			input_pwd.GetComponent<UIInput> ().inputType=UIInput.InputType.Password;
			pwd_eye.GetComponent<UILabel>().text="SHOW";
		}
		input_pwd.GetComponent<UIInput>().isSelected = true;
	}

	public void Goto_Signup(){
		PlayerPrefs.SetString ("current_page", "SignUp");
        PlayerPrefs.Save();
        signup_bt.GetComponent<UIPlayTween>().enabled=true;
		signup_bt.GetComponent<UIPlayTween> ().Play (true);

		signup_title.GetComponent<UIPlayTween>().enabled=true;
		signup_title.GetComponent<UIPlayTween> ().Play (true);

	}

	public void Goto_Camera(){
		SceneManager.LoadScene("camera", LoadSceneMode.Single);

	}
		
	public void Signup(){
		
		string email_txt = email.GetComponent<UIInput> ().value.Trim();
		string pwd_txt = pwd.GetComponent<UIInput> ().value.Trim ();

		if (email_txt == "" || email_txt=="EMAIL ADDRESS") {
			UIStatus.Show (Localization.Get ("Email Address can not be empty!"));
		} else if (pwd_txt == "" || pwd_txt == "PASSWORD") {
			UIStatus.Show (Localization.Get ("Password can not be empty!"));
		} else if(pwd_txt.Length<8){
			UIStatus.Show (Localization.Get ("Password is too short!(minimum is 8 characters.)"));
		} else if(pwd_txt.IndexOfAny(SpecialChars)==-1){
			UIStatus.Show (Localization.Get ("Password must contain at least one special character."));
		}else{
			PlayerPrefs.SetString ("current_page", "Welcome");
			PlayerPrefs.SetString ("email", email_txt);
			PlayerPrefs.SetString ("password", pwd_txt);
            PlayerPrefs.Save();
			signup_bt.GetComponent<UIPlayTween>().enabled=true;
			signup_bt.GetComponent<UIPlayTween> ().Play (true);

			signup_title.GetComponent<UIPlayTween>().enabled=true;
			signup_title.GetComponent<UIPlayTween> ().Play (true);
		}

	}

	public void Signin(){
		string email_txt = email.GetComponent<UIInput> ().value.Trim();
		string pwd_txt = pwd.GetComponent<UIInput> ().value.Trim ();

		if (email_txt == "" || email_txt=="EMAIL ADDRESS") {
			UIStatus.Show (Localization.Get ("Email Address can not be empty!"));
		} else if (pwd_txt == "" || pwd_txt == "PASSWORD") {
			UIStatus.Show (Localization.Get ("Password can not be empty!"));
		} else {
			StartCoroutine (WSigninRequest ());
		}

	}


	IEnumerator WRefreshToken() {
		loading.SetActive (true);
		string postScoreURL= Config.baseURL + "/oauth/token";
		WWWForm form = new WWWForm();
		var requestBody = "{ \"client_id\": \"e5d92e3f799987a9489b0edd6a00d78018f2abbf2a8aa98124d9f2d913e8e515\", \"client_secret\": \"4eb4a4532538864c6000f06f4fce5fc1e2d6fb12705cef5dc6891bdd68aa9eae\", \"grant_type\": \"refresh_token\", \"refresh_token\": \""+PlayerPrefs.GetString("refresh_token")+"\" }";
		var encoding = new System.Text.UTF8Encoding();
		var postHeader = form.headers;

		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"]="application/json";

		WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
		yield return request;

		if (request.error != null)
		{
			Debug.Log("request error: " + request.error);
			Debug.Log("request error: " + request.responseHeaders);
		}
		else
		{
			JSONNode jNodes = JSON.Parse (request.text);
			PlayerPrefs.SetString ("token", jNodes["access_token"]);
			PlayerPrefs.SetString ("refresh_token", jNodes["refresh_token"]);
			PlayerPrefs.SetString ("expires_in", jNodes["expires_in"]);
            PlayerPrefs.Save();
            Debug.Log("request success");
			Debug.Log("returned data" + request.text);
			SceneManager.LoadScene("home", LoadSceneMode.Single);

		}
	}

	IEnumerator WSigninRequest() {
		loading.SetActive (true);
		string postScoreURL= Config.baseURL + "/oauth/token";
		WWWForm form = new WWWForm();
		var requestBody = "{ \"client_id\": \"e5d92e3f799987a9489b0edd6a00d78018f2abbf2a8aa98124d9f2d913e8e515\", \"client_secret\": \"4eb4a4532538864c6000f06f4fce5fc1e2d6fb12705cef5dc6891bdd68aa9eae\", \"grant_type\": \"password\", \"email\": \""+email.GetComponent<UIInput> ().value.Trim()+"\", \"password\": \""+pwd.GetComponent<UIInput> ().value.Trim()+"\", \"username\": \""+email.GetComponent<UIInput> ().value.Trim()+"\", \"scope\": \"mobile\" }";
		var encoding = new System.Text.UTF8Encoding();
		var postHeader = form.headers;

		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"]="application/json";

		WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
		yield return request;

		if (request.error != null)
		{
			loading.SetActive (false);
			UIStatus.Show (Localization.Get ("Signin Failed!"));
			JSONNode jNodes = JSON.Parse (request.text);
			Debug.Log (jNodes);
			Debug.Log("request error: " + request.error);
			Debug.Log("request error: " + request.responseHeaders);
		}
		else
		{
			JSONNode jNodes = JSON.Parse (request.text);
			PlayerPrefs.SetString ("token", jNodes["access_token"]);
			PlayerPrefs.SetString ("refresh_token", jNodes["refresh_token"]);
			PlayerPrefs.SetString ("expires_in", jNodes["expires_in"]);
            PlayerPrefs.Save();
            Debug.Log("request success");
			Debug.Log("returned data" + request.text);
			//Application.LoadLevel ("setting");
			StartCoroutine(WGetProfileRequest());
		}
	}

	IEnumerator WGetProfileRequest() {
		string getURL= Config.baseURL + "/v1/profiles";
		WWWForm form = new WWWForm();
		var getHeader = form.headers;
		getHeader["Content-Type"] = "application/json";
		getHeader["Accept"]="application/json";
		getHeader ["Authorization"] = "Bearer " + PlayerPrefs.GetString ("token");

		WWW request = new WWW(getURL, null, getHeader);
		yield return request;

		if (request.error != null)
		{
			loading.SetActive (false);
			UIStatus.Show (Localization.Get ("Signin Failed While Getting Profiles!"));

			Debug.Log("request error: " + request.error);
			Debug.Log("request error: " + request.responseHeaders);
		}
		else
		{
			JSONNode jNodes = JSON.Parse (request.text);

			PlayerPrefs.SetInt ("profile_counter", jNodes ["profiles"].Count);
            PlayerPrefs.Save();
            Debug.Log ("Profile Counter: " + jNodes ["profiles"].Count);
			for (int i = 0; i < jNodes ["profiles"].Count; i++) {
				PlayerPrefs.SetString ("profile_id_"+(i+1), jNodes["profiles"][i]["id"]);
				PlayerPrefs.SetString ("profile_name_"+(i+1), jNodes["profiles"][i]["name"]);
				PlayerPrefs.SetString ("profile_birthday_"+(i+1), jNodes["profiles"][i]["date_of_birth"]);
				PlayerPrefs.SetString ("profile_gender_"+(i+1), jNodes["profiles"][i]["gender"]);
				PlayerPrefs.SetString ("profile_dominant_"+(i+1), jNodes["profiles"][i]["dominant_hand"]);
				PlayerPrefs.SetString ("profile_picture_"+(i+1), jNodes["profiles"][i]["picture"]);
////////////////////////////////////////
                PlayerPrefs.SetString("profile_battery_reporting_period_" + (i + 1),jNodes["profiles"][i]["settings"]["battery_reporting_period"]);
////////////////////////////////////////                              
                //Here should get the avator index or image profile link from backend api
                PlayerPrefs.SetInt ("profile_avator_" + (i + 1), -1);//if none, this is set default avator index id.
                PlayerPrefs.Save();
            }
           
            SceneManager.LoadScene("home", LoadSceneMode.Single);
			Debug.Log("request success");
			Debug.Log("returned data" + request.text);
		}
	}
}