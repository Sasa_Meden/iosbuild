﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Responsive_Map : MonoBehaviour {

	public GameObject[] maps;
	public GameObject content_wrap;
	// Use this for initialization
	void Start () {
		float s_width = Screen.width*1920/Screen.height;

		float ratio = 2048f / 557f;
		content_wrap.GetComponent<UIWrapContent>().itemSize=(int)(s_width * ratio);
		for (int i = 0; i < maps.Length; i++) {
			maps [i].GetComponent<UI2DSprite> ().width = (int)s_width;
			maps [i].GetComponent<UI2DSprite> ().height = (int)(s_width * ratio);
			maps [i].transform.localPosition = new Vector3 (0f, (0f - maps [i].GetComponent<UI2DSprite> ().height * (maps.Length - 1 - i)), 0f);
		}
	}

	// Update is called once per frame
	void Update () {

	}
}
