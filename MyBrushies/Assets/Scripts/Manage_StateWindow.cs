﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.SceneManagement;
using System;
using System.Globalization;

public class Manage_StateWindow : MonoBehaviour {

	public string[] personal_data;
	public Sprite avatar;
	public int personal_index;
	public Sprite[] childrens;
	public GameObject name;
	public GameObject photo;
	public GameObject week_bt;
	public GameObject month_bt;
	public GameObject profile_bt;
	public GameObject profile_window;
	public GameObject _window;
	public GameObject back_bt;
	public GameObject photo_loading;

	public GameObject calendar;
	public GameObject static_label;
	public GameObject loading;

	public bool init_flag=false;

	// Use this for initialization
	void Start () {
        Debug.Log("SCN StateWindow");
    }
	
	// Update is called once per frame
	void Update () {
		if (init_flag) {
			init ();
			init_flag = false;
		}
	}

	public void init(){
		if (personal_data [5] == "") {
			if (personal_data [3] == "male") {
				photo.GetComponent<UI2DSprite> ().sprite2D = childrens [0];
			} else {
				photo.GetComponent<UI2DSprite> ().sprite2D = childrens [1];
			}
		} else {
			//StartCoroutine (DisplayPhoto(photo, personal_data [5]));
			photo.GetComponent<UI2DSprite> ().sprite2D=avatar;
		}
		name.GetComponent<UILabel> ().text = personal_data [1];
		StartCoroutine (WGetBrushiesStatus (personal_data [0]));
	}

	public void Toggle_week(){
		week_bt.SetActive (true);
		month_bt.SetActive (false);
	}

	public void Toggle_month(){
		week_bt.SetActive (false);
		month_bt.SetActive (true);
	}

	public void Set_Profile(){
		profile_window.GetComponent<Manage_ProfileSetting> ().personal_data = personal_data;
		profile_window.GetComponent<Manage_ProfileSetting> ().avatar = avatar;
		profile_window.GetComponent<Manage_ProfileSetting> ().personal_index = personal_index; 
		profile_window.GetComponent<Manage_ProfileSetting> ().setting_flag = true;
		profile_window.GetComponent<Manage_ProfileSetting> ().new_flag = false;
		//profile_bt.GetComponent<UIPlayTween> ().enabled = true;
		profile_bt.GetComponent<UIPlayTween> ().Play (true);
	}

	public IEnumerator DisplayPhoto(GameObject obj, string photo_url) {
		photo_loading.SetActive (true);
		WWW file = new WWW(photo_url);
		yield return file;

		Texture2D sourceTex = file.texture;
		Sprite ppp = new Sprite();
		ppp = Sprite.Create(sourceTex, new Rect(0, 0, sourceTex.width, sourceTex.height), new Vector2(0, 0), 100f);
		obj.GetComponent<UI2DSprite> ().sprite2D = ppp;
		photo_loading.SetActive (false);
	}

	IEnumerator WGetBrushiesStatus(string profile_id) {
		string getURL= Config.baseURL + "/v1/brushing_statistics";
		WWWForm form = new WWWForm();
		var getHeader = form.headers;
		getHeader["Content-Type"] = "application/json";
		getHeader["Accept"]="application/json";
		getHeader ["Authorization"] = "Bearer " + PlayerPrefs.GetString ("token");
		getHeader ["Current-Profile-Id"] = profile_id;
		var end_date = System.DateTime.Now;
		var start_date = end_date.AddDays (-91);

		TimeZone localtimezone = TimeZone.CurrentTimeZone;
		string timezone = localtimezone.GetUtcOffset (end_date).ToString();
		char[] timezone_arr = timezone.ToCharArray ();
		if (timezone_arr [0] == '-') {
			timezone = ""+timezone_arr [0] + timezone_arr [1] + timezone_arr [2] + timezone_arr [3] + timezone_arr [4] + timezone_arr [5];
		} else {
			timezone = "%2B"+timezone_arr [0] + timezone_arr [1] + timezone_arr [2] + timezone_arr [3] + timezone_arr [4];
		}

		Debug.Log ("TimeZone: " + timezone);
		var request_param="?start_date="+start_date.ToString("yyyy-MM-dd")+"&end_date="+end_date.ToString("yyyy-MM-dd")+"&utc_offset="+timezone;

		WWW request = new WWW(getURL+request_param, null, getHeader);
		yield return request;

		if (request.error != null)
		{
			UIStatus.Show (Localization.Get ("Failed Retrieving Brushing Statistics!"));
			Debug.Log (getURL + request_param);
			Debug.Log("request header: " + form.headers);
			Debug.Log("request error: " + request.error);
			Debug.Log("request error: " + request.responseHeaders);
		}
		else
		{
			JSONNode jNodes = JSON.Parse (request.text);

			int s_counts = 0;
			for (int i = 1; i < 92; i++) {
				var date = end_date.AddDays (-i).ToString("yyyy-MM-dd");

				if ((jNodes ["brushing_statistics"] [date] ["am"].AsBool) || (jNodes ["brushing_statistics"] [date] ["pm"].AsBool)){
					s_counts += 1;
					Debug.Log (date+": "+jNodes ["brushing_statistics"] [date] ["am"]+" "+jNodes ["brushing_statistics"] [date] ["pm"]);
				}

			}
			Debug.Log ("COUNTS: " + s_counts);
			static_label.GetComponent<UILabel>().text="Brushied: "+s_counts+" of last 90 days";
			calendar.GetComponent<Manage_Calender> ().jStatics = jNodes;
			calendar.GetComponent<Manage_Calender> ().init_flag = true;

			Debug.Log("request success");
			Debug.Log("returned data" + request.text);
		}
	}

	public void goto_setupdevice(){
		loading.SetActive (true);
        SceneManager.LoadScene("setupDevice", LoadSceneMode.Single);
        //Application.LoadLevel ("setupDevice");
	}
}
