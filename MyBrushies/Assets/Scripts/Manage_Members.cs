﻿using UnityEngine;
using System.Collections;

public class Manage_Members : MonoBehaviour {

    public GameObject[] childs;
	public string[] child_ids;

	public GameObject clone_obj;
    public int[] points_1_x;
    public int[] points_1_y;

    public int[] points_2_x;
    public int[] points_2_y;

    public int[] points_3_x;
    public int[] points_3_y;

    public int[] points_4_x;
    public int[] points_4_y;

    public int[] points_5_x;
    public int[] points_5_y;
    public bool moving_flag = false;
    public int group_id;

    public bool forward_flag = false;//when group is activated, the member objects will be moved into next step status
    public bool add_flag = false;
    // Use this for initialization
    void Start()
    {
        points_1_x = new int[1];
        points_1_y = new int[1];

        points_2_x = new int[2];
        points_2_y = new int[2];

        points_3_x = new int[3];
        points_3_y = new int[3];

        points_4_x = new int[4];
        points_4_y = new int[4];

        points_5_x = new int[5];
        points_5_y = new int[5];

        points_1_x[0]=0;
        points_1_y[0] = -75;

        points_2_x[0] = 0;
        points_2_y[0] = -75;
        points_2_x[1] = 0;
        points_2_y[1] = 75;

        points_3_x[0] = 0;
        points_3_y[0] = -75;
        points_3_x[1] = 66;
        points_3_y[1] = 37;
        points_3_x[2] = -66;
        points_3_y[2] = 37;

        points_4_x[0] = 0;
        points_4_y[0] = -75;
        points_4_x[1] = 75;
        points_4_y[1] = 0;
        points_4_x[2] = 0;
        points_4_y[2] = 75;
        points_4_x[3] = -75;
        points_4_y[3] = 0;

        points_5_x[0] = 0;
        points_5_y[0] = -75;
        points_5_x[1] = 75;
        points_5_y[1] = -19;
        points_5_x[2] = 43;
        points_5_y[2] = 63;
        points_5_x[3] = -43;
        points_5_y[3] = 63;
        points_5_x[4] = -75;
        points_5_y[4] = -19;
    }

    // Update is called once per frame
    void Update () {
        if (moving_flag)
        {
            int count = 0;
            foreach (Transform child in transform)
            {
                GameObject tmp= child.gameObject;
                if (tmp.name != "Title")
                {
                    childs[count] = tmp.gameObject;
                    count += 1;
                }
            }

            for (int i = 0; i < count; i++)
            {
                switch (count)
                {
                    case 1:
                        childs[i].transform.localPosition = new Vector3(points_1_x[i], points_1_y[i], 0);
                        break;
                    case 2:
                        childs[i].transform.localPosition = new Vector3(points_2_x[i], points_2_y[i], 0);
                        break;
                    case 3:
                        childs[i].transform.localPosition = new Vector3(points_3_x[i], points_3_y[i], 0);
                        break;
                    case 4:
                        childs[i].transform.localPosition = new Vector3(points_4_x[i], points_4_y[i], 0);
                        break;
                    case 5:
                        childs[i].transform.localPosition = new Vector3(points_5_x[i], points_5_y[i], 0);
                        break;
                }
            }
            moving_flag = false;
        }

        if (forward_flag)
        {
            Forward_Init_Setting();
        }else
        {
            Back_Init_Setting();
        }

		if (add_flag && childs.Length < 5)
        {
            GameObject[] tmp_childs = new GameObject[childs.Length + 1];
			string[] tmp_child_ids = new string[childs.Length + 1];
            for(int i=0; i<childs.Length; i++)
            {
                tmp_childs[i] = childs[i];
				tmp_child_ids [i] = child_ids [i];
            }
            tmp_childs[childs.Length]=(GameObject)GameObject.Instantiate(childs[0], new Vector3(0, 0, 0), Quaternion.identity);
            tmp_childs[childs.Length].transform.parent = transform;
			tmp_childs [childs.Length].GetComponent<UI2DSprite> ().sprite2D = clone_obj.GetComponent<UI2DSprite> ().sprite2D;
			tmp_child_ids [childs.Length] = clone_obj.GetComponent<Manage_Drag_Object> ().id;
            tmp_childs[childs.Length].transform.localScale = new Vector3(1, 1, 1);
            switch (childs.Length)
            {
                case 1:
                    tmp_childs[childs.Length].transform.localPosition = new Vector3(points_2_x[childs.Length], points_2_y[childs.Length], 0);
                    break;
                case 2:
                    tmp_childs[childs.Length].transform.localPosition = new Vector3(points_3_x[childs.Length], points_3_y[childs.Length], 0);
                    break;
                case 3:
                    tmp_childs[childs.Length].transform.localPosition = new Vector3(points_4_x[childs.Length], points_4_y[childs.Length], 0);
                    break;
                case 4:
                    tmp_childs[childs.Length].transform.localPosition = new Vector3(points_5_x[childs.Length], points_5_y[childs.Length], 0);
                    break;
            }
            
			tmp_childs [childs.Length].SetActive (true);
			tmp_childs [childs.Length].transform.GetComponent<Animator> ().enabled = true;

            childs = new GameObject[childs.Length + 1];
			child_ids = new string[childs.Length + 1];
            childs = tmp_childs;
			child_ids = tmp_child_ids;
            forward_flag = false;
        }
		add_flag = false;
	}

    void Forward_Init_Setting() {
        float speed = 10.5f;
        for (int i = 0; i < childs.Length; i++)
        {
            switch (childs.Length)
            {
                case 1:
                    Vector3 position1 = Vector3.Lerp(childs[i].transform.localPosition, new Vector3(points_2_x[i], points_2_y[i], 0), Time.deltaTime * speed);
                    childs[i].transform.localPosition = position1;
                    break;
                case 2:
                    Vector3 position2 = Vector3.Lerp(childs[i].transform.localPosition, new Vector3(points_3_x[i], points_3_y[i], 0), Time.deltaTime * speed);
                    childs[i].transform.localPosition = position2;
                    break;
                case 3:
                    Vector3 position3 = Vector3.Lerp(childs[i].transform.localPosition, new Vector3(points_4_x[i], points_4_y[i], 0), Time.deltaTime * speed);
                    childs[i].transform.localPosition = position3;
                    break;
                case 4:
                    Vector3 position4 = Vector3.Lerp(childs[i].transform.localPosition, new Vector3(points_5_x[i], points_5_y[i], 0), Time.deltaTime * speed);
                    childs[i].transform.localPosition = position4;
                    break;
            }
        } 
    }

    void Back_Init_Setting()
    {
        float speed = 10.5f;
        for (int i = 0; i < childs.Length; i++)
        {
            if (childs[i] != null)
            {
                switch (childs.Length)
                {
                    case 1:
                        Vector3 position1 = Vector3.Lerp(childs[i].transform.localPosition, new Vector3(points_1_x[i], points_1_y[i], 0), Time.deltaTime * speed);
                        childs[i].transform.localPosition = position1;
                        break;
                    case 2:
                        Vector3 position2 = Vector3.Lerp(childs[i].transform.localPosition, new Vector3(points_2_x[i], points_2_y[i], 0), Time.deltaTime * speed);
                        childs[i].transform.localPosition = position2;
                        break;
                    case 3:
                        Vector3 position3 = Vector3.Lerp(childs[i].transform.localPosition, new Vector3(points_3_x[i], points_3_y[i], 0), Time.deltaTime * speed);
                        childs[i].transform.localPosition = position3;
                        break;
                    case 4:
                        Vector3 position4 = Vector3.Lerp(childs[i].transform.localPosition, new Vector3(points_4_x[i], points_4_y[i], 0), Time.deltaTime * speed);
                        childs[i].transform.localPosition = position4;
                        break;
                }
            }
        }
    }
}
