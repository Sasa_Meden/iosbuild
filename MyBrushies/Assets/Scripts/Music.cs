﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {
    public static Music instance = null;
    //public AudioSource audio;
    //public bool play = true;
    // Use this for initialization
    public static Music Instance
    {
        get { return instance; }
    }

    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        } else {
            instance = this;
           /* audio = instance.GetComponent<AudioSource>();
            if (!play)
            {
                audio.Stop();
            }
            else {
                audio.Play();
            }*/
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
