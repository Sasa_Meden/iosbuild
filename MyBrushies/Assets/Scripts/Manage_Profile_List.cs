﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manage_Profile_List : MonoBehaviour {

	public GameObject clone_profile;
	public GameObject[] profiles;

	public string[] personal_data;
	public string[][] profile_datas;
	public Sprite[] avatars;
	public int personal_index;
	public Sprite[] childrens;
	public GameObject grid;

	public GameObject _window;
	public GameObject back_bt;
	public GameObject home_window;

	public bool reinitialize_flag = false;
	public bool initialize_flag=false;

	// Use this for initialization
	void Start () {
		profiles_arrange ();
	}
	
	// Update is called once per frame
	void Update () {
		if (reinitialize_flag) {
			reinitialize_flag = false;
			reinitialize_profiles ();
		}
	}

	public void reinitialize_profiles(){
		int counter=PlayerPrefs.GetInt ("profile_counter");
		for (int i = 0; i < profiles.Length; i++) {
			GameObject.Destroy (profiles [i]);
		}
		profiles_arrange ();
		for (int j = 0; j < profiles.Length; j++) {
			profiles [j].transform.Find ("photo").GetComponent<UI2DSprite> ().sprite2D = home_window.GetComponent<Manage_HomeProfiles> ().profiles [j].GetComponent<UI2DSprite> ().sprite2D;
		}
		grid.GetComponent<UIGrid>().enabled = true;
//		grid.transform.localPosition = new Vector3 (-326.0f, -100.0f, 0f);
	}

	void profiles_arrange(){
		int counter = PlayerPrefs.GetInt ("profile_counter");
		profile_datas = new string[counter][];
		profiles = new GameObject[counter];
		for (int i = 1; i <= counter; i++) {
			profiles[i-1]=(GameObject)GameObject.Instantiate(clone_profile, new Vector3(0, 0, 0), Quaternion.identity);
			profile_datas [i-1] = new string[6];
			profile_datas [i-1][0] = PlayerPrefs.GetString ("profile_id_" + i);
			profile_datas [i-1][1] = PlayerPrefs.GetString ("profile_name_" + i);
			profile_datas [i-1][2] = PlayerPrefs.GetString ("profile_birthday_" + i);
			profile_datas [i-1][3] = PlayerPrefs.GetString ("profile_gender_" + i);
			profile_datas [i-1][4] = PlayerPrefs.GetString ("profile_dominant_" + i);
			profile_datas [i-1][5] = PlayerPrefs.GetString ("profile_picture_" + i);

          //  profile_datas [i-1][6] = PlayerPrefs.GetString("profile_battery_reporting_period_" + i);

            profiles [i - 1].transform.Find ("Label").GetComponent<UILabel> ().text = profile_datas [i - 1] [1];

			profiles [i - 1].GetComponent<Goto_Profile> ().index = i;
			profiles [i - 1].GetComponent<Goto_Profile> ().personal_data = profile_datas [i - 1];

//			if (profile_datas [i - 1] [5] == "") {
				if (profile_datas [i - 1] [3] == "male") {
					profiles [i - 1].transform.Find ("photo").GetComponent<UI2DSprite> ().sprite2D = childrens [0];
				} else {
					profiles [i - 1].transform.Find ("photo").GetComponent<UI2DSprite> ().sprite2D = childrens [1];
				}
//			} else {
//				profiles [i - 1].transform.Find ("photo").GetComponent<UI2DSprite> ().sprite2D=home_window.GetComponent<Manage_HomeProfiles> ().profiles [i-1].GetComponent<Manage_Profile> ().avatar;
//			}

			profiles [i-1].transform.parent = grid.transform;
			profiles [i - 1].transform.localScale = new Vector3 (1f, 1f, 1f);
			profiles [i - 1].transform.localPosition = new Vector3(0, 0, 0);
			//profiles [i - 1].GetComponent<Manage_Profile> ().index = i;
			profiles [i - 1].SetActive (true);
			grid.transform.GetComponent<UIGrid>().enabled = true;
		}
	}

	public void SetAvatar_SpecificProfile(Sprite sp, int index, string[] data){
		profiles [index - 1].transform.Find ("photo").GetComponent<UI2DSprite> ().sprite2D = sp;
		profiles [index - 1].transform.Find ("Label").GetComponent<UILabel> ().text = data [1];
		profiles [index - 1].GetComponent<Goto_Profile> ().personal_data = data;

	}
}
