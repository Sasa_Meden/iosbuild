﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;
using UnityEngine.SceneManagement;
public class Manage_Profiles : MonoBehaviour {

	public GameObject grid;
	public GameObject clone_profile;
	public GameObject[] profiles;
	public string[][] profile_datas;
	public GameObject loading;
	public Texture2D[] avator_imgs;
    public Sprite[] avators;


    private string baseURL="http://pearly01.mybrushies.com";
	private int _counter;
	private int _eCounter;
	// Use this for initialization
	void Start () {
		Screen.orientation = ScreenOrientation.Portrait;
		profiles_arrange ();
		_counter = 0;
		_eCounter = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    void profiles_arrange(){
		int counter = PlayerPrefs.GetInt ("profile_counter");
		profile_datas = new string[counter][];
		profiles = new GameObject[counter];
		for (int i = 1; i <= counter; i++) {
			profile_datas[i-1] = new string[5];
			profile_datas [i-1][0] = PlayerPrefs.GetString ("profile_id_" + i);
			profile_datas [i-1][1] = PlayerPrefs.GetString ("profile_name_" + i);
			profile_datas [i-1][2] = PlayerPrefs.GetString ("profile_birthday_" + i);
			profile_datas [i-1][3] = PlayerPrefs.GetString ("profile_gender_" + i);
			profile_datas [i-1][4] = PlayerPrefs.GetString ("profile_dominant_" + i);

			profiles[i-1]=(GameObject)GameObject.Instantiate(clone_profile, new Vector3(0, 0, 0), Quaternion.identity);
			profiles [i-1].transform.parent = grid.transform;
			profiles [i - 1].transform.localScale = new Vector3 (1f, 1f, 1f);
			profiles [i - 1].transform.localPosition = new Vector3(0, 0, 0);
			profiles [i - 1].GetComponent<Manage_AvatorProfile> ().index = i;
			profiles [i - 1].SetActive (true);
			profiles [i - 1].GetComponent<Manage_AvatorProfile> ().personal_data = profile_datas [i - 1];
		}
		grid.transform.GetComponent<UIGrid>().enabled = true;
       
    }

	public void next_click(){
		loading.SetActive (true);
		for(int i=0; i<profiles.Length; i++){
			int index = profiles [i].GetComponent<Manage_AvatorProfile> ().index;
			if (PlayerPrefs.GetInt ("profile_avator_" + index, -1) != -1) {
				_eCounter++;
                Texture2D avatar_text = textureFromSprite(avators[PlayerPrefs.GetInt("profile_avator_" + index, -1)]);
                byte[] bytes = avatar_text.EncodeToPNG();
                string enc = System.Convert.ToBase64String (bytes);
				StartCoroutine (WProfileUpdateRequest (i + 1, enc));
				Debug.Log ("image_string: " + enc);
			}
		}
		if(_eCounter==0)
            SceneManager.LoadScene("home", LoadSceneMode.Single);
    }

	IEnumerator WProfileUpdateRequest(int pos, string image_enc) {
		loading.SetActive (true);
		string postScoreURL= baseURL + "/v1/profiles/"+PlayerPrefs.GetString("profile_id_"+pos);
		WWWForm form = new WWWForm();
		string sex = PlayerPrefs.GetString ("profile_gender_" + pos);
		string handed = PlayerPrefs.GetString ("profile_dominant_" + pos);

		var requestBody = "{ \"profile\": { \"name\": \""+PlayerPrefs.GetString ("profile_name_" + pos)+"\", \"date_of_birth\": \""+PlayerPrefs.GetString ("profile_birthday_" + pos)+"\", \"gender\": \""+sex+"\", \"dominant_hand\": \""+handed+"\", \"picture\": \"data:image/png;base64,"+image_enc+"\" }}";
		var encoding = new System.Text.UTF8Encoding();
		var postHeader = form.headers;

		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"]="application/json";
		postHeader["X-HTTP-Method-Override"]="PUT";
		postHeader["Authorization"]="Bearer "+PlayerPrefs.GetString("token");

		WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
		yield return request;

		if (request.error != null)
		{
			loading.SetActive (false);
			UIStatus.Show (Localization.Get ("Profile Setting Failed!"));

			Debug.Log("request error: " + request.error);
			Debug.Log("request error: " + request.responseHeaders);
		}
		else
		{
			_counter++;
			JSONNode jNodes = JSON.Parse (request.text);
            PlayerPrefs.SetString ("profile_id_"+pos, jNodes["profile"]["id"]);
			PlayerPrefs.SetString ("profile_name_"+pos, jNodes["profile"]["name"]);
			PlayerPrefs.SetString ("profile_birthday_"+pos, jNodes["profile"]["date_of_birth"]);
			PlayerPrefs.SetString ("profile_gender_"+pos, jNodes["profile"]["gender"]);
			PlayerPrefs.SetString ("profile_dominant_"+pos, jNodes["profile"]["dominant_hand"]);
			PlayerPrefs.SetString ("profile_picture_"+pos, jNodes["profile"]["picture"]);
///////////////////////////////////////////////////////
            PlayerPrefs.SetString("profile_battery_reporting_period_" + pos, jNodes["profile"]["settings"]["battery_reporting_period"]);
///////////////////////////////////////////////////////
            PlayerPrefs.Save();
//            Debug.Log("requestXXX_MP " + jNodes["profile"]["settings"]["battery_reporting_period"]);

            Debug.Log("request success");
			Debug.Log("returned data" + request.text);
			if (_counter == _eCounter) {
                SceneManager.LoadScene("home", LoadSceneMode.Single);
            }
    	}
	}
    public Texture2D textureFromSprite(Sprite sprite)
    {
        if (sprite.rect.width != sprite.texture.width)
        {
            Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
            Debug.Log("SIZE "+(int)sprite.rect.width+"..."+ (int)sprite.rect.height);
            Color[] newColors = sprite.texture.GetPixels((int)sprite.rect.x,
                (int)sprite.rect.y,
                (int)sprite.rect.width,
                (int)sprite.rect.height);
            newText.SetPixels(newColors);
            newText.Apply();
            return newText;
        }
        else
            return sprite.texture;
    }

}
