﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manage_MapWindow : MonoBehaviour {

	public GameObject scroll_content;
	public GameObject map_start;
	public GameObject map_body;

	public bool scroll_flag;
	// Use this for initialization
	void Start () {
		scroll_flag = false;
		int size = 30;

		for (int i = 0; i < size; i++) {
			GameObject clone_body = GameObject.Instantiate (map_body);
			clone_body.transform.localPosition = new Vector3 (0f, 0f, 0f);
			clone_body.transform.localScale = new Vector3 (1f, 1f, 1f);
			clone_body.transform.parent = scroll_content.transform;
			clone_body.SetActive (true);
		}
		GameObject clone_start = GameObject.Instantiate (map_start);
		clone_start.transform.parent = scroll_content.transform;
		clone_start.transform.localPosition = new Vector3 (0f, 0f, 0f);
		clone_start.transform.localScale = new Vector3 (1f, 1f, 1f);
		clone_start.SetActive (true);
	}
	
	// Update is called once per frame
	void Update () {
		if (scroll_flag) {
			if(scroll_content.GetComponent<UIScrollView> ().contentPivot == UIWidget.Pivot.Bottom)
				scroll_content.GetComponent<UIScrollView> ().contentPivot = UIWidget.Pivot.BottomLeft;
			else
				scroll_content.GetComponent<UIScrollView> ().contentPivot = UIWidget.Pivot.Bottom;
			scroll_flag = false;
		}
	}
}
