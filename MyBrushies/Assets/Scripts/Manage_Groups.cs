﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.IO;

public class Manage_Groups : MonoBehaviour {

    public GameObject[] groups;
    public GameObject[][] group_members;
    public int[] group_member_counts;
    public GameObject clone_group;
    public GameObject clone_create;
    public GameObject clone_member;

	int all_members_count=0;

    public GameObject content_parent;
    public int[][] x_y;
	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt ("members_counter", 0);
		PlayerPrefs.SetInt ("members_loaded", 0);
        PlayerPrefs.Save();
        StartCoroutine(AnalyticsJSON());
    }
	
	// Update is called once per frame
	void Update () {
        if (PlayerPrefs.GetInt("members_counter") == all_members_count)
        {
            PlayerPrefs.SetInt("members_loaded", 1);
            PlayerPrefs.Save();
        }
   }

    //analytics json
    public IEnumerator AnalyticsJSON() {
        //string groups_str = Read();
        
        WWW file = new WWW("http://dev999.com/higherstar/groups.json");
        yield return file;

        JSONNode group_data = JSON.Parse(file.text);
        groups = new GameObject[group_data["groups"].Count];
        group_members= new GameObject[group_data["groups"].Count][];
        group_member_counts = new int[group_data["groups"].Count];

        content_parent.GetComponent<Manage_Mouse_Drag>().groups = new GameObject[group_data["groups"].Count];

        for ( int i=0; i<group_data["groups"].Count; i++)
        {
            groups[i] = (GameObject)GameObject.Instantiate(clone_group, new Vector3(0, 0, 0), Quaternion.identity);
            groups[i].transform.parent = transform;
            groups[i].transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
            groups[i].transform.localPosition = new Vector3(0, 0, 0);
            groups[i].transform.FindChild("Title").transform.GetComponent<UILabel>().text = group_data["groups"][i]["name"];
            group_member_counts[i] = group_data["groups"][i]["members"].Count;
			all_members_count += group_member_counts [i];
            group_members[i] = new GameObject[group_data["groups"][i]["members"].Count];
            groups[i].GetComponent<Manage_Members>().childs = new GameObject[group_data["groups"][i]["members"].Count];
			groups[i].GetComponent<Manage_Members>().child_ids = new string[group_data["groups"][i]["members"].Count];
            groups[i].GetComponent<Manage_Members>().group_id = i;
            for (int j=0; j<group_data["groups"][i]["members"].Count; j++)
            {
				StartCoroutine(DisplayPhoto(i, j, group_data["groups"][i]["members"][j]["photo"], group_data["groups"][i]["members"][j]["id"], group_data["groups"][i]["members"][j]["name"]));
            }
			groups [i].SetActive (true);

            content_parent.GetComponent<Manage_Mouse_Drag>().groups[i] = groups[i];
        }
        transform.GetComponent<UIGrid>().enabled = true;
        clone_create.transform.parent = transform;
		clone_create.SetActive (true);
    }

	public IEnumerator DisplayPhoto(int group_id, int pos, string photo_url, string id, string name)
    {
        WWW file = new WWW(photo_url);
        yield return file;

        Texture2D sourceTex = file.texture;
        Texture2D circle_texture = new Texture2D(sourceTex.height, sourceTex.width);
        int cx = sourceTex.width / 2;
        int cy = sourceTex.height / 2;
        int r = sourceTex.height / 2;
        for (int i = (int)(cx - r); i < cx + r; i++)
        {
            for (int j = (int)(cy - r); j < cy + r; j++)
            {
                float dx = i - cx;
                float dy = j - cy;
                float d = Mathf.Sqrt(dx * dx + dy * dy);
                if (d <= r)
                    circle_texture.SetPixel(i - (int)(cx - r), j - (int)(cy - r), sourceTex.GetPixel(i, j));
                else
                    circle_texture.SetPixel(i - (int)(cx - r), j - (int)(cy - r), Color.clear);
            }
        }

        circle_texture.Apply();

        Sprite ppp = new Sprite();
        ppp = Sprite.Create(circle_texture, new Rect(0, 0, 350, 350), new Vector2(0, 0), 350.0f);

        group_members[group_id][pos] = (GameObject)GameObject.Instantiate(clone_member, new Vector3(0, 0, 0), Quaternion.identity);
		groups [group_id].GetComponent<Manage_Members> ().child_ids [pos] = id;
		group_members [group_id] [pos].GetComponent<Manage_Each_Member> ().id = id;
		group_members [group_id] [pos].GetComponent<Manage_Each_Member> ().name = name;
        group_members[group_id][pos].GetComponent<UI2DSprite>().sprite2D = ppp;
        group_members[group_id][pos].transform.parent = groups[group_id].transform;
        group_members[group_id][pos].GetComponent<UI2DSprite>().width = 45;
        group_members[group_id][pos].GetComponent<UI2DSprite>().height = 45;
        group_members[group_id][pos].transform.localPosition = new Vector3(0, 0, 0);
        group_members[group_id][pos].transform.localScale = new Vector3(1, 1, 1);
		group_members[group_id][pos].SetActive(true);
        group_members[group_id][pos].GetComponent<UI2DSprite>().depth = 300;
        groups[group_id].GetComponent<Manage_Members>().moving_flag = true;

		PlayerPrefs.SetInt ("members_counter", PlayerPrefs.GetInt ("members_counter") + 1);
        PlayerPrefs.Save();
        
    }

    string Read()
    {
        StreamReader sr;
        if (Application.isMobilePlatform)
        {
            sr = new StreamReader(Application.persistentDataPath + "/resources/groups.json");
        }
        else {
            sr = new StreamReader(Application.dataPath + "/resources/groups.json");
        }
             
        string content = sr.ReadToEnd();
        sr.Close();
        return content;
    }


}
