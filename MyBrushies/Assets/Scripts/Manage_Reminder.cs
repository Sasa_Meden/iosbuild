﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manage_Reminder : MonoBehaviour {

	public GameObject am_label;
	public GameObject am_start;
	public GameObject am_stop;
	public bool am_started=false;

	public GameObject pm_label;
	public GameObject pm_start;
	public GameObject pm_stop;
	public bool pm_started=false;

	public GameObject dialog;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void _startAM(){
		am_start.SetActive (false);
		am_stop.SetActive (true);
		am_started = false;
	}

	public void _stopAM(){
		am_start.SetActive (true);
		am_stop.SetActive (false);
		am_started = true;
	}

	public void _startPM(){
		pm_start.SetActive (false);
		pm_stop.SetActive (true);
		pm_started = false;
	}

	public void _stopPM(){
		pm_start.SetActive (true);
		pm_stop.SetActive (false);
		pm_started = true;
	}

	public void _setNotification_AM(){
		dialog.SetActive (true);
	}

	public void _setNotification_PM(){
		dialog.SetActive (true);
	}

	public void _setTime(){
		dialog.SetActive (false);
	}

	public void _cancel(){
		dialog.SetActive (false);
	}
}
