﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.SceneManagement;
public class Manage_AvatorPage : MonoBehaviour {

	public GameObject dialog;
	public int avator_index;
	public Sprite[] avators;
	public GameObject avator;
	public GameObject camera_bt;
	public GameObject loading;

	public GameObject file_browser;
	//WebCamTexture webCameTextire;
	// Use this for initialization
	void Start () {
		avator_index = -1;
		//webCameTextire = new WebCamTexture ();
		//renderer.material.mainTexture = webCameTextire;
	}
	
	// Update is called once per frame
	void Update () {
		if (avator_index != -1) {
			avator.GetComponent<UI2DSprite> ().sprite2D = avators [avator_index];
			avator.SetActive (true);
			camera_bt.SetActive (false);
		}
	}

	public void goto_next(){
        //loading.GetComponent<Manage_Loading> ().flag = 1;
        //loading.GetComponent<Manage_Loading> ().time = 5f;
        //loading.SetActive (true);
        SceneManager.LoadScene("home", LoadSceneMode.Single);
       
	}

	public void show_photo_dialog(){
		dialog.SetActive (true);
	}

	public void take_photo(){
		dialog.SetActive (false);
		PlayerPrefs.SetInt ("selected_tkphoto_index", PlayerPrefs.GetInt ("temp_avator_index"));
        PlayerPrefs.Save();
        SceneManager.LoadScene("camera", LoadSceneMode.Single);
        
	}

	public void choose_photo_library(){
		file_browser.SetActive (true);
		dialog.SetActive (false);
	}

	public void choose_avator(){
		dialog.SetActive (false);
	}

	public void cancel(){
		dialog.SetActive (false);
	}
		
}
