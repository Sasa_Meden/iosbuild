﻿using UnityEngine;
using System.Collections;

public class Manage_Avator_Item : MonoBehaviour {

	public int index;
	public GameObject border;
	public GameObject[] other_borders;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick(){
		int select_index = PlayerPrefs.GetInt ("temp_avator_index");
		PlayerPrefs.SetInt ("profile_avator_"+select_index, index);
        PlayerPrefs.Save();
		Debug.Log ("profile avator select index: "+select_index);
		Debug.Log ("profile avator index: " + index);
		for (int i = 0; i < other_borders.Length; i++) {
			other_borders [i].SetActive (false);
		}
		border.SetActive (true);
	}
}
