﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Manage_MapWindowDialog : MonoBehaviour {

	public GameObject loading;
    
	// Use this for initialization
	void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {

       if (Input.GetKeyDown(KeyCode.Escape))
        {
            GotoHome();
        }
    }

    public void GotoHome(){
		loading.SetActive (true);
        SceneManager.LoadScene("home", LoadSceneMode.Single);
    }
}
