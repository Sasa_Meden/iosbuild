﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PearlyReactions : MonoBehaviour {
    private Animator animPearly = new Animator();

    /// <summary>
    /// Pearly reaction definitions
    /// </summary>

    public AudioClip goodLuck1;
    public AudioClip goodLuck2;

    private string[] PopBalloonsTrue = { "You got it!", "Beautifully popped!" };
    private int LastPopBalloonsTrue = -1;
    public AudioClip youGotIt1;
    public AudioClip youGotIt2;

    public AudioClip beautifullyPopped1;
    public AudioClip beautifullyPopped2;
    public AudioClip beautifullyPopped3;
    public AudioClip beautifullyPopped4;

    private string[] PopBalloonsFalse = { "Pop that balloon!", "Don’t let it get away!" };
    private int LastPopBalloonsFalse = -1;
    public AudioClip popThatBalloon1;
    public AudioClip popThatBalloon2;
    public AudioClip popThatBalloon3;

    public AudioClip dontLetItGetAway1;
    public AudioClip dontLetItGetAway2;
    public AudioClip dontLetItGetAway3;

    private string[] PositiveMid = { "Fantastic job!", "You got this!", "You’re so cool!", "Your teeth love you so much right now!", "Keep going!" };
    private int LastPositiveMid = -1;
    public AudioClip fantasticJob1;
    public AudioClip fantasticJob2;
    public AudioClip fantasticJob3;

    public AudioClip youGotThis1;
    public AudioClip youGotThis2;
    public AudioClip youGotThis3;
    public AudioClip youGotThis4;

    public AudioClip youreSoCool1;
    public AudioClip youreSoCool2;
    public AudioClip youreSoCool3;
    public AudioClip youreSoCool4;

    public AudioClip yourTeethLoveYou1;
    public AudioClip yourTeethLoveYou2;

    public AudioClip keepGoing1;
    public AudioClip keepGoing2;
    public AudioClip keepGoing3;

    private string[] NegativeMid = { "Where did you go?", "I’m trying not to cry", "I miss you", "I’m not needy but I miss you", "Ever seen a penguin cry?", "Hey, where are you?", "Am I talking to myself?", "We’re not finished here" };
    private int LastNegativeMid = -1;
    public AudioClip whereDidYouGo1;
    public AudioClip whereDidYouGo2;

    public AudioClip tryingNotToCry1;
    public AudioClip tryingNotToCry2;
    public AudioClip tryingNotToCry3;

    public AudioClip iMissYou1;
    public AudioClip iMissYou2;

    public AudioClip notNeedy1;
    public AudioClip notNeedy2;

    public AudioClip penguinCry1;
    public AudioClip penguinCry2;

    public AudioClip whereAreYou1;
    public AudioClip whereAreYou2;

    public AudioClip talkingToMyself1;
    public AudioClip talkingToMyself2;

    public AudioClip notFinished1;
    public AudioClip notFinished2;

    private string[] PositivePost = { "Well done!", "Congratulations!", "You’re the best!", "Now, that’s what I call a game!" };
    private int LastPositivePost = -1;
    public AudioClip wellDone1;
    public AudioClip wellDone2;
    public AudioClip wellDone3;

    public AudioClip congratulations1;
    public AudioClip congratulations2;

    public AudioClip youreTheBest1;
    public AudioClip youreTheBest2;

    public AudioClip thatsWhatICallAGame1;
    public AudioClip thatsWhatICallAGame2;

    private string[] NegativePoolMid = { "Don’t let them win!", "Uh-o! Watch out!", "Get those germs!" };
    private int LastNegativePoolMid = -1;
    public AudioClip dontLetThemWin1;
    public AudioClip dontLetThemWin2;
    public AudioClip dontLetThemWin3;

    public AudioClip watchOut1;
    public AudioClip watchOut2;
    public AudioClip watchOut3;

    public AudioClip getThoseGerms1;
    public AudioClip getThoseGerms2;
    public AudioClip getThoseGerms3;



    public AudioClip youWon1;
    public AudioClip youWon2;

    public AudioClip ready1;
    public AudioClip ready2;

    public AudioClip steady1;
    public AudioClip steady2;

    public AudioClip go1;
    public AudioClip go2;

    public AudioClip toothpaste1;
    public AudioClip toothpaste2;

    public AudioClip tongue1;
    public AudioClip tongue2;
    public AudioClip tongue3;
    public AudioClip tongue4;
    public AudioClip tongue5;

    public AudioClip lowerLeft;
    public AudioClip lowerRight;
    public AudioClip upperLeft;
    public AudioClip upperRight;


    /// <summary>
    /// Audio sources for Pearly
    /// </summary>

    private float Volume = 1.0f;
    private static AudioSource audioGoodLuck1;
    private static AudioSource audioGoodLuck2;

    private static AudioSource audioYouGotIt1;
    private static AudioSource audioYouGotIt2;

    private static AudioSource audioBeautifullyPopped1;
    private static AudioSource audioBeautifullyPopped2;
    private static AudioSource audioBeautifullyPopped3;
    private static AudioSource audioBeautifullyPopped4;

    private static AudioSource audioPopThatBalloon1;
    private static AudioSource audioPopThatBalloon2;
    private static AudioSource audioPopThatBalloon3;

    private static AudioSource audioDontLetItGetAway1;
    private static AudioSource audioDontLetItGetAway2;
    private static AudioSource audioDontLetItGetAway3;

    private static AudioSource audioFantasticJob1;
    private static AudioSource audioFantasticJob2;
    private static AudioSource audioFantasticJob3;

    private static AudioSource audioYouGotThis1;
    private static AudioSource audioYouGotThis2;
    private static AudioSource audioYouGotThis3;
    private static AudioSource audioYouGotThis4;

    private static AudioSource audioYoureSoCool1;
    private static AudioSource audioYoureSoCool2;
    private static AudioSource audioYoureSoCool3;
    private static AudioSource audioYoureSoCool4;

    private static AudioSource audioYourTeethLoveYou1;
    private static AudioSource audioYourTeethLoveYou2;

    private static AudioSource audioKeepGoing1;
    private static AudioSource audioKeepGoing2;
    private static AudioSource audioKeepGoing3;

    private static AudioSource audioWhereDidYouGo1;
    private static AudioSource audioWhereDidYouGo2;

    private static AudioSource audioTryingNotToCry1;
    private static AudioSource audioTryingNotToCry2;
    private static AudioSource audioTryingNotToCry3;

    private static AudioSource audioIMissYou1;
    private static AudioSource audioIMissYou2;

    private static AudioSource audioNotNeedy1;
    private static AudioSource audioNotNeedy2;

    private static AudioSource audioPenguinCry1;
    private static AudioSource audioPenguinCry2;

    private static AudioSource audioWhereAreYou1;
    private static AudioSource audioWhereAreYou2;

    private static AudioSource audioTalkingToMyself1;
    private static AudioSource audioTalkingToMyself2;

    private static AudioSource audioNotFinished1;
    private static AudioSource audioNotFinished2;

    private static AudioSource audioWellDone1;
    private static AudioSource audioWellDone2;
    private static AudioSource audioWellDone3;

    private static AudioSource audioCongratulations1;
    private static AudioSource audioCongratulations2;

    private static AudioSource audioYoureTheBest1;
    private static AudioSource audioYoureTheBest2;

    private static AudioSource audioThatsWhatICallAGame1;
    private static AudioSource audioThatsWhatICallAGame2;

    private static AudioSource audioDontLetThemWin1;
    private static AudioSource audioDontLetThemWin2;
    private static AudioSource audioDontLetThemWin3;

    private static AudioSource audioWatchOut1;
    private static AudioSource audioWatchOut2;
    private static AudioSource audioWatchOut3;

    private static AudioSource audioGetThoseGerms1;
    private static AudioSource audioGetThoseGerms2;
    private static AudioSource audioGetThoseGerms3;

    private static AudioSource audioYouWon1;
    private static AudioSource audioYouWon2;

    private static AudioSource audioReady1;
    private static AudioSource audioReady2;

    private static AudioSource audioSteady1;
    private static AudioSource audioSteady2;

    private static AudioSource audioGo1;
    private static AudioSource audioGo2;

    private static AudioSource audioToothpaste1;
    private static AudioSource audioToothpaste2;

    private static AudioSource audioTongue1;
    private static AudioSource audioTongue2;
    private static AudioSource audioTongue3;
    private static AudioSource audioTongue4;
    private static AudioSource audioTongue5;

    private static AudioSource audioUpperLeft;
    private static AudioSource audioUpperRight;
    private static AudioSource audioLowerLeft;
    private static AudioSource audioLowerRight;

    public void Awake()
    {

        
       
        
        // add the necessary AudioSources:
        audioGoodLuck1 = AddAudio(goodLuck1, false, false, Volume);
        audioGoodLuck2 = AddAudio(goodLuck2, false, false, Volume);

        audioYouGotIt1 = AddAudio(youGotIt1, false, false, Volume);
        audioYouGotIt2 = AddAudio(youGotIt2, false, false, Volume);

        audioBeautifullyPopped1 = AddAudio(beautifullyPopped1, false, false, Volume);
        audioBeautifullyPopped2 = AddAudio(beautifullyPopped2, false, false, Volume);
        audioBeautifullyPopped3 = AddAudio(beautifullyPopped3, false, false, Volume);
        audioBeautifullyPopped4 = AddAudio(beautifullyPopped4, false, false, Volume);

        audioPopThatBalloon1 = AddAudio(popThatBalloon1, false, false, Volume);
        audioPopThatBalloon2 = AddAudio(popThatBalloon2, false, false, Volume);
        audioPopThatBalloon3 = AddAudio(popThatBalloon3, false, false, Volume);

        audioDontLetItGetAway1 = AddAudio(dontLetItGetAway1, false, false, Volume);
        audioDontLetItGetAway2 = AddAudio(dontLetItGetAway1, false, false, Volume);
        audioDontLetItGetAway3 = AddAudio(dontLetItGetAway1, false, false, Volume);

        audioFantasticJob1 = AddAudio(fantasticJob1, false, false, Volume);
        audioFantasticJob2 = AddAudio(fantasticJob2, false, false, Volume);
        audioFantasticJob3 = AddAudio(fantasticJob3, false, false, Volume);

        audioYouGotThis1 = AddAudio(youGotThis1, false, false, Volume);
        audioYouGotThis2 = AddAudio(youGotThis2, false, false, Volume);
        audioYouGotThis3 = AddAudio(youGotThis3, false, false, Volume);
        audioYouGotThis4 = AddAudio(youGotThis4, false, false, Volume);

        audioYoureSoCool1 = AddAudio(youreSoCool1, false, false, Volume);
        audioYoureSoCool2 = AddAudio(youreSoCool2, false, false, Volume);
        audioYoureSoCool3 = AddAudio(youreSoCool3, false, false, Volume);
        audioYoureSoCool4 = AddAudio(youreSoCool4, false, false, Volume);

        audioYourTeethLoveYou1 = AddAudio(yourTeethLoveYou1, false, false, Volume);
        audioYourTeethLoveYou2 = AddAudio(yourTeethLoveYou2, false, false, Volume);

        audioKeepGoing1 = AddAudio(keepGoing1, false, false, Volume);
        audioKeepGoing2 = AddAudio(keepGoing2, false, false, Volume);
        audioKeepGoing3 = AddAudio(keepGoing3, false, false, Volume);

        audioWhereDidYouGo1 = AddAudio(whereDidYouGo1, false, false, Volume);
        audioWhereDidYouGo2 = AddAudio(whereDidYouGo2, false, false, Volume);

        audioTryingNotToCry1 = AddAudio(tryingNotToCry1, false, false, Volume);
        audioTryingNotToCry2 = AddAudio(tryingNotToCry2, false, false, Volume);
        audioTryingNotToCry3 = AddAudio(tryingNotToCry3, false, false, Volume);

        audioIMissYou1 = AddAudio(iMissYou1, false, false, Volume);
        audioIMissYou2 = AddAudio(iMissYou2, false, false, Volume);

        audioNotNeedy1 = AddAudio(notNeedy1, false, false, Volume);
        audioNotNeedy2 = AddAudio(notNeedy2, false, false, Volume);

        audioPenguinCry1 = AddAudio(penguinCry1, false, false, Volume);
        audioPenguinCry2 = AddAudio(penguinCry2, false, false, Volume);

        audioWhereAreYou1 = AddAudio(whereAreYou1, false, false, Volume);
        audioWhereAreYou2 = AddAudio(whereAreYou2, false, false, Volume);

        audioTalkingToMyself1 = AddAudio(talkingToMyself1, false, false, Volume);
        audioTalkingToMyself2 = AddAudio(talkingToMyself2, false, false, Volume);

        audioNotFinished1 = AddAudio(notFinished1, false, false, Volume);
        audioNotFinished2 = AddAudio(notFinished2, false, false, Volume);

        audioWellDone1 = AddAudio(wellDone1, false, false, Volume);
        audioWellDone2 = AddAudio(wellDone2, false, false, Volume);
        audioWellDone3 = AddAudio(wellDone3, false, false, Volume);

        audioCongratulations1 = AddAudio(congratulations1, false, false, Volume);
        audioCongratulations2 = AddAudio(congratulations2, false, false, Volume);

        audioYoureTheBest1 = AddAudio(youreTheBest1, false, false, Volume);
        audioYoureTheBest2 = AddAudio(youreTheBest2, false, false, Volume);

        audioThatsWhatICallAGame1 = AddAudio(thatsWhatICallAGame1, false, false, Volume);
        audioThatsWhatICallAGame2 = AddAudio(thatsWhatICallAGame2, false, false, Volume);

        audioDontLetThemWin1 = AddAudio(dontLetThemWin1, false, false, Volume);
        audioDontLetThemWin2 = AddAudio(dontLetThemWin2, false, false, Volume);
        audioDontLetThemWin3 = AddAudio(dontLetThemWin3, false, false, Volume);

        audioWatchOut1 = AddAudio(watchOut1, false, false, Volume);
        audioWatchOut2 = AddAudio(watchOut2, false, false, Volume);
        audioWatchOut3 = AddAudio(watchOut3, false, false, Volume);

        audioGetThoseGerms1 = AddAudio(getThoseGerms1, false, false, Volume);
        audioGetThoseGerms2 = AddAudio(getThoseGerms2, false, false, Volume);
        audioGetThoseGerms3 = AddAudio(getThoseGerms3, false, false, Volume);

        audioYouWon1 = AddAudio(youWon1, false, false, Volume);
        audioYouWon2 = AddAudio(youWon2, false, false, Volume);

        audioReady1 = AddAudio(ready1, false, false, Volume);
        audioReady2 = AddAudio(ready2, false, false, Volume);

        audioSteady1 = AddAudio(steady1, false, false, Volume);
        audioSteady2 = AddAudio(steady2, false, false, Volume);

        audioGo1 = AddAudio(go1, false, false, Volume);
        audioGo2 = AddAudio(go2, false, false, Volume);

        audioToothpaste1 = AddAudio(toothpaste1, false, false, Volume);
        audioToothpaste2 = AddAudio(toothpaste2, false, false, Volume);

        audioTongue1 = AddAudio(tongue1, false, false, Volume);
        audioTongue2 = AddAudio(tongue2, false, false, Volume);
        audioTongue3 = AddAudio(tongue3, false, false, Volume);
        audioTongue4 = AddAudio(tongue4, false, false, Volume);
        audioTongue5 = AddAudio(tongue5, false, false, Volume);

        audioUpperLeft = AddAudio(upperLeft, false, false, Volume);
        audioUpperRight = AddAudio(upperRight, false, false, Volume);

        audioLowerLeft = AddAudio(lowerLeft, false, false, Volume);
        audioLowerRight = AddAudio(lowerRight, false, false, Volume);
    }


    // Use this for initialization
    void Start () {
       
       animPearly = this.GetComponent<Animator>();
       PearlyStay();

    }
    /// <summary>
    /// Add audio sources for Pearly function
    /// </summary>
    /// <param name="clip"></param>
    /// <param name="loop"></param>
    /// <param name="playAwake"></param>
    /// <param name="vol"></param>
    /// <returns></returns>
    public AudioSource AddAudio(AudioClip clip, bool loop, bool playAwake, float vol)
    {
        AudioSource newAudio = gameObject.AddComponent<AudioSource>();

        newAudio.clip = clip;
        newAudio.loop = loop;
        newAudio.playOnAwake = playAwake;
        newAudio.volume = vol;
        return newAudio;

    }

    /// <summary>
    /// Getter for Pearly reactions
    /// </summary>
    /// <returns></returns>
    public string getGoodLuck()
    {
        int aud = Random.Range(0, 2);

        if (aud == 0)
        {
            audioGoodLuck1.Play();
        }
        else
        {
            audioGoodLuck2.Play();
        }
        PearlyWave();
       
        return "Good luck!";
    }

    public string getPopBalloonsTrue()
    {
        int pos = Random.Range(0, PopBalloonsTrue.Length);

        while(LastPopBalloonsTrue == pos)
        {
            pos = Random.Range(0, PopBalloonsTrue.Length);
        }

        LastPopBalloonsTrue = pos;

        if (pos == 0)
        {
            int aud = Random.Range(0, 2);
            if (aud == 0)
            {
                PearlyWave();
                audioYouGotIt1.Play();
            }
            else
            {
                PearlyJump();
                audioYouGotIt2.Play();
            }
        }
        else
        {
            int aud = Random.Range(0, 4);

            switch (aud)
            {
                case 0:
                    audioBeautifullyPopped1.Play();
                    PearlyWave();
                    break;
                case 1:
                    audioBeautifullyPopped2.Play();
                    PearlyJump();
                    break;
                case 2:
                    audioBeautifullyPopped3.Play();
                    PearlyWave();
                    break;
                case 3:
                    audioBeautifullyPopped4.Play();
                    PearlyJump();
                    break;
            }


        }

        return PopBalloonsTrue[pos];
    }

    public string getPopBalloonsFalse()
    {
        int pos = Random.Range(0, PopBalloonsFalse.Length);

        while(LastPopBalloonsFalse == pos)
        {
            pos = Random.Range(0, PopBalloonsFalse.Length);
        }

        LastPopBalloonsFalse = pos;

        if (pos == 0)
        {
            int aud = Random.Range(0, 3);

            switch (aud)
            {
                case 0:
                    audioPopThatBalloon1.Play();
                    PearlyWave();
                    break;
                case 1:
                    audioPopThatBalloon2.Play();
                    PearlyWave();
                    break;
                case 2:
                    audioPopThatBalloon3.Play();
                    PearlyWave();
                    break;
            }

        }
        else
        {
            int aud = Random.Range(0, 3);
            switch (aud)
            {
                case 0:
                    audioDontLetItGetAway1.Play();
                    PearlyWave();
                    break;
                case 1:
                    audioDontLetItGetAway2.Play();
                    PearlyWave();
                    break;
                case 2:
                    audioDontLetItGetAway3.Play();
                    PearlyWave();
                    break;
            }
        }

        return PopBalloonsFalse[pos];
    }

    public string getPositiveMid()
    {
        int pos = Random.Range(0, PositiveMid.Length);

        while(LastPositiveMid == pos)
        {
            pos = Random.Range(0, PositiveMid.Length);
        }

        int aud = 0;
        switch (pos)
        {
            case 0:
                aud = Random.Range(0, 3);
                switch (aud)
                {
                    case 0:
                        audioFantasticJob1.Play();
                        PearlyJump();
                        break;
                    case 1:
                        audioFantasticJob2.Play();
                        PearlyWave();
                        break;
                    case 2:
                        audioFantasticJob3.Play();
                        PearlyJump();
                        break;
                }
                break;
            case 1:
                aud = Random.Range(0, 4);
                switch (aud)
                {
                    case 0:
                        audioYouGotThis1.Play();
                        PearlyJump();
                        break;
                    case 1:
                        audioYouGotThis2.Play();
                        PearlyWave();
                        break;
                    case 2:
                        audioYouGotThis3.Play();
                        PearlyJump();
                        break;
                    case 3:
                        audioYouGotThis4.Play();
                        PearlyWave();
                        break;
                }
                break;
            case 2:
                aud = Random.Range(0, 4);
                switch (aud)
                {
                    case 0:
                        audioYoureSoCool1.Play();
                        PearlyWave();
                        break;
                    case 1:
                        audioYoureSoCool2.Play();
                        PearlyWave();
                        break;
                    case 2:
                        audioYoureSoCool3.Play();
                        PearlyWave();
                        break;
                    case 3:
                        audioYoureSoCool4.Play();
                        PearlyWave();
                        break;
                }
                break;
            case 3:
                aud = Random.Range(0, 2);
                switch (aud)
                {
                    case 0:
                        audioYourTeethLoveYou1.Play();
                        PearlyWave();
                        break;
                    case 1:
                        audioYourTeethLoveYou2.Play();
                        PearlyJump();
                        break;
                }
                break;
            case 4:
                aud = Random.Range(0, 3);
                switch (aud)
                {
                    case 0:
                        audioKeepGoing1.Play();
                        PearlyWave();
                        break;
                    case 1:
                        audioKeepGoing2.Play();
                        PearlyWave();
                        break;
                    case 2:
                        audioKeepGoing3.Play();
                        PearlyWave();
                        break;
                }
                break;
        }
       
        return PositiveMid[pos];
    }

    public string getNegativeMid()
    {
        int pos = Random.Range(0, NegativeMid.Length);

        while(LastNegativeMid == pos)
        {
            pos = Random.Range(0, NegativeMid.Length);
        }

        int aud = 0;
        switch (pos)
        {
            case 0:
                aud = Random.Range(0, 2);
                switch (aud)
                {
                    case 0:
                        audioWhereDidYouGo1.Play();
                        break;
                    case 1:
                        audioWhereDidYouGo2.Play();
                        break;
                }
                break;
            case 1:
                aud = Random.Range(0, 3);
                switch (aud)
                {
                    case 0:
                        audioTryingNotToCry1.Play();
                        break;
                    case 1:
                        audioTryingNotToCry2.Play();
                        break;
                    case 2:
                        audioTryingNotToCry3.Play();
                        break;
                }
                break;
            case 2:
                aud = Random.Range(0, 2);
                switch (aud)
                {
                    case 0:
                        audioIMissYou1.Play();
                        break;
                    case 1:
                        audioIMissYou2.Play();
                        break;
                }
                break;
            case 3:
                aud = Random.Range(0, 2);
                switch (aud)
                {
                    case 0:
                        audioNotNeedy1.Play();
                        break;
                    case 1:
                        audioNotNeedy2.Play();
                        break;
                }
                break;
            case 4:
                aud = Random.Range(0, 2);
                switch (aud)
                {
                    case 0:
                        audioPenguinCry1.Play();
                        break;
                    case 1:
                        audioPenguinCry2.Play();
                        break;
                }
                break;
            case 5:
                aud = Random.Range(0, 2);
                switch (aud)
                {
                    case 0:
                        audioWhereAreYou1.Play();
                        break;
                    case 1:
                        audioWhereAreYou2.Play();
                        break;
                }
                break;
            case 6:
                aud = Random.Range(0, 2);
                switch (aud)
                {
                    case 0:
                        audioTalkingToMyself1.Play();
                        break;
                    case 1:
                        audioTalkingToMyself2.Play();
                        break;
                }
                break;
            case 7:
                aud = Random.Range(0, 2);
                switch (aud)
                {
                    case 0:
                        audioNotFinished1.Play();
                        break;
                    case 1:
                        audioNotFinished2.Play();
                        break;
                }
                break;
        }
        PearlySad();
        return NegativeMid[pos];
    }

    public string getPositivePost()
    {
        int pos = Random.Range(0, PositivePost.Length);

        while(LastPositivePost == pos)
        {
            pos = Random.Range(0, PositivePost.Length);
        }

        int aud = 0;

        switch (pos)
        {
            case 0:
                aud = Random.Range(0, 3);
                switch (aud)
                {
                    case 0:
                        audioWellDone1.Play();
                        break;
                    case 1:
                        audioWellDone2.Play();
                        break;
                    case 2:
                        audioWellDone3.Play();
                        break;
                }
                break;
            case 1:
                aud = Random.Range(0, 2);
                switch (aud)
                {
                    case 0:
                        audioCongratulations1.Play();
                        break;
                    case 1:
                        audioCongratulations2.Play();
                        break;
                }
                break;
            case 2:
                aud = Random.Range(0, 2);
                switch (aud)
                {
                    case 0:
                        audioYoureTheBest1.Play();
                        break;
                    case 1:
                        audioYoureTheBest2.Play();
                        break;
                }
                break;
            case 3:
                aud = Random.Range(0, 2);
                switch (aud)
                {
                    case 0:
                        audioThatsWhatICallAGame1.Play();
                        break;
                    case 1:
                        audioThatsWhatICallAGame2.Play();
                        break;
                }
                break;
        }
        PearlyJump();
        return PositivePost[pos];
    }

    public string getNegativePoolMid()
    {
        int pos = Random.Range(0, NegativePoolMid.Length);

        while(LastNegativePoolMid == pos)
        {
            pos = Random.Range(0, NegativePoolMid.Length);
        }

        int aud = 0;

        switch (pos)
        {
            case 0:
                aud = Random.Range(0, 3);
                switch (aud)
                {
                    case 0:
                        audioDontLetThemWin1.Play();
                        break;
                    case 1:
                        audioDontLetThemWin2.Play();
                        break;
                    case 2:
                        audioDontLetThemWin3.Play();
                        break;
                }
                break;
            case 1:
                aud = Random.Range(0, 3);
                switch (aud)
                {
                    case 0:
                        audioWatchOut1.Play();
                        break;
                    case 1:
                        audioWatchOut2.Play();
                        break;
                    case 2:
                        audioWatchOut3.Play();
                        break;
                }
                break;
            case 2:
                aud = Random.Range(0, 3);
                switch (aud)
                {
                    case 0:
                        audioGetThoseGerms1.Play();
                        break;
                    case 1:
                        audioGetThoseGerms2.Play();
                        break;
                    case 2:
                        audioGetThoseGerms3.Play();
                        break;
                }
                break;
        }
        PearlySad();
        return NegativePoolMid[pos];
    }

    public string getPositivePoolEnd()
    {

        int aud = Random.Range(0, 2);

        if (aud == 0)
        {
            audioYouWon1.Play();
        }
        else
        {
            audioYouWon2.Play();
        }
        PearlyJump();
        return "Yay, you won!";
    }

    public string getReady()
    {
        int aud = Random.Range(0, 2);

        if (aud == 0)
        {
            audioReady1.Play();
        }
        else
        {
            audioReady2.Play();
        }
        return "Ready";
    }

    public string getSteady()
    {
        int aud = Random.Range(0, 2);

        if (aud == 0)
        {
            audioSteady1.Play();
        }
        else
        {
            audioSteady2.Play();
        }
        return "Steady";
    }

    public string getGo()
    {
        int aud = Random.Range(0, 2);

        if (aud == 0)
        {
            audioGo1.Play();
        }
        else
        {
            audioGo2.Play();
        }
        PearlyWave();
        return "GO!";
    }

    public string getToothpaste()
    {
        int aud = Random.Range(0, 2);

        if (aud == 0)
        {
            audioToothpaste1.Play();
        }
        else
        {
            audioToothpaste2.Play();
        }
        PearlyWave();
        return "Got toothpaste?";
    }

    public string getTongue()
    {
        int aud = Random.Range(0, 5);

        switch (aud)
        {
            case 0:
                audioTongue1.Play();
                break;
            case 1:
                audioTongue2.Play();
                break;
            case 2:
                audioTongue3.Play();
                break;
            case 3:
                audioTongue4.Play();
                break;
            case 4:
                audioTongue5.Play();
                break;
        }
        PearlyWave();
        return "Brush your tongue!";
    }

    public string getUpperLeft()
    {
        audioUpperLeft.Play();
        PearlyWave();
        return "Upper left";
    }

    public string getUpperRight()
    {
        audioUpperRight.Play();
        PearlyWave();
        return "Upper right";
    }

    public string getLowerLeft()
    {
        audioLowerLeft.Play();
        PearlyWave();
        return "Lower left";
    }

    public string getLowerRight()
    {
        audioLowerRight.Play();
        PearlyWave();
        return "Lower right";
    }
    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.W))
        {
            PearlyWave();
            
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            PearlyJump();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            PearlySad();
        }
    }

    public void PearlyWave()
    {
        //Debug.Log("wave");
        animPearly.Play("Wave", 0, 0f);
        // PearlyStay();
    }

    public void PearlyJump()
    {
        //Debug.Log("jump");
        animPearly.Play("Jump", 0, 0f);
        // PearlyStay();
    }

    public void PearlySad()
    {
       // Debug.Log("sad");
        animPearly.Play("Sad", 0, 0f);
        // PearlyStay();
    }

    public void PearlyStay()
    {
        // Debug.Log("sad");
        animPearly.Play("Stay", 0, 0f);

    }
   
}
