﻿using UnityEngine;
using System.Collections;

public class Manage_Mouse_Drag : MonoBehaviour {

    public bool flag = false;
    public GameObject drag_friend;
    public GameObject group_view;
    public GameObject[] groups;
    public int match_group_id=-1;
	public bool group_in_flag=false;
	public bool group_out_flag=false;
	public bool exist_flag=false;

    [System.NonSerialized] Transform mTrans;
    [System.NonSerialized] UITexture mUITex;
    [System.NonSerialized] Texture2D mTex;
    [System.NonSerialized] UICamera mCam;
    [System.NonSerialized] Vector2 mPos;

    public Vector3 test_friend_position;
    public Vector3 test_group_position;
    // Use this for initialization
    void Start () {
        mTrans = transform;
        mUITex = GetComponent<UITexture>();
        mCam = UICamera.FindCameraForLayer(gameObject.layer);

        match_group_id = -1;
    }
	
	// Update is called once per frame
	void Update () {
        if (flag)
            Sample();
	}

    void OnPress(bool pressed) {
        Debug.Log("Mouse Drag Pressed");
        if (enabled && flag && pressed && UICamera.currentScheme != UICamera.ControlScheme.Controller) Sample();
        if (!pressed)
        {
            Destroy(drag_friend);

        }
    }
    void OnDrag(Vector2 delta) {
        if (enabled && flag)
        {
            Sample();
        }

    }
    void OnPan(Vector2 delta)
    {
        if (enabled && flag)
        {
            mPos.x = Mathf.Clamp01(mPos.x + delta.x);
            mPos.y = Mathf.Clamp01(mPos.y + delta.y);
            Select(mPos);
        }
    }

    void Sample()
    {
        Vector3 pos = UICamera.lastEventPosition;
        pos = mCam.cachedCamera.ScreenToWorldPoint(pos);

        pos = mTrans.InverseTransformPoint(pos);
        Vector3[] corners = mUITex.localCorners;
        mPos.x = Mathf.Clamp01((pos.x - corners[0].x) / (corners[2].x - corners[0].x));
        mPos.y = Mathf.Clamp01((pos.y - corners[0].y) / (corners[2].y - corners[0].y));

        if (drag_friend != null)
        {
            pos.x = Mathf.Lerp(corners[0].x, corners[2].x, mPos.x);
            pos.y = Mathf.Lerp(corners[0].y, corners[2].y, mPos.y);
            pos = mTrans.TransformPoint(pos);
            drag_friend.transform.OverlayPosition(pos, mCam.cachedCamera);
            test_friend_position = drag_friend.transform.position;
            test_group_position = groups[0].transform.position;
            if (distance_detect())
            {
                Debug.Log("Match:" + match_group_id);
				if (!in_array (groups [match_group_id].GetComponent<Manage_Members> ().child_ids, drag_friend.GetComponent<Manage_Drag_Object> ().id)) {
					groups [match_group_id].GetComponent<Manage_Members> ().forward_flag = true;
					groups [match_group_id].GetComponent<Manage_Members> ().clone_obj = drag_friend;
					exist_flag = false;
				} else {
					exist_flag = true;
				}
            }else
            {
                if (match_group_id != -1)
                {
                    groups[match_group_id].GetComponent<Manage_Members>().forward_flag = false;
					groups [match_group_id].GetComponent<Manage_Members> ().clone_obj = null;
                    match_group_id = -1;
					group_in_flag = false;
                }
            }
        }
        
    }

    public bool distance_detect() {
        bool flag = false;
        float w = (groups[1].transform.position.x - groups[0].transform.position.x) / 2;
        for (int i = 0; i < groups.Length; i++) {
			if (Vector3.Distance (groups [i].transform.position, drag_friend.transform.position) < w) {
				flag = true;
				match_group_id = i;
				if (groups [match_group_id].GetComponent<Manage_Each_Group> ().trigger_flag!=1 && groups [match_group_id].GetComponent<Manage_Each_Group> ().trigger_flag!=0) {

					if (groups [i].GetComponent<Manage_Members> ().childs.Length == 5) {
						groups [match_group_id].GetComponent<Manage_Each_Group> ().trigger_flag = 0;
						UIStatus.Show(Localization.Get("Maximum Size of This Group"));
					}else if(in_array(groups [i].GetComponent<Manage_Members> ().child_ids, drag_friend.GetComponent<Manage_Drag_Object>().id)){
						groups [match_group_id].GetComponent<Manage_Each_Group> ().trigger_flag = 0;
						UIStatus.Show(Localization.Get(drag_friend.GetComponent<Manage_Drag_Object>().name+" Already Exists In This Group!"));
					}
					else
						groups [match_group_id].GetComponent<Manage_Each_Group> ().trigger_flag = 1;
					
					groups [match_group_id].GetComponent<Manage_Each_Group> ().flag = true;
					group_in_flag = true;

				}
			} else {
				if (groups [i].GetComponent<Manage_Each_Group> ().trigger_flag==1) {
					groups [i].GetComponent<Manage_Each_Group> ().trigger_flag = 2;
					groups [i].GetComponent<Manage_Each_Group> ().flag = true;
				}
				if (groups [i].GetComponent<Manage_Each_Group> ().trigger_flag==0) {
					groups [i].GetComponent<Manage_Each_Group> ().trigger_flag = 2;
				}
				groups[i].GetComponent<Manage_Members>().forward_flag = false;
			}
        }
        return flag;
    }

	public bool in_array(string[] arr, string element){
		for(int i=0; i<arr.Length; i++){
			if (arr [i] == element)
				return true;
		}
		return false;
	}

    public void Select(Vector2 v)
    {
        v.x = Mathf.Clamp01(v.x);
        v.y = Mathf.Clamp01(v.y);
        mPos = v;

        if (drag_friend != null)
        {
            Vector3[] corners = mUITex.localCorners;
            v.x = Mathf.Lerp(corners[0].x, corners[2].x, mPos.x);
            v.y = Mathf.Lerp(corners[0].y, corners[2].y, mPos.y);
            v = mTrans.TransformPoint(v);
            drag_friend.transform.OverlayPosition(v, mCam.cachedCamera);
        }
        
    }
}
