﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manage_AvatorProfile : MonoBehaviour {

	public int index = 0;
	public GameObject button_dialog;
	public GameObject title;
	public GameObject parent_obj;
	public Sprite[] avators;
	public GameObject single_circle1;
	public GameObject single_circle2;
	public GameObject photo_circle;
	public string[] personal_data;

	public float debug_x;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		string[][] data = parent_obj.GetComponent<Manage_Profiles> ().profile_datas;
		debug_x = transform.position.x;
		if (transform.position.x <(0.008f) && transform.position.x >(-0.0009f)) {
			title.GetComponent<UILabel> ().text = "Hi "+personal_data [1]+"!";
		}

		if (PlayerPrefs.GetInt ("selected_tkphoto_index", -1) != index) {
			string image_path = "file://"+PlayerPrefs.GetString ("selected_tkphoto_link_" + index, "none");
			if (image_path != "file://none") {
				Debug.Log (image_path);
				StartCoroutine (DisplayPhoto (image_path, 350, 350));
			}
			PlayerPrefs.SetInt ("selected_tkphoto_index", -1);
            PlayerPrefs.Save();
		}

		if (PlayerPrefs.GetInt ("profile_avator_" + index, -1) !=-1) {
			transform.GetComponent<UI2DSprite> ().sprite2D = avators [PlayerPrefs.GetInt ("profile_avator_" + index, -1)];
//			transform.GetComponent<UI2DSprite> ().width = 250;
//			transform.GetComponent<UI2DSprite> ().height = 250;
			single_circle1.SetActive (false);
			single_circle2.SetActive (false);
			photo_circle.SetActive (true);
		}
	}

	public void click_bt(){
		PlayerPrefs.SetInt ("temp_avator_index", index);
        PlayerPrefs.Save();
		button_dialog.SetActive (true);
	}

	public IEnumerator DisplayPhoto(string photo_url, int w, int h) {
		WWW file = new WWW(photo_url);
		yield return file;

		Texture2D sourceTex = file.texture;
		Texture2D circle_texture = new Texture2D(sourceTex.height, sourceTex.width);
		int cx = sourceTex.width/2;
		int cy = sourceTex.height/2;
		int r = sourceTex.height / 2;
		for (int i = (int)(cx - r); i < cx + r; i++)
		{
			for (int j = (int)(cy - r); j < cy + r; j++)
			{
				float dx = i - cx;
				float dy = j - cy;
				float d = Mathf.Sqrt(dx * dx + dy * dy);
				if (d <= r)
					circle_texture.SetPixel(i - (int)(cx - r), j - (int)(cy - r), sourceTex.GetPixel(i, j));
				else
					circle_texture.SetPixel(i - (int)(cx - r), j - (int)(cy - r), Color.clear);
			}
		}
		circle_texture.Apply();

		Sprite ppp = new Sprite();
		ppp = Sprite.Create(circle_texture, new Rect(0, 0, w, h), new Vector2(0, 0), 350.0f);
		GetComponent<UI2DSprite>().sprite2D = ppp;
	}

}
