﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manage_Profile : MonoBehaviour
{

    public GameObject setting_bt;
    public Vector3 localposition;
    public int index;
    public string[] personal_data;
	public Sprite avatar;
    public GameObject home_title;
    public GameObject home_desc;

    public GameObject state_window;
    public GameObject profile_window;
    public GameObject home_window;

    public bool active_flag;
    public bool init_flag;
   // private PurpleToothbrush purple;
    private string profileMAC = "";

    private static string currentProfileIdUser = String.Empty;
	public GameObject start_bt;
    // private static bool firstLoad = true;
    // Use this for initialization
    void Start()
    {
        Debug.Log("KKK MPStart");
        currentProfileIdUser = String.Empty;
        active_flag = false;
        init_flag = false;
        if (index != -1)
        {
            if (transform.position.x < (0.02f) && transform.position.x > (-0.02f))
            {
                setting_bt.SetActive(true);
            }
            else
            {
                setting_bt.SetActive(false);
            }
        }

        if (index == 1)
        {
            home_title.GetComponent<UILabel>().text = personal_data[1];
            home_desc.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        localposition = transform.position;
        if (init_flag)
            active_init();

		if (transform.position.x < (0.02f) && transform.position.x > (-0.02f) && (index == -1)) {
			start_bt.GetComponent<UI2DSprite> ().color = Color.gray;
			start_bt.GetComponent<UIButton> ().isEnabled = false;
		} else {
			start_bt.GetComponent<UIButton> ().isEnabled = true;
		}
    }

    //
    //	IEnumerator callInit_Profile()
    //	{
    //		yield return new WaitForSeconds(1);
    //		active_init ();
    //	}

    public void start_init()
    {
        if (index != -1)
        {
            if (transform.position.x < (0.02f) && transform.position.x > (-0.02f))
            {
                setting_bt.SetActive(true);
                home_title.GetComponent<UILabel>().text = personal_data[1];
                home_desc.SetActive(true);
               // StartCoroutine(setActiveProfileID (personal_data [0]));
                //setActiveDeviceAddress ();
            }
            else
            {
                setting_bt.SetActive(false);
            }
        }
        else
        {
            if (transform.position.x < (0.02f) && transform.position.x > (-0.02f))
            {
                home_title.GetComponent<UILabel>().text = "ADD NEW CHILD";
                home_desc.SetActive(false);
				start_bt.GetComponent<UIButton> ().isEnabled = false;
            }
        }
    }

    public void active_init()
    {
        if (transform.position.x < (0.02f) && transform.position.x > (-0.02f) && (Config.activeprofile_index != index))
        {
            if (index != -1)
            {
                setting_bt.SetActive(true);
                home_title.GetComponent<UILabel>().text = personal_data[1];
                home_desc.SetActive(true);

                //initialize the profile window at same time
                profile_window.GetComponent<Manage_ProfileSetting>().personal_data = personal_data;

                //initialize the state window at same time
                state_window.GetComponent<Manage_StateWindow>().personal_data = personal_data;
                state_window.GetComponent<Manage_StateWindow>().init();

                StartCoroutine(setActiveProfileID(personal_data[0], personal_data[6]));

               

                //setActiveDeviceAddress ();
                //  Debug.Log("KKK1 PROFILE NAME:" + personal_data[0] + " NAME:" + personal_data[1]);
                home_window.GetComponent<Manage_HomeWindow>().setMusic();

            }
            else
            {
                home_title.GetComponent<UILabel>().text = "Add New Child";
				home_desc.SetActive (false);
            }
            Config.activeprofile_index = index;
            Debug.Log("Actived PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP: " + index);
            
        }

        if (transform.position.x < (0f) && transform.position.x > (-0.02f))
        {
            if (index != -1)
            {
                setting_bt.SetActive(true);
            }
        }

        if (transform.position.x >= (0f) || transform.position.x <= (-0.02f))
        {
            if (index != -1)
                setting_bt.SetActive(false);
        }
    }


    IEnumerator setActiveProfileID(string profileID, string bReport)
    {
        int count = 0;
        Debug.Log("KKK1 PROFILE :" + PlayerPrefs.GetString("ActiveProfile"));
        Config.activeprofile_counts += 1;

        

 //////////////////////////////FOR BATTERY/////////////////////////////////////////////////////       
        if(bReport==null || bReport.Equals(""))
        {
            home_window.GetComponent<Manage_HomeWindow>().setBatteryReportingPeriod("30.0");
            PlayerPrefs.SetString("battery" + PlayerPrefs.GetString("ActiveProfile"),"30.0");
        }
        else
        {
            home_window.GetComponent<Manage_HomeWindow>().setBatteryReportingPeriod(bReport);
            PlayerPrefs.SetString("battery" + PlayerPrefs.GetString("ActiveProfile"), bReport);
        }
////////////////////////////////////////////////////////////////////////////////////////////////
        if (profileID != currentProfileIdUser)
        {
            currentProfileIdUser = profileID;
            PlayerPrefs.SetString("ActiveProfile", profileID);
            PlayerPrefs.Save();
            profileMAC = PlayerPrefs.GetString("macFilter" + PlayerPrefs.GetString("ActiveProfile"));
            Debug.Log("KKK ACTIVE PROFILE AND MAC:" + PlayerPrefs.GetString("ActiveProfile") + "..." + profileMAC);

            // try
            //  {
            PurpleContainer.instance.StopPurpleToothBrush();
            while (count < 1000 && (PurpleContainer.instance.bleState != PurpleToothbrush.DISCONECTED
            && PurpleContainer.instance.bleState != PurpleToothbrush.DEINITIALIZED))
            {
                yield return new WaitForSeconds(0.01f);
                count++;
            }
            if (!profileMAC.Equals(string.Empty))
            {
                PurpleContainer.instance.StartPurpleToothBrush(false, profileMAC);
                Debug.Log("KKK PROFILE1 Start with MAC");
            }
            else
            {
                PurpleContainer.instance.StartPurpleToothBrush();
                Debug.Log("KKK PROFILE1 Start without MAC");
            }
            // }
            //  catch (Exception e)
            //  {
            //      Debug.Log("KKK PROFILE1 PT exception: " + e);
            //  }
        }
        else
        {
            yield return null;
        }
    }

    public void goto_setting()
    {
        state_window.GetComponent<Manage_StateWindow>().personal_data = personal_data;
		state_window.GetComponent<Manage_StateWindow> ().avatar = avatar;
        state_window.GetComponent<Manage_StateWindow>().personal_index = index;
        state_window.GetComponent<Manage_StateWindow>().init_flag = true;
        setting_bt.GetComponent<UIPlayTween>().enabled = true;
        setting_bt.GetComponent<UIPlayTween>().Play(true);
    }

    public void goto_profile()
    {
        profile_window.GetComponent<Manage_ProfileSetting>().personal_data = personal_data;
		profile_window.GetComponent<Manage_ProfileSetting> ().avatar = avatar;
        profile_window.GetComponent<Manage_ProfileSetting>().personal_index = index;
        profile_window.GetComponent<Manage_ProfileSetting>().setting_flag = true;
        if (index != -1)
        {
            profile_window.GetComponent<Manage_ProfileSetting>().new_flag = false;
        }
        else
        {
            profile_window.GetComponent<Manage_ProfileSetting>().new_flag = true;
        }
        GetComponent<UIPlayTween>().Play(true);
    }
}