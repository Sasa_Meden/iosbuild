﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class Manage_Menus : MonoBehaviour {

    public GameObject setting_menu;
    public GameObject nav_menu;
    public Color output;
	public GameObject loading;
    // Use this for initialization
    void Start()
    {
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Goto_Setting()
    {
        setting_menu.SetActive(true);
    }

    public void Goto_Nav()
    {
        //nav_menu.active = true;
    }

    public void Close_Setting() {
        setting_menu.SetActive(false);
    }

    public void Close_Nav()
    {
        //nav_menu.active = false;
    }

    public void Set_Theme_Color()
    {
        if (UIColorPicker.current != null)
        {
            PlayerPrefs.SetString("t_color", ""+UIColorPicker.current.value);
            PlayerPrefs.Save();
        }
        Debug.Log(PlayerPrefs.GetString("t_color"));
    }

	public void Goto_Home(){
		loading.SetActive (true);
        SceneManager.LoadScene("home", LoadSceneMode.Single);
        
	}

	public void Goto_devicestate(){
		
	}

	public void logout(){
		loading.SetActive (true);
		PlayerPrefs.DeleteKey ("token");
		PlayerPrefs.DeleteKey ("refresh_token");
		PlayerPrefs.DeleteKey ("profile_counter");
        SceneManager.LoadScene("auth", LoadSceneMode.Single);
        
	}
    
    
}
