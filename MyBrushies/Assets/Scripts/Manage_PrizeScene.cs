﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Manage_PrizeScene : MonoBehaviour {

	public GameObject gamesmenu_window;
	public GameObject playgames_window;
	public GameObject playgames_bt;
	public GameObject gamesmenu_bt;
	public GameObject web_obj;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        // Debug.Log("CCCC ManagePrizeScene");

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (web_obj.GetComponent<SampleWebView>().webview_flag)
            {
                web_obj.GetComponent<SampleWebView>().webview_flag = false;
                GameObject.Destroy(web_obj.GetComponent<SampleWebView>().webViewObject);
            }
            else if (gamesmenu_window.transform.localPosition.x == -611 && playgames_window.transform.localPosition.x != -611) {
				gamesmenu_bt.GetComponent<UIPlayTween> ().Play (true);
			} else if (playgames_window.transform.localPosition.x == -611 &&  gamesmenu_window.transform.localPosition.x == -611){
				playgames_bt.GetComponent<UIPlayTween> ().Play (true);
			}
		}
	}
}
