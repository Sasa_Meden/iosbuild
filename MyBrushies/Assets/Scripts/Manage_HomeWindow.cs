﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using SimpleJSON;

public class Manage_HomeWindow : MonoBehaviour {

	public int game_index;
	public GameObject loading;
	public bool sound_flag;
	public GameObject sound_img;
	public GameObject sound_label;
	public Sprite[] sounds;

    public float local_time;
	public int _Time;
	public GameObject battery_value_obj;
	public GameObject macaddress_obj;
	public GameObject activeprofileid_obj;
	public GameObject activeprofilecounts_obj;

	//Muisc Object from "Music" tag
	public GameObject mainmusicObject;

	private string baseURL="http://pearly01.mybrushies.com";

    private string adapter_id = "";
    private short batteryLevel = 100;
    private float batteryReportingPeriod = 30.0f;
 
    // Use this for initialization
    void Start () {
        Debug.Log("KKK HWS");
        AudioListener.volume = 1;
        Screen.orientation = ScreenOrientation.Portrait;
        local_time = 0f;
		sound_flag = false;
		mainmusicObject = GameObject.FindGameObjectWithTag("Music");
        if (mainmusicObject != null)
        {
            setMusic();
        }

		battery_value_obj.GetComponent<UILabel> ().text = GetBatteryLevel_normalized(batteryLevel) + "%";
        
//		StartCoroutine (WBatteryStatus ());
	}

    // Update is called once per frame
    void Update()
    {
        // this counts batteryReportingPeriod seconds
        if (local_time > batteryReportingPeriod)
        {
            //calling battery status api
            StartCoroutine(WBatteryStatus());
            local_time = 0.0f;
        }
        local_time += Time.deltaTime;

        
        if (PurpleContainer.instance == null)
            if (PurpleContainer.instance.bleState != PurpleToothbrush.CONNECTED)
            {
                adapter_id = "NOT CONNECTED";
            }
            else
            {
                adapter_id = PurpleContainer.instance.GetConnectedMAC();
                batteryLevel = PurpleContainer.instance.GetBatteryPercentage();
                Debug.Log("KKK HOME MAC FILTER" + PurpleContainer.instance.GetConnectedMAC());
        }
        
        macaddress_obj.GetComponent<UILabel>().text = adapter_id;
        battery_value_obj.GetComponent<UILabel>().text = GetBatteryLevel_normalized(batteryLevel) + "%";
		activeprofileid_obj.GetComponent<UILabel> ().text = PlayerPrefs.GetString ("ActiveProfile", "");
		activeprofilecounts_obj.GetComponent<UILabel> ().text = ""+Config.activeprofile_counts;
    }

    private short GetBatteryLevel_normalized(short realBatLevel)
    {
        //>40% say 100%
        //<10% say 0%
        //10-40 scale
        const short floor = 10;
        const short roof = 40;

        if (realBatLevel >= roof)
            return 100;
        if (realBatLevel <= floor)
            return 0;
        return (short)((realBatLevel - floor) * (100 / (roof - floor)));

    }

	public string GetUniqueID(){
		string key = "UNIQUE_ID";

		var random = new System.Random();                     
		DateTime epochStart = new System.DateTime(1970, 1, 1, 8, 0, 0, System.DateTimeKind.Utc);
		double timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;

		string uniqueID =GetPlatform()+String.Format("{0:X}", Convert.ToInt32(timestamp))+""+String.Format("{0:X}", Convert.ToInt32(Time.time*1000000))+""+String.Format("{0:X}", random.Next(1000000000));                //random number



		if(PlayerPrefs.HasKey(key)){
			uniqueID = PlayerPrefs.GetString(key);            
		} else {            
			PlayerPrefs.SetString(key, uniqueID);
			PlayerPrefs.Save();    
		}

		Debug.Log("Generated Unique ID: "+uniqueID);
		return uniqueID;
	}

	public string GetActiveProfileID(){
		string val = "";
		if (PlayerPrefs.HasKey ("ActiveProfile")) {
			val = PlayerPrefs.GetString ("ActiveProfile");
		} else {
			GameObject profile = gameObject.GetComponent<Manage_HomeProfiles> ().profiles [0];
			val = profile.GetComponent<Manage_Profile> ().personal_data [0];
		}
		return val;
	}

	public string GetAdapterID(){
            
        return adapter_id;
	}

	private string GetPlatform(){
		string val = "";
		if (Application.platform == RuntimePlatform.Android) {
			val="android";
		} else {
			val="iphone";
		}
		return val;
	}

	public void startGame(){
		loading.SetActive (true);
		if (game_index == 1) {
			StartCoroutine(goto_bubble());
		} else if (game_index == 2) {
            StartCoroutine(goto_pool());
		} else if (game_index == 3) {
            StartCoroutine(goto_ballons());
		}
	}

    /// <summary>
    /// ///IN PROFILE CREATION GENERATE UNIQUE id THAT WILL BE THE SAME FOR THAT PROFILE.
    /// ///each time user changes profile then save that unique ID to PlayerPrefs "ActiveProfileID"
    /// </summary>

	public void sound_off_on(){
		if (sound_flag) {
			sound_flag = false;
            mainmusicObject.GetComponent<AudioSource>().volume = 0f;
            sound_img.GetComponent<UI2DSprite> ().sprite2D = sounds [0];
			sound_label.GetComponent<UILabel>().text="MUSIC: OFF";
            PlayerPrefs.SetFloat("Music" + PlayerPrefs.GetString("ActiveProfile"), 0f);
            //PlayerPrefs.SetFloat("Music", 0f);
            PlayerPrefs.Save();
            Debug.Log("MUSIC OFF " + PlayerPrefs.GetFloat("Music" + PlayerPrefs.GetString("ActiveProfile")) +"Profile ID: "+ PlayerPrefs.GetString("ActiveProfile"));
        } else {
			sound_flag = true;
            mainmusicObject.GetComponent<AudioSource>().volume = 1f;
            sound_img.GetComponent<UI2DSprite> ().sprite2D = sounds [1];
			sound_label.GetComponent<UILabel>().text="MUSIC: ON";
			PlayerPrefs.SetFloat("Music" + PlayerPrefs.GetString("ActiveProfile"), 1f);
            //PlayerPrefs.SetFloat("Music", 1f);
            PlayerPrefs.Save();
            Debug.Log("MUSIC ON " + PlayerPrefs.GetFloat("Music" + PlayerPrefs.GetString("ActiveProfile")) + "Profile ID: " + PlayerPrefs.GetString("ActiveProfile"));
        }
	}

	

	//Calling Battery Status API
	IEnumerator WBatteryStatus() {

        //Debug.Log("GGG Battery");
        string postScoreURL= baseURL + "/v1/battery_statuses";
		WWWForm form = new WWWForm();
       
        string timestamp = "" + System.DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");

        Debug.Log("GGG btlShort " + batteryLevel);
        float btl = (float)batteryLevel;
        Debug.Log("GGG btlFloat " + btl);
        btl = btl / 100;
        Debug.Log("GGG btl100 " + btl);
        // GetActiveProfileID(): this returns actived profile id.
        // GetBatteryLevel(): this returns the current battery level percentage.
        // GetUniqueID(): this returns the phone unique id
        // GetAdapterID(); this returns the connected BLE device Mac Address
        var requestBody = "{ \"battery_status\": { \"timestamp\": \""+timestamp+"\", \"percentage\": "+ btl + ", \"phone_id\": \""+GetUniqueID()+"\", \"adapter_id\": \""+GetAdapterID()+"\" }}";
        Debug.Log("GGG requestLOG" + requestBody);
        var encoding = new System.Text.UTF8Encoding();
		var postHeader = form.headers;

		postHeader["Content-Type"] = "application/json";
		postHeader["Accept"]="application/json";
		postHeader["Authorization"]="Bearer "+PlayerPrefs.GetString("token");
		postHeader ["Current-Profile-Id"] = GetActiveProfileID ();

		WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
		yield return request;

		if (request.error != null)
		{
			loading.SetActive (false);
			Debug.Log("request error: " + request.text);
			Debug.Log("request error: " + request.responseHeaders);
		}
		else
		{
			JSONNode jNodes = JSON.Parse (request.text);
			Debug.Log ("success: " + jNodes["error_message"]);

		}
	}

    public void setMusic()
    {
        Debug.Log("MUSIC ActiveProfile " + PlayerPrefs.GetString("ActiveProfile") + "Profile ID: " + PlayerPrefs.GetString("ActiveProfile"));
        if (PlayerPrefs.HasKey("Music" + PlayerPrefs.GetString("ActiveProfile")))
        {
            Debug.Log("MUSIC HASKEY ");
            if (PlayerPrefs.GetFloat("Music" + PlayerPrefs.GetString("ActiveProfile")) == 0f)
            {
                Debug.Log("MUSIC OFF " + PlayerPrefs.GetFloat("Music" + PlayerPrefs.GetString("ActiveProfile")));
                sound_flag = false;
                mainmusicObject.GetComponent<AudioSource>().volume = 0f;
                sound_img.GetComponent<UI2DSprite>().sprite2D = sounds[0];
                sound_label.GetComponent<UILabel>().text = "MUSIC: OFF";
            }
            else
            {
                Debug.Log("MUSIC ON " + PlayerPrefs.GetFloat("Music" + PlayerPrefs.GetString("ActiveProfile")));
                sound_flag = true;
                mainmusicObject.GetComponent<AudioSource>().volume = 1f;
                sound_img.GetComponent<UI2DSprite>().sprite2D = sounds[1];
                sound_label.GetComponent<UILabel>().text = "MUSIC: ON";
            }
        }
        else
        {
            Debug.Log("MUSIC NOKEY ");
            mainmusicObject.GetComponent<AudioSource>().volume = 1f;
            sound_img.GetComponent<UI2DSprite>().sprite2D = sounds[1];
            sound_label.GetComponent<UILabel>().text = "MUSIC: ON";
            PlayerPrefs.SetFloat("Music" + PlayerPrefs.GetString("ActiveProfile"), 1f);
            // PlayerPrefs.SetFloat("Music", 1f);
            PlayerPrefs.Save();
            Debug.Log("MUSIC NOKEY ON " + PlayerPrefs.GetFloat("Music" + PlayerPrefs.GetString("ActiveProfile")));
            sound_flag = true;

        }
    }

    public void setBatteryReportingPeriod(string bPeriond)
    {
        try
        {
            batteryReportingPeriod = float.Parse(bPeriond);
        }
        catch (Exception e)
        {
            batteryReportingPeriod = 30.0f;
        }
    }







    IEnumerator goto_bubble(){
        // float fadeTime = GameObject.Find("Fade").GetComponent<Fade>().BeginFade(1);
        yield return null;// new WaitForSeconds(fadeTime);
		SceneManager.LoadScene("SquaresGame", LoadSceneMode.Single);
	}

    IEnumerator goto_pool(){
       // float fadeTime = GameObject.Find("Fade").GetComponent<Fade>().BeginFade(1);
        yield return null; //new WaitForSeconds(fadeTime);
        SceneManager.LoadScene("PoolRace", LoadSceneMode.Single);
	}

    IEnumerator goto_ballons(){
        //float fadeTime = GameObject.Find("Fade").GetComponent<Fade>().BeginFade(1);
        yield return null; //new WaitForSeconds(fadeTime);
        SceneManager.LoadScene("Balloons", LoadSceneMode.Single);
	}
}

