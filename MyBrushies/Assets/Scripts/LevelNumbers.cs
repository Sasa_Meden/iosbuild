﻿using UnityEngine;
using System.Collections;

public class LevelNumbers {
    static public int currentLevel = 0;
    static public string URL = "";
    // Use this for initialization
    public static void SetLvlNum(int lvl)
    {
        currentLevel = lvl;
    }

    public static int GetLvlNum() {
        return currentLevel;
    }
    public static void SetURL(string url)
    {
        URL = url;
    }

    public static string GetURL()
    {
        return URL;
    }
}
	