﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manage_HoppingWindw : MonoBehaviour {

	public bool started;
	public float _fTime = 0.0f;
	public int _iTime = 0;
	public Sprite[] clocks;
	public Sprite[] tooths;
	public Sprite[] bars;
	public GameObject clock;
	public GameObject tooth;
	public GameObject bar;
	public GameObject sec_Number;
	public GameObject old_desc3;
	public GameObject old_desc1;
	public GameObject old_desc2;

	public GameObject thanks_title;
	public GameObject thanks_desc;
	public GameObject thanks_image;

	public GameObject hopping_window;
	// Use this for initialization
	void Start () {
		started = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (started) {
			_fTime += Time.deltaTime;
			_iTime = (int)_fTime;

			if (_iTime < 16) {
				clock.GetComponent<UI2DSprite> ().sprite2D = clocks [_iTime];
				tooth.GetComponent<UI2DSprite> ().sprite2D = tooths [_iTime];
				bar.GetComponent<UI2DSprite> ().sprite2D = bars [_iTime];
				sec_Number.GetComponent<UILabel> ().text = ""+_iTime;
			} else {
				
				old_desc1.SetActive (false);
				old_desc2.SetActive (false);
				old_desc3.SetActive (false);
				clock.SetActive (false);
				tooth.SetActive (false);

				thanks_desc.SetActive (true);
				thanks_image.SetActive (true);
				thanks_title.SetActive (true);
				if (_iTime > 20) {
					started = false;
					hopping_window.SetActive (false);
					bar.GetComponent<UIPlayTween> ().enabled = true;
					bar.GetComponent<UIPlayTween> ().Play (true);
				}
			}

		}
	}
		
	/// <summary>
	/// SET this when user clicks button to play with NO device
	/// </summary>
	private void setDemoGame()
	{
		PlayerPrefs.SetInt("DemoGame", 1);
		PlayerPrefs.Save();
	}
	/// <summary>
	/// SET this when user clicks button to play WITH device
	/// </summary>
	private void setDeviceGame()
	{
		PlayerPrefs.SetInt("DemoGame", 0);
		PlayerPrefs.Save();
	}
}
