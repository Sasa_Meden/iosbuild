﻿using UnityEngine;
using System.Collections;

public class TreeScale : MonoBehaviour {
    private Transform transformThis;
    private static float x = 1.0f;
    private static float yMax = 1.0f;
    private static float yMin = 0.8f;
    private static float z = 1.0f;
    private Vector3 start = new Vector3(x, yMax, z);
    private Vector3 target = new Vector3(x, yMin, z);

    public float ScaleSpeed = 0.0f;
    void Awake()
    {
        Application.targetFrameRate = 30;
    }
    // Use this for initialization
    void Start()
    {
        transformThis = this.transform;
        ScaleSpeed = Random.Range(0.001f, 0.005f);
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 scale = transformThis.localScale;

        if (scale != target)
        {
            transformThis.localScale = Vector3.MoveTowards(scale, target, ScaleSpeed);
        }
        else
        {

            target = start;


            if (scale == start)
            {
                target = new Vector3(x, yMin, z);

            }
        }



    }
}

