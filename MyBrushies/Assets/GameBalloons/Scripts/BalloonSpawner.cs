﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using SimpleJSON;
using System;
public class BalloonSpawner : MonoBehaviour
{

    public Text infoText;
    public bool infoTxtShow = false;
    public Button btnStart;
    public Text txtCountDown;
    public GameObject CountDownAudio;
    private AudioSource countdownsound;
    public Button btnMainMenu;

    public Button btnSound;
    public Sprite spriteSoundOn;
    // private Color32 colorSoundOn;
    public Sprite spriteSoundOff;
    // private Color32 colorSoundOff;
    private bool soundOnOff = true;
    public GameObject mainAudio;

    public Button btnDevice;
    public Sprite deviceOn;
    // private Color32 colorDeviceOn;
    public Sprite deviceOff;
    // private Color32 colorDeviceOff;

    public GameObject startPanel;


    public GameObject exitPanel;
    public Button exitYes;
    public Button exitNo;


    public Image progressBar;
    private float countDownTime = 0.0f;

    private float sadCountTime = 0.0f;

    public Image imgGreenBlink;
    public Image imgRedBlink;
    public Image imgYellowBlink;
    public Image imgBlueBlink;

    public GameObject pumpa;
    private GameObject PumpUP;
    private PumpaScript pumpaScript;



    //private int index;
    private int balloonCounter = 0;
    public Text txtBalloonCounter;


    public bool startGame = false;



    public Button btnConnect;
    public Text txtConnect;
    private bool blink = false;
    private int countDownTimes = 1;

    // public GameObject SpeedIndicator;
    // private IndicatorScript speedIndicatorScript;
    public Slider speedIndicatorSlider;


    private GameObject mainmusicObject;
    private int brojac = 0;

    // public Text PumpSpeedText;

    private float SpeedMultiplyer = 1.0f;
    private int bSpeedValue = 0;

    public GameObject PearlyControler;
    private PearlyReactions PearlyText;
    public Text txtPearly;
    private bool talking = false;

    private float speedKoef = 0.0f;
    private float speed = 0.0f;
    // private int msgCounter = 2;

    public GameObject showScore;
    public GameObject showPlus;
    public GameObject showMinus;


    private bool brushiesDemo;

    private string baseURL = "http://pearly01.mybrushies.com";
    private string brushingSessionID = "";
    private float batteryReportingPeriod = 30.0f;

    public float local_time;
    private string adapter_id = "";
    private short batteryLevel = 100;


    //private bool brushingSessionComplete = false;

    // Use this for initialization
    void Start()
    {
        // PlayerPrefs.DeleteAll();

        Screen.orientation = ScreenOrientation.Landscape;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        if(PlayerPrefs.GetString("battery" + PlayerPrefs.GetString("ActiveProfile")).Equals(""))
        {
            setBatteryReportingPeriod("30.0");
        }else
        {
            setBatteryReportingPeriod(PlayerPrefs.GetString("battery" + PlayerPrefs.GetString("ActiveProfile")));
        }

        

        /////DEMO OR DEVICE/////
        if (PlayerPrefs.GetInt("DemoGame") == 0)
        {
            btnDevice.GetComponentInChildren<Image>().sprite = deviceOn;
            brushiesDemo = false;
            showPlus.SetActive(true);
            showMinus.SetActive(true);
        }
        else
        {
            btnDevice.GetComponentInChildren<Image>().sprite = deviceOff;
            //   btnDevice.GetComponentInChildren<Image>().color = colorDeviceOff;
            brushiesDemo = true;
            showPlus.SetActive(false);
            showMinus.SetActive(false);
        }
        countdownsound = CountDownAudio.GetComponent<AudioSource>();
        txtCountDown.text = "Play";
        countDownTimes = 1;
        countDownTime = 0.0f;
        balloonCounter = 0;
        txtBalloonCounter.text = "SCORE: " + balloonCounter;
        PearlyText = PearlyControler.GetComponent<PearlyReactions>();
       
        speedIndicatorSlider.value = 0.0f;

        if (!infoTxtShow)
        {
            infoText.text = "";
        }
        // index = Random.Range(0, 7);
        // index = 2;
        PumpUP = pumpa.transform.Find("PumpaUP").gameObject;
        pumpaScript = PumpUP.GetComponent<PumpaScript>();
        pumpaScript.setPumpSpeed(0.0f, 0.0f);
        btnConnect.interactable = false;
        txtConnect.text = "";
        ////////////////////////// MUSIC ///////////////////////////////////////////////////////////////////
        mainmusicObject = GameObject.FindGameObjectWithTag("Music");
        if (mainmusicObject != null)
        {
            if (PlayerPrefs.HasKey("Music" + PlayerPrefs.GetString("ActiveProfile")))
            {
                if (PlayerPrefs.GetFloat("Music" + PlayerPrefs.GetString("ActiveProfile")) == 0f)
                {
                    mainmusicObject.GetComponent<AudioSource>().volume = 0f;
                }
                else
                {
                    mainmusicObject.GetComponent<AudioSource>().volume = 1f;
                }
            }
            else
            {
                mainmusicObject.GetComponent<AudioSource>().volume = 1f;
                PlayerPrefs.SetFloat("Music" + PlayerPrefs.GetString("ActiveProfile"), 1f);
                PlayerPrefs.Save();
            }
        }

        //////////////////////////////////SOUND/////////////////////////////////////////////////////////////
        if (PlayerPrefs.HasKey("sound_" + PlayerPrefs.GetString("ActiveProfile")))
            {
                if(PlayerPrefs.GetInt("sound_" + PlayerPrefs.GetString("ActiveProfile")) == 1)
                {
                    ////Turn Sound On
                    btnSound.GetComponent<Image>().sprite = spriteSoundOn;
                    //  btnSound.GetComponent<Image>().color = colorSoundOn;
                    mainAudio.GetComponent<AudioListener>().enabled = true;
                    AudioListener.volume = 1;
                    soundOnOff = true;
                }else
                {
                    ////Turn Sound Off
                    btnSound.GetComponent<Image>().sprite = spriteSoundOff;
                    //  btnSound.GetComponent<Image>().color = colorSoundOff;
                    AudioListener.volume = 0;
                    soundOnOff = false;
                }

            }
            else
            {
                ////Turn Sound On
                btnSound.GetComponent<Image>().sprite = spriteSoundOn;
                //  btnSound.GetComponent<Image>().color = colorSoundOn;
                mainAudio.GetComponent<AudioListener>().enabled = true;
                AudioListener.volume = 1;
                soundOnOff = true;
                PlayerPrefs.SetInt("sound_" + PlayerPrefs.GetString("ActiveProfile"), 1);
                PlayerPrefs.Save();
            }

        //////////////////////////////////BRUSH SPEED/////////////////////////////////////////////////////     

        if (PlayerPrefs.HasKey("BrushSpeed"))
        {
            bSpeedValue = PlayerPrefs.GetInt("BrushSpeed");
            if (bSpeedValue == 0)
            {
                SpeedMultiplyer = 1.0f;

            }
            else if (bSpeedValue > 0)
            {
                SpeedMultiplyer = 1.0f + bSpeedValue / 10;
            }
            else
            {
                SpeedMultiplyer = (0.0f - bSpeedValue / 10) * -1;
            }
        }
        else
        {
            SpeedMultiplyer = 1.0f;
        }
        ////////////////////////////////DEMO GAME//////////////////////////////////////////
        if (brushiesDemo)
        {
            speedKoef = 0.0f;
            //showScore.SetActive(false);
            showPlus.SetActive(true);
            showMinus.SetActive(true);

        }
        else
        {
            speedKoef = 0.0f;
            //showScore.SetActive(true);
            showPlus.SetActive(false);
            showMinus.SetActive(false);
        }
        ////////////////////////////////////////////////////////////////////////////////////////


        startPanel.SetActive(true);
        progressBar.fillAmount = 0;

        

    }

    // Update is called once per frame
    void Update()
    {
        // StartCoroutine(captureScr());

        if (startGame)
        {

            if (countDownTimes > 4)
            {

                //startPanel.SetActive(true);
                startGame = false;
                txtPearly.text = "";
                //brushingSessionComplete = true;
                StartCoroutine(BalloonsEnd());

            }
            countDownTime += Time.deltaTime;
            progressBar.fillAmount = (countDownTime / 30);

            //Debug.Log(countDownTime);
            if (countDownTime > 30.0f)
            {
                countDownTimes++;
                switchQuadrant(countDownTimes);
                countDownTime = 0.0f;
                if (!brushiesDemo)
                {
                    if (countDownTimes < 5)
                    {
                        StartCoroutine(quadrantMessage(countDownTimes));
                    }
                }
                else
                {
                    if (countDownTimes < 5)
                    {
                        StartCoroutine(quadrantMessage(countDownTimes));
                    }
                }

            }

            ////////////////////NO BRUSH CODE////////////////////////////////////
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                BtnPlus();
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                BtnMinus();
            }
            ////////////////////////////////////////////////////////////////////////

            if (!brushiesDemo)
            {
                if (PurpleContainer.instance.bleState != PurpleToothbrush.CONNECTED)
                {
                    btnConnect.interactable = true;
                    txtConnect.color = new Color(1, 0, 0);
                    txtConnect.text = "Please connect brush device";
                    if (infoTxtShow)
                        infoText.text = PurpleContainer.instance.GetLog();
                    Debug.Log("VVV PBL" + PurpleContainer.instance.GetLog());
                    pumpaScript.setPumpSpeed(0.0f, 0.0f);
                    blink = true;
                    //sliderTransform.position = new Vector3(-15.4f, sliderTransform.position.y, sliderTransform.position.z);
                    //speedIndicatorScript.setLimitPos(-1);
                    speedIndicatorSlider.value = 0.0f;
                    if (speed < 0.001)
                    {
                        sadCountTime += Time.deltaTime;
                    }
                    else
                    {
                        sadCountTime = 0.0f;
                    }

                    if (speed < 0.001 && talking == false && sadCountTime > 5.0f)
                    {
                        StartCoroutine(SadMessages());
                    }
                }
                else
                {
                    if (infoTxtShow)
                        btnConnect.interactable = false;
                    txtConnect.color = new Color(0, 1, 0);
                    Debug.Log("VVV PBC" + PurpleContainer.instance);
                    if (blink)
                    {
                        StartCoroutine(connectTextBlink());
                    }
                    var sensorReading = PurpleContainer.instance.GetLastReading();


            /////////////////////////////////////////BATTERY REPORT/////////////////////////////
                    if (local_time > batteryReportingPeriod)
                    {
                        //calling battery status api
                        StartCoroutine(WBatteryStatus());
                        local_time = 0.0f;
                    }
                    local_time += Time.deltaTime;

                    adapter_id = PurpleContainer.instance.GetConnectedMAC();
                    batteryLevel = PurpleContainer.instance.GetBatteryPercentage();

            //////////////////////////////////////////////////////////////////////////////////

                    speed = sensorReading.velocity * SpeedMultiplyer;
                    pumpaScript.setPumpSpeed(speed, 0.0f);
                    if (speed < 0.001)
                    {
                        sadCountTime += Time.deltaTime;
                    }
                    else
                    {
                        sadCountTime = 0.0f;
                    }

                    if (speed < 0.001 && talking == false && sadCountTime > 5.0f)
                    {
                        StartCoroutine(SadMessages());
                    }

                    //  Debug.Log("TTTT"+pumpaScript.GetSpeed());

                    speedIndicatorSlider.value = sensorReading.velocity * SpeedMultiplyer;
                    //  PumpSpeedText.text = "Pvel"+pumpaScript.GetSpeed();
                    if (sensorReading.velocity * SpeedMultiplyer <= 0.001f)
                    {

                        speedIndicatorSlider.value = 0.0f;
                    }
                    if (sensorReading.velocity * SpeedMultiplyer >= 1.0f)
                    {

                        speedIndicatorSlider.value = 1.0f;
                    }

                }
            }
            else
            {


                if (speedKoef < 0.001)
                {
                    sadCountTime += Time.deltaTime;
                }
                else
                {
                    sadCountTime = 0.0f;
                }

                pumpaScript.setPumpSpeed(speedKoef, 0.0f);
                speedIndicatorSlider.value = speedKoef;
                if (speedKoef < 0.001 && talking == false && sadCountTime > 5.0f)
                {
                    StartCoroutine(SadMessages());
                }
            }
        }
        else
        {
            //  msgCounter = 2;
            sadCountTime = 0.0f;
            pumpaScript.setPumpSpeed(0.0f, 0.0f);
            //sliderTransform.position = new Vector3(-15.4f, sliderTransform.position.y, sliderTransform.position.z);
            // speedIndicatorScript.setLimitPos(-1);
            speedIndicatorSlider.value = 0.0f;
            progressBar.fillAmount = 0;
        }
        // Debug.Log("SLIDER " + speedIndicatorSlider.value + "VALUE " + speedKoef);
    }

    /// <summary>
    /// Go to GLobalMenu
    /// </summary>
    IEnumerator exitGame()
    {
        exitPanel.SetActive(false);
        yield return StartCoroutine(WStopBrushing());
        SceneManager.LoadScene("home", LoadSceneMode.Single);
    }
    IEnumerator endGame()
    {
        // AudioListener.volume = 1;
        //  soundOnOff = true;
        yield return StartCoroutine(WStopBrushing());
        SceneManager.LoadScene("prize", LoadSceneMode.Single);
    }
    /// <summary>
    /// Stat Game
    /// </summary>
    public void BtnStart()
    {
        btnStart.interactable = false;
        progressBar.fillAmount = 0;
        countDownTimes = 1;
        balloonCounter = 0;
        txtBalloonCounter.text = "SCORE: " + balloonCounter;
        ///////RemoveDialog
        countDownTime = 0.0f;
        switchQuadrant(countDownTimes);
        StartCoroutine(StartingMessages());
        // Debug.Log("START");
    }

    IEnumerator StartingMessages()
    {
        //brushingID = GetUniqueID();
        talking = true;
        txtPearly.text = PearlyText.getToothpaste();
        yield return new WaitForSeconds(3);
        txtPearly.text = PearlyText.getReady();
        yield return new WaitForSeconds(1);
        txtCountDown.text = "3";
        countdownsound.Play();
        yield return new WaitForSeconds(1);
        txtCountDown.text = "2";
        countdownsound.Play();
        yield return new WaitForSeconds(1);
        txtCountDown.text = "1";
        countdownsound.Play();
        yield return new WaitForSeconds(1);
        txtPearly.text = PearlyText.getGoodLuck();
        txtCountDown.text = "Go";
        yield return new WaitForSeconds(1.5f);
        startPanel.SetActive(true);
        startPanel.SetActive(false);
        startGame = true;


        StartCoroutine(WStartBrushig());
        txtPearly.text = PearlyText.getUpperLeft();
        yield return new WaitForSeconds(2);
        talking = false;

    }

    IEnumerator quadrantMessage(int q)
    {
        talking = true;
        switch (q)
        {
            case 2:
                txtPearly.text = PearlyText.getUpperRight();
                yield return new WaitForSeconds(3);
                break;
            case 3:
                txtPearly.text = PearlyText.getLowerLeft();
                yield return new WaitForSeconds(3);
                break;
            case 4:
                txtPearly.text = PearlyText.getLowerRight();
                yield return new WaitForSeconds(3);
                break;
        }
        talking = false;
    }

    IEnumerator SadMessages()
    {
        if (countDownTime < 25.0f)
        {
            talking = true;
            txtPearly.text = PearlyText.getNegativeMid();
            yield return new WaitForSeconds(5);
            talking = false;
        }
    }

    /// <summary>
    /// Go to ExitPanel
    /// </summary>
    public void BtnMainMenu()
    {
        exitPanel.SetActive(true);

    }

    /// <summary>
    /// Panel button exit Yes
    /// </summary>
    public void BtnExitYes()
    {
        StartCoroutine(exitGame());
    }
    /// <summary>
    /// Panel button exit No
    /// </summary>
    public void BtnExitNo()
    {
        exitPanel.SetActive(false);
    }

    /// <summary>
    /// Show device connected
    /// </summary>
    /// <returns></returns>
    IEnumerator connectTextBlink()
    {
        blink = false;
        txtConnect.text = "Device connected";
        yield return new WaitForSeconds(3.5f);
        txtConnect.text = "";
    }


    /// <summary>
    /// ProgressBars
    /// </summary>
    /// <param name="quadrant"></param>
    public void switchQuadrant(int quadrant)
    {
        switch (quadrant)
        {
            case 1:
                progressBar.color = new Color32(154, 205, 0, 255);
                StartCoroutine(BlinkGreenProgress());
                break;
            case 2:
                progressBar.color = new Color32(226, 104, 66, 255);
                StartCoroutine(BlinkRedProgress());
                break;
            case 3:
                progressBar.color = new Color32(255, 189, 23, 255);
                StartCoroutine(BlinkYellowProgress());
                break;
            case 4:
                progressBar.color = new Color32(66, 160, 226, 255);
                StartCoroutine(BlinkBlueProgress());
                break;
        }
    }
    IEnumerator BlinkGreenProgress()
    {
        Color32 cGreen = imgGreenBlink.color;
        while (countDownTimes == 1)
        {
            if (cGreen.a == 0)
            {
                cGreen.a = 1;
                imgGreenBlink.color = cGreen;
            }
            cGreen.a -= 51;
            imgGreenBlink.color = cGreen;
            yield return new WaitForSeconds(0.05f);
        }
        cGreen.a = 255;
        imgGreenBlink.color = cGreen;
    }
    IEnumerator BlinkRedProgress()
    {
        Color32 cRed = imgRedBlink.color;
        while (countDownTimes == 2)
        {
            if (cRed.a == 0)
            {
                cRed.a = 1;
                imgRedBlink.color = cRed;
            }
            cRed.a -= 51;
            imgRedBlink.color = cRed;
            yield return new WaitForSeconds(0.05f);
        }
        cRed.a = 255;
        imgRedBlink.color = cRed;
    }
    IEnumerator BlinkYellowProgress()
    {
        Color32 cYellow = imgYellowBlink.color;
        while (countDownTimes == 3)
        {

            if (cYellow.a == 0)
            {
                cYellow.a = 1;
                imgYellowBlink.color = cYellow;
            }
            cYellow.a -= 51;
            imgYellowBlink.color = cYellow;
            yield return new WaitForSeconds(0.05f);
        }
        cYellow.a = 255;
        imgYellowBlink.color = cYellow;
    }
    IEnumerator BlinkBlueProgress()
    {
        Color32 cBlue = imgBlueBlink.color;
        while (countDownTimes == 4)
        {
            if (cBlue.a == 0)
            {
                cBlue.a = 1;
                imgBlueBlink.color = cBlue;
            }
            cBlue.a -= 51;
            imgBlueBlink.color = cBlue;
            yield return new WaitForSeconds(0.05f);
        }
        cBlue.a = 0;
        imgBlueBlink.color = cBlue;
    }
    public void ResetColors()
    {
        Color32 cRed = imgRedBlink.color;
        Color32 cYellow = imgYellowBlink.color;
        Color32 cBlue = imgBlueBlink.color;
        cRed.a = 0;
        imgRedBlink.color = cRed;
        cYellow.a = 0;
        imgYellowBlink.color = cYellow;
        cBlue.a = 0;
        imgBlueBlink.color = cBlue;
    }

    public void numOfBalloons()
    {
        balloonCounter++;
        txtBalloonCounter.text = "SCORE: " + balloonCounter;
        if (countDownTime < 25.0f)
        {
            if (!talking)
            {
                if (UnityEngine.Random.Range(0, 2) == 0)
                {
                    txtPearly.text = PearlyText.getPopBalloonsTrue();
                }
                else
                {
                    txtPearly.text = PearlyText.getPositiveMid();
                }
            }
        }
    }

    public void BalloonMiss()
    {
        // if (!brushiesDemo)
        // {
        if (countDownTime < 25.0f)
        {
            if (!talking)
            {
                txtPearly.text = PearlyText.getPopBalloonsFalse();
            }
        }
        // }
        // else
        // {
        //    if (countDownTime < 25.0f)
        //    {
        //        if (!talking)
        //       {
        //          if ((msgCounter % 2) == 0)
        //          {
        //            txtPearly.text = PearlyText.getPositiveMid();
        //        }
        //       msgCounter++;
        //     }
        //   }
        // }
    }

    IEnumerator BalloonsEnd()
    {
        ResetColors();
        txtPearly.text = PearlyText.getTongue();
        yield return new WaitForSeconds(5);
        StartCoroutine(endGame());
    }
    /// <summary>
    /// Capture Screen
    /// </summary>
    /// <returns></returns>
    IEnumerator captureScr()
    {
        Application.CaptureScreenshot("D:\\Img\\Balloons" + brojac + ".png", 2);
        brojac++;
        yield return null;
    }

    /// <summary>
    /// Simulated Speed up
    /// </summary>
    public void BtnPlus()
    {
        if (speedKoef < 0.99f)
            speedKoef += 0.1f;

        // Debug.Log("B: " + speedKoef);

    }
    /// <summary>
    /// Simulated Speed down
    /// </summary>
    public void BtnMinus()
    {
        if (speedKoef > 0.05f)
            speedKoef -= 0.1f;

        //  Debug.Log("B: " + speedKoef);
    }

    /// <summary>
    /// Sound On/Off
    /// </summary>
    public void SoundOnOff()
    {
        if (soundOnOff)
        {
            ////Turn Sound Off
            btnSound.GetComponent<Image>().sprite = spriteSoundOff;
            //  btnSound.GetComponent<Image>().color = colorSoundOff;
            AudioListener.volume = 0;
            soundOnOff = false;
            PlayerPrefs.SetInt("sound_" + PlayerPrefs.GetString("ActiveProfile"), 0);
            PlayerPrefs.Save();
        }
        else
        {
            ////Turn Sound On
            btnSound.GetComponent<Image>().sprite = spriteSoundOn;
            //  btnSound.GetComponent<Image>().color = colorSoundOn;
            mainAudio.GetComponent<AudioListener>().enabled = true;
            AudioListener.volume = 1;
            soundOnOff = true;
            PlayerPrefs.SetInt("sound_" + PlayerPrefs.GetString("ActiveProfile"), 1);
            PlayerPrefs.Save();
        }
    }


    public void DemoOrDevice()
    {
        if (brushiesDemo)
        {
            btnDevice.GetComponent<Image>().sprite = deviceOn;
            // btnDevice.GetComponent<Image>().color = colorDeviceOn;
            brushiesDemo = false;
            PlayerPrefs.SetInt("DemoGame", 0);
            PlayerPrefs.Save();
            showPlus.SetActive(false);
            showMinus.SetActive(false);
        }
        else
        {
            btnDevice.GetComponent<Image>().sprite = deviceOff;
            // btnDevice.GetComponent<Image>().color = colorDeviceOff;
            brushiesDemo = true;
            PlayerPrefs.SetInt("DemoGame", 1);
            PlayerPrefs.Save();
            showPlus.SetActive(true);
            showMinus.SetActive(true);
        }
    }



    public string GetActiveProfileID()
    {
        string val = "";
        if (PlayerPrefs.HasKey("ActiveProfile"))
        {
            val = PlayerPrefs.GetString("ActiveProfile");
        }
        else
        {
            GameObject profile = gameObject.GetComponent<Manage_HomeProfiles>().profiles[0];
            val = profile.GetComponent<Manage_Profile>().personal_data[0];
        }
        return val;
    }

    

    //Calling Start Brushing Session API
    IEnumerator WStartBrushig()
    {

        Debug.Log("BBB STARTBRUSHING");
        string postScoreURL = baseURL + "/v1/brushing/begin";
        WWWForm form = new WWWForm();
        //tring timestamp = "" + System.DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss tt");



        // GetActiveProfileID(): this returns actived profile id.
        // GetBatteryLevel(): this returns the current battery level percentage.
        // GetUniqueID(): this returns the phone unique id
        // GetAdapterID(); this returns the connected BLE device Mac Address

        var requestBody = "{ \"brushing_event\": { \"id\": \"" + " " + "\", \"profile_id\": \"" + " " + "\" }}";
        //   Debug.Log("BBB requestLOG" + requestBody);
        var encoding = new System.Text.UTF8Encoding();
        var postHeader = form.headers;

        postHeader["Content-Type"] = "application/json";
        postHeader["Accept"] = "application/json";
        postHeader["Authorization"] = "Bearer " + PlayerPrefs.GetString("token");
        postHeader["Current-Profile-Id"] = GetActiveProfileID();

        WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
        yield return request;

        if (request.error != null)
        {
            // loading.SetActive(false);
            //  UIStatus.Show(Localization.Get("Battery Level Updated Failed!"));

            Debug.Log("BBB request error: " + request.error);
            Debug.Log("BBB request error: " + request.responseHeaders);
        }
        else
        {
            JSONNode jNodes = JSON.Parse(request.text);
            brushingSessionID = jNodes["brushing_event"]["id"];
            Debug.Log("BBB success: " + brushingSessionID);
            Debug.Log("BBB success: " + request.text);
            
        }

        //Debug.Log("BBB1 start" + brushingSessionID);

    }

    //Calling Start Brushing Session API
    IEnumerator WStopBrushing()
    {

        if (!brushingSessionID.Equals(""))// && brushingSessionComplete == true)/////////SEND ONLY IF STARTED BRUSHING
        {

            Debug.Log("BBB STOPBRUSHING");
            string postScoreURL = baseURL + "/v1/brushing/done";
            WWWForm form = new WWWForm();

            //tring timestamp = "" + System.DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss tt");



            // GetActiveProfileID(): this returns actived profile id.
            // GetBatteryLevel(): this returns the current battery level percentage.
            // GetUniqueID(): this returns the phone unique id
            // GetAdapterID(); this returns the connected BLE device Mac Address
            var requestBody = "{ \"id\": \"" + brushingSessionID + "\" }";
            Debug.Log("BBB1 requestLOG" + requestBody);
            var encoding = new System.Text.UTF8Encoding();
            var postHeader = form.headers;

            postHeader["Content-Type"] = "application/json";
            postHeader["Accept"] = "application/json";
            postHeader["Authorization"] = "Bearer " + PlayerPrefs.GetString("token");
            postHeader["Current-Profile-Id"] = GetActiveProfileID();

            WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
            yield return request;

            if (request.error != null)
            {
                // loading.SetActive(false);
                //  UIStatus.Show(Localization.Get("Battery Level Updated Failed!"));

                Debug.Log("BBB request error: " + request.error);
                Debug.Log("BBB request error: " + request.responseHeaders);
            }
            else
            {
                Debug.Log("BBB success: " + request.text);

            }

        }

    }

    //Calling Battery Status API
    IEnumerator WBatteryStatus()
    {

        //Debug.Log("GGG Battery");
        string postScoreURL = baseURL + "/v1/battery_statuses";
        WWWForm form = new WWWForm();

        string timestamp = "" + System.DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
        
        float btl = (float)batteryLevel;
        Debug.Log("GGG btlFloat " + btl);
        btl = btl / 100;
        Debug.Log("GGG btl100 " + btl);
        // GetActiveProfileID(): this returns actived profile id.
        // GetBatteryLevel(): this returns the current battery level percentage.
        // GetUniqueID(): this returns the phone unique id
        // GetAdapterID(); this returns the connected BLE device Mac Address
        var requestBody = "{ \"battery_status\": { \"timestamp\": \"" + timestamp + "\", \"percentage\": " + btl + ", \"phone_id\": \"" + GetUniqueID() + "\", \"adapter_id\": \"" + adapter_id + "\" }}";
        Debug.Log("GGG requestLOG" + requestBody);
        var encoding = new System.Text.UTF8Encoding();
        var postHeader = form.headers;

        postHeader["Content-Type"] = "application/json";
        postHeader["Accept"] = "application/json";
        postHeader["Authorization"] = "Bearer " + PlayerPrefs.GetString("token");
        postHeader["Current-Profile-Id"] = GetActiveProfileID();

        WWW request = new WWW(postScoreURL, encoding.GetBytes(requestBody), postHeader);
        yield return request;

        if (request.error != null)
        {
            //loading.SetActive(false);
            Debug.Log("request error: " + request.text);
            Debug.Log("request error: " + request.responseHeaders);
        }
        else
        {
            JSONNode jNodes = JSON.Parse(request.text);
            Debug.Log("success: " + jNodes["error_message"]);

        }
    }


    public string GetUniqueID()
    {
        string key = "UNIQUE_ID";

        var random = new System.Random();
        DateTime epochStart = new System.DateTime(1970, 1, 1, 8, 0, 0, System.DateTimeKind.Utc);
        double timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;

        string uniqueID = GetPlatform() + String.Format("{0:X}", Convert.ToInt32(timestamp)) + "" + String.Format("{0:X}", Convert.ToInt32(Time.time * 1000000)) + "" + String.Format("{0:X}", random.Next(1000000000));                //random number
        if (PlayerPrefs.HasKey(key))
        {
            uniqueID = PlayerPrefs.GetString(key);
        }
        else
        {
            PlayerPrefs.SetString(key, uniqueID);
            PlayerPrefs.Save();
        }

        Debug.Log("Generated Unique ID: " + uniqueID);
        return uniqueID;
    }
    private string GetPlatform()
    {
        string val = "";
        if (Application.platform == RuntimePlatform.Android)
        {
            val = "android";
        }
        else
        {
            val = "iphone";
        }
        return val;
    }

    public void setBatteryReportingPeriod(string bPeriond)
    {
        try
        {
            batteryReportingPeriod = float.Parse(bPeriond);
        }
        catch(Exception e)
        {
            batteryReportingPeriod = 30.0f;
        }
   }

    void OnApplicationQuit()
    {
        StartCoroutine(WStopBrushing());

    }

}
