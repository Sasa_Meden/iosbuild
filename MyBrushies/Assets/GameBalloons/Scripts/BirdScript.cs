﻿using UnityEngine;
using System.Collections;

public class BirdScript : MonoBehaviour
{
    private int randomSide = 0;
    private int randomScale = 0;
    private float Speed = 5.3f;
    private float height = 0.0f;
    private bool waitForIt = true;
    private Transform transformThis;
    private Vector3 direction;

    private float randomHeightMax = 0.0f;
    private float randomHeightMin = 0.0f;
    private int randomHeightDirection = 0;

    private Renderer spriteRenderOrder;
    private float FloatScale;
	public AudioSource WingFlapAudio;
    // Use this for initialization
    void Start()
    {
        transformThis = this.transform;
        spriteRenderOrder = this.GetComponentInChildren<Renderer>();
        SetPosition();
        StartCoroutine(WaitFor());
    }
    // Update is called once per frame
    void Update()
    {
       // CurrentPos = transformThis.position;
        if (transformThis.position.x > 35)
        {
            SetPosition();
            StartCoroutine(WaitFor());

        }
        if (transformThis.position.x < -35)
        {
            SetPosition();
            StartCoroutine(WaitFor());

        }
        if (waitForIt)
        {
            transformThis.Translate(direction * Speed * Time.deltaTime, Space.World);
        }
        if (transformThis.position.y < randomHeightMax || transformThis.position.y > randomHeightMin || transformThis.position.y > 10.0f || transformThis.position.y < -2.0f)
        {
            HeightDirection();
            StartCoroutine(HeightChange());
        }
    }

    private void SetPosition()
    {
		WingFlapAudio.Stop ();
		Speed = Random.Range(5.3f, 7.3f);
        randomSide = Random.Range(0, 2);
        randomScale = Random.Range(0,3);
        height = Random.Range(-2.0f, 8.0f);

        randomHeightMax = Random.Range(-1.1f, 1.1f);
        randomHeightMin = Random.Range(1.0f, 5.0f);

        if (randomSide == 0)
        {
            direction = new Vector3(-1, 0, 0);
            transformThis.position = new Vector3(35, height, 0);
            transformThis.localEulerAngles = new Vector3(0, 0, 0);
        }
        else
        {
            direction = new Vector3(1, 0, 0);
            transformThis.position = new Vector3(-35, height, 0);
            transformThis.localEulerAngles = new Vector3(0, 180, 0);
        }
        if (height > 1.0f)
        {
            if (randomScale == 0)
            {
                transformThis.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                spriteRenderOrder.sortingOrder = -1;
				//WingFlapAudio.volume = 0.3f;

            }
            else if(randomScale == 1)
            {
                transformThis.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                spriteRenderOrder.sortingOrder = -1;
				//WingFlapAudio.volume = 0.1f;
            }

        }
        else
        {
            transformThis.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            spriteRenderOrder.sortingOrder = -1;
			//WingFlapAudio.volume = 0.3f;
        }
    }

    IEnumerator WaitFor()
    {
        waitForIt = false;
        yield return new WaitForSeconds(Random.Range(7.0f, 35.0f));
        waitForIt = true;
		WingFlapAudio.Play ();

    }
    IEnumerator HeightChange()
    {
        // heightTime = false;
        yield return new WaitForSeconds(Random.Range(1.0f, 5.0f));
        //  heightTime = true;

    }
    public void HeightDirection()
    {
        randomHeightDirection = Random.Range(0, 2);
        randomHeightMax = Random.Range(-1.1f, 1.1f);
        randomHeightMin = Random.Range(1.0f, 5.0f);
        if (randomHeightDirection == 0)
        {
            direction.y = -1.0f;
        }
        else
        {
            direction.y = 1.0f;
        }
    }
}
