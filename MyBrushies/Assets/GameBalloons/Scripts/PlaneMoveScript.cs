﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneMoveScript : MonoBehaviour {
    private Transform transformThis;
    private bool waitForIt = true;
    public float PlaneSpeed = 7.0f;
    private Vector3 direction;
    // Use this for initialization
    void Start () {
        transformThis = this.transform;
        direction = new Vector3(-1, 0, 0);
        SetPosition();
        StartCoroutine(WaitFor());
    }
	
	// Update is called once per frame
	void Update () {

        if (transformThis.position.x < -30)
        {
            SetPosition();
            StartCoroutine(WaitFor());

        }
        
        if (waitForIt)
        {
            transformThis.Translate(direction * PlaneSpeed * Time.deltaTime, Space.World);
        }


    }

    private void SetPosition()
    {
        transformThis.position = new Vector3(30.0f, 10.13f, 0.0f);
    }
    IEnumerator WaitFor()
    {
        waitForIt = false;
        yield return new WaitForSeconds(Random.Range(6.0f, 30.0f));
        waitForIt = true;
   }
}
