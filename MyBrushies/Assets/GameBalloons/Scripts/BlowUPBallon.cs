﻿using UnityEngine;
using System.Collections;

public class BlowUPBallon : MonoBehaviour {
    public GameObject Blow;
    private Transform transformBlow;
	private Renderer rendererThis;

    

    private Vector3 Bp;
    public GameObject [] Butterflies;
    private GameObject B0;
    private GameObject B1;
    private GameObject B2;
    private GameObject B3;
    private GameObject B4;
   

    private int count = 0;

    void Start () {
        transformBlow = Blow.transform;
		rendererThis = Blow.GetComponentInChildren<Renderer> ();
        StartCoroutine(FadeOut());

        count = Random.Range(1, 6);

        switch (count)
        {

            case 1:
                Bp = new Vector3(Random.Range(transformBlow.position.x + 1.2f, transformBlow.position.x - 1.2f), Random.Range(transformBlow.position.y + 1.2f, transformBlow.position.y - 1.2f), 0.0f);
                B0 = Instantiate(Butterflies[1]);
                B0.transform.position = Bp;
                break;
            case 2:
                Bp = new Vector3(Random.Range(transformBlow.position.x + 1.2f, transformBlow.position.x - 1.2f), Random.Range(transformBlow.position.y + 1.2f, transformBlow.position.y - 1.2f), 0.0f);
                B1 = Instantiate(Butterflies[1]);
                B1.transform.position = Bp;
               break;
            case 3:
                Bp = new Vector3(Random.Range(transformBlow.position.x + 1.2f, transformBlow.position.x - 1.2f), Random.Range(transformBlow.position.y + 1.2f, transformBlow.position.y - 1.2f), 0.0f);
                B2 = Instantiate(Butterflies[2]);
                B2.transform.position = Bp;
                break;
            case 4:
                Bp = new Vector3(Random.Range(transformBlow.position.x + 1.2f, transformBlow.position.x - 1.2f), Random.Range(transformBlow.position.y + 1.2f, transformBlow.position.y - 1.2f), 0.0f);
                B3 = Instantiate(Butterflies[3]);
                B3.transform.position = Bp;
                break;
            case 5:
                Bp = new Vector3(Random.Range(transformBlow.position.x + 1.2f, transformBlow.position.x - 1.2f), Random.Range(transformBlow.position.y + 1.2f, transformBlow.position.y - 1.2f), 0.0f);
                B4 = Instantiate(Butterflies[4]);
                B4.transform.position = Bp;
                break;
        }
    }
	
    IEnumerator FadeOut()
    {
        for (int i = 0; i < 10; i++)
        {
            Color color = rendererThis.material.color;
            color.a -= 0.1f;
            rendererThis.material.color = color;
            yield return new WaitForSeconds(0.01f);
        }
        Destroy(Blow);
    }
}
