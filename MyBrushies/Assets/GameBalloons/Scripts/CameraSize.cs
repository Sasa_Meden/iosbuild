﻿using UnityEngine;
using System.Collections;

public class CameraSize : MonoBehaviour
{
    public Camera cam;
    private Vector2 aspectRatio;
    // public float FullScreenOrthographicSize = 1.0f;
    // Use this for initialization
    void Start()
    {
        aspectRatio = AspectRatio.GetAspectRatio(Screen.width, Screen.height);
        cam.aspect = aspectRatio.x / aspectRatio.y;
    }
    void Update()
    {
        cam.aspect = aspectRatio.x / aspectRatio.y;
    }
}
