﻿using UnityEngine;
using System.Collections;

public class PumpaScript : MonoBehaviour
{

    public GameObject[] FloatBalloon;
    public GameObject[] BlowBallon;
    public GameObject BallonSpawnPosition;
    public GameObject ExplodeBallon;
    public GameObject NewBlowBallon;
    private GameObject NewFloatBalloon;
    private BallonScript ballonScript;
    private BallonDissapear ballonFloatScript;
    private int index;
    public GameObject BalloonSpawn;
    private BalloonSpawner balloonSpawnerScript;

    public GameObject PumpUP;
    public float PumpSpeed = 0.0f;
    private static float x = -1.15f;
    private static float yMax = 4.0f;
    private static float yMin = 2.0f;
    private static float z = 0.0f;
    private Transform transformThis;
    private Vector3 start = new Vector3(x, yMax, z);
    private Vector3 target = new Vector3(x, yMin, z);
   
	public AudioSource pumpAudioDown;
    private bool firstDown = true;
   
    private float vel = 0.0f;
    private float qual = 0.0f;



    // Use this for initialization
    void Start()
    {
      
	    transformThis = PumpUP.transform;
        index = Random.Range(0, 7);
        NewBlowBallon = Instantiate(BlowBallon[index]);
        NewBlowBallon.transform.position = BallonSpawnPosition.transform.position;
        ballonScript = NewBlowBallon.GetComponent<BallonScript>();
        balloonSpawnerScript = BalloonSpawn.GetComponent<BalloonSpawner>();
        ballonScript.scaleSpeed = 0.0f;
       
  }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transformThis.localPosition;
        if (pos != target)
        {
            
            if (target.y == yMin)
            {
                ballonScript.deflate = false;
                ballonScript.ChangeBallonVelocity(vel,qual);

            }
            if (target.y == yMax)
            {
                ballonScript.deflate = true;
                ballonScript.ChangeBallonVelocity(vel,qual);

            }

            transformThis.localPosition = Vector3.MoveTowards(pos, target, PumpSpeed);
            if (firstDown && PumpSpeed > 0.0f)
            {
                pumpAudioDown.Play();
                firstDown = false;
                
            }
        }
        else
        {

            target = start;

            //pumpAudioDown.Stop ();
            if (pos == start)
            {
                pumpAudioDown.Play();
                target = new Vector3(x, yMin, z);
                //	audioPlay=1;
            }
        }
        pumpAudioDown.pitch = 0.897f + PumpSpeed;
       
        if ((ballonScript.CurrentScale >= ballonScript.MaxScale) && (vel < 0.4f || vel > 0.6f))
        {
            Destroy(NewBlowBallon);
            NewFloatBalloon = Instantiate(FloatBalloon[index]);
            NewFloatBalloon.transform.position = BallonSpawnPosition.transform.position;
            ballonFloatScript = NewFloatBalloon.GetComponent<BallonDissapear>();
            SetFloatDirection();
            ballonFloatScript.index = index;
            // balloonCounter++;
            // CountTextNumber.text = "" + balloonCounter;
            // index = 2;
            index = Random.Range(0, 7);
            NewBlowBallon = Instantiate(BlowBallon[index]);
            NewBlowBallon.transform.position = BallonSpawnPosition.transform.position;
            ballonScript = NewBlowBallon.GetComponent<BallonScript>();
            balloonSpawnerScript.BalloonMiss();
        }

        if ((ballonScript.CurrentScale >= ballonScript.MaxScale) && (vel >= 0.4f || vel <= 0.6f) )
        {
            //   Debug.Log("BLOW : "+ amp);
            Destroy(NewBlowBallon);
            Instantiate(ExplodeBallon);
            ExplodeBallon.transform.position = BallonSpawnPosition.transform.position;
            //index = 2;
            index = Random.Range(0, 7);
            NewBlowBallon = Instantiate(BlowBallon[index]);
            NewBlowBallon.transform.position = BallonSpawnPosition.transform.position;
            ballonScript = NewBlowBallon.GetComponent<BallonScript>();
            balloonSpawnerScript.numOfBalloons();
        }

       
    }

   public void setPumpSpeed(float Rvelocity, float Rquality)
    {
        vel = Rvelocity;
        qual = Rquality;
        if (Rvelocity >= 0.1f)
        {
			//pumpAudio.Play ();
			// LblText = string.Empty;
            // LblText = string.Format("RECIEVED:\t{0}", Rvelocity);
            //Debug.Log (Rvelocity);
			//pumpAudio.pitch = 0.897f + (Rvelocity+0.03f);
            if (Rvelocity > 0.5f) { PumpSpeed = 0.05f + (Rvelocity / 3.0f);
				//pumpAudio.pitch = 0.897f + (Rvelocity / 3);
            }
            else
            {
                PumpSpeed = 0.05f + (Rvelocity / 4.0f);
				//pumpAudio.pitch = 0.897f + (Rvelocity / 2);
            }
            
        }
        else {
            stopPump();

        }
    }
    private void stopPump()
    {
        Vector3 pos = transform.localPosition;
        if (pos != start)
        {
            transformThis.localPosition = Vector3.MoveTowards(pos, start, 0.05f);
        }
        
        if (ballonScript != null && ballonScript.CurrentScale >= 0.001f)
        {
       //  Debug.Log("DEFLATE");
        ballonScript.deflate = true;
         }

        pumpAudioDown.Stop ();
        //audioPlay = 2;
        PumpSpeed = 0.0f;
        firstDown = true;
    }

    /////////////////////////NEW CODE//////////////////////////////////////////////////

    /* public void setPumpSpeed(int amplitude)
     {
         amp = amplitude;
        // Debug.Log("AMP: "+amp);
         if (amplitude <= 500)
         {
             PumpSpeed = ((yMax - yMin) / amplitude) * 10;
             //pumpAudio.pitch = 0.897f + (Rvelocity / 2);
            // Debug.Log(PumpSpeed);
         }

         else
         {
             stopPump();

         }
     }*/
    ///////////////////////////////////////////////////////////////////////////

    /* private void stopPump() {
         Vector3 pos = transformThis.localPosition;
         if (pos != start)
         {
             transformThis.localPosition = Vector3.MoveTowards(pos, start, 0.05f);
         }
         ballonScript.ChangeBallonVelocity(amp);
         //if (ballonScript.CurrentScale > 0.001f)
       //  {
            // Debug.Log("DEFLATE");
             ballonScript.deflate = true;
        // }

         //pumpAudio.Stop ();
         //audioPlay = 2;
         PumpSpeed = 0.0f;
         firstDown = true;
     }*/
    ///////////////////////////////////////////////////////////////////////////
    void SetFloatDirection()
    {
        float dir = Random.Range(-1.0f, 1.0f);
        if (dir < 0.0)
        {
            ballonFloatScript.directionSpeed = 0.3f;
        }
        else
        {
            ballonFloatScript.directionSpeed = -0.3f;
        }
    }
    public float GetSpeed()
    {
        return PumpSpeed;
    }

}
