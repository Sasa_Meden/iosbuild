﻿using UnityEngine;
using System.Collections;

public class BallonScript : MonoBehaviour
{
    public GameObject ballon;
    private Transform transformThis;
    public float scaleSpeed = 0.5f;
    public float MaxScale = 1.0f;
    public float MinScale = 0.01f;
    public float CurrentScale = 0.0f;
    public AudioSource deflateAudio;
    private int audioStop = 0;
    public bool deflate = true;
    // public float speedMillis;
    void Start()
    {
        transformThis = ballon.transform;
        deflateAudio.volume = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        CurrentScale = transformThis.localScale.x;

        

        if (CurrentScale < 0.001f)
        {
            transformThis.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        }
        if (transformThis.localScale.x < 1.0f && deflate == false)
        {

            transformThis.localScale += new Vector3(MaxScale, MaxScale, MaxScale) * scaleSpeed * Time.deltaTime;
        }
        if (transformThis.localScale.x <= 1.0f && deflate == true)
        {

            transformThis.localScale += new Vector3(MaxScale, MaxScale, MaxScale) * (-0.08f) * Time.deltaTime;
        }

    }
   public void ChangeBallonVelocity(float Rvelocity, float Rquality)
    {
        if (Rvelocity >= 0.1f)
        {
            deflateAudio.volume = 0.8f;
            if (deflateAudio.isPlaying) {
                deflateAudio.Stop();
                audioStop = 0;
            }
            if (Rvelocity > 0.5f)
            {
                scaleSpeed = 0.5f + (Rvelocity / 4);
            }
            else
            {
                scaleSpeed = 0.5f + (Rvelocity / 3);
            }

        }
        else
        {
            if (audioStop == 0)
            {
                deflateAudio.Play();
                deflateAudio.pitch = 1.0f;
                audioStop = 1;
            }
            scaleSpeed = -0.08f;
            deflateAudio.pitch -= 0.001f;
            if (CurrentScale <= 0.002f)
            {
                deflateAudio.Stop();
                audioStop = 0;
                scaleSpeed = 0.0f;
            }
        }


    }


    /*public void ChangeBallonVelocity(int amplitude)
    {
        if (amplitude <= 500)
        {
            scaleSpeed = ((MaxScale - MinScale) / amplitude) * 55;

        }

          if (scaleSpeed >= 0.008f)
        {
            if (deflateAudio.isPlaying)
            {
                deflateAudio.Stop();
                audioStop = 0;
            }
            

        }
        else if(amplitude > 500)
        {
            if (audioStop == 0)
            {
                deflateAudio.Play();
                deflateAudio.pitch = 1.0f;
                audioStop = 1;
            }
            scaleSpeed = -0.18f;
            deflateAudio.pitch -= 0.001f;
            if (CurrentScale < 0.0f)
            {
                transformThis.localScale = new Vector3(0.001f, 0.001f, 0.001f);
                deflateAudio.Stop();
                audioStop = 0;
                scaleSpeed = 0.0f;
            }
        }


    }*/

   /* public void ChangeBallonVelocity(int amplitude)
    {
        if (amplitude <= 500)
        {
            scaleSpeed = ((MaxScale - MinScale) / amplitude) * 55;

            if (scaleSpeed >= 0.108f)
            {
                if (deflateAudio.isPlaying)
                {
                    deflateAudio.Stop();
                    audioStop = 0;
                }

            }
        }
        else
        {
            if (audioStop == 0 && CurrentScale > 0.001f)
            {
                deflateAudio.Play();
                deflateAudio.pitch = 1.0f;
                audioStop = 1;
            }
            scaleSpeed = -0.18f;
            deflateAudio.pitch -= 0.0008f;
            if (CurrentScale <= 0.001f)
            {
                deflateAudio.Stop();
                audioStop = 0;
                scaleSpeed = 0.0f;
            }
        }
    }*/
}



