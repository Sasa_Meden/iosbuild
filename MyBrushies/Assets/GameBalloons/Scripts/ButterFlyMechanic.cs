﻿using UnityEngine;
using System.Collections;

public class ButterFlyMechanic : MonoBehaviour
{

    private bool OnScreen = true;
    public float Speed = 4.0f;
    //private float side = 32.0f;
    private float height = -1.0f;
    private int randomSide = 0;

    private Vector3 direction = new Vector3(0f, 0f, 0f);

    private float randomHeightMax = 0.0f;
    private float randomHeightMin = 0.0f;
    private int randomHeightDirection = 0;
    private int randomSideDirection = 0;
   // private float randomSideMax = 0.0f;
   // private float randomSideMin = 0.0f;
    //private bool sideTime = true;

    private bool waitOff = true;
    //private bool heightTime = true;

   // private float TimeOnScreen = 0.0f;
    private float OffScreenTime;
    //private float MaxTimeOnScreen = 0.0f;

    private float FirstTime = 0.0f;
    private float Sidetime = 0.0f;

    public bool InFlower = false;
    private Transform FlowerCenter;
    public ButterFlyMechanic butterFlyScript;

    private Animator animatorThis;
    private Transform transformThis;

    

    // Use this for initialization
    void Start()
    {
       
        transformThis = this.transform;
        animatorThis = this.GetComponentInChildren<Animator>();
        FirstTime = Random.Range(0.1f, 4.0f);
       // MaxTimeOnScreen = Random.Range(10.0f, 25.0f);
       // SetStartSideHeight();
        HeightDirection();
       // StartCoroutine(WaitOffScreen());
        // StartCoroutine(HeightChange());
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log("Z: " + transformThis.position.z);
        // transformThis.Translate(Vector3.left * Speed * Time.deltaTime, Space.World);

        if (!InFlower)
        {
            animatorThis.SetBool("IsFlying", true);
            if (OnScreen)
            {
                if (Time.time > Sidetime)
                {
                    SideDirection();
                    StartCoroutine(SideChange());
                    Sidetime = Time.time + FirstTime;
                }

            }



            //  Debug.Log("MaxTimeOnScreen: " + MaxTimeOnScreen + "Time: " + Time.time);

           /* if (transformThis.position.x > 32)
            {
                //  SetStartSideHeight();
                //   HeightDirection();
                // StartCoroutine(HeightChange());
                //   StartCoroutine(WaitOffScreen());
                outside();

            }
            if (transformThis.position.x < -32)
            {
                //   SetStartSideHeight();
                //   HeightDirection();
                //  StartCoroutine(HeightChange());
                //    StartCoroutine(WaitOffScreen());
                outside();
            }*/

            if (transformThis.position.y < randomHeightMax || transformThis.position.y > randomHeightMin || transformThis.position.y < -10.0f)
            {
                HeightDirection();
                StartCoroutine(HeightChange());
            }
            if (waitOff)
            {
                OnScreen = true;
                transformThis.Translate(direction * Speed * Time.deltaTime, Space.World);
            }

        }
        else {
             transformThis.position = Vector3.MoveTowards(transformThis.position, FlowerCenter.position, 0.3f);
             if (transformThis.position == FlowerCenter.position)
             {
                 StartCoroutine(OnFlower());


             }


        }
       /* if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            animatorThis.SetBool("IsFlying", true);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            animatorThis.SetBool("IsFlying", false);
        }*/

    }

    public void SetStartSideHeight()
    {
        randomSide = Random.Range(0, 2);
        height = Random.Range(-10.0f, -2.0f);

        if (randomSide == 0)
        {
           
            direction = new Vector3(-1.0f, 0.0f, 0.0f);
            transformThis.position = new Vector3(32f, height, 0f);
            transformThis.localEulerAngles = new Vector3(0, 0, 0);
        }
        else
        {
           
            direction = new Vector3(1.0f, 0.0f, 0.0f);
            transformThis.position = new Vector3(-32f, height, 0f);
            transformThis.localEulerAngles = new Vector3(0, 180, 0);
        }

    }
    public void HeightDirection()
    {
        randomHeightDirection = Random.Range(0, 2);
        randomHeightMax = Random.Range(-9.1f, -7.1f);
        randomHeightMin = Random.Range(-7.0f, -3.1f);
        if (randomHeightDirection == 0)
        {
            direction.y = -1.0f;
        }
        else
        {
            direction.y = 1.0f;
        }
    }

    public void SideDirection()
    {
        randomSideDirection = Random.Range(0, 2);
       
        FirstTime = Random.Range(0.1f, 4.0f);

        if (randomSideDirection == 0)
        {
            direction.x = -1.0f;
            transformThis.localEulerAngles = new Vector3(0, 0, 0);
        }
        else
        {
            direction.x = 1.0f;
            transformThis.localEulerAngles = new Vector3(0, 180, 0);
        }
    }
   /* IEnumerator WaitOffScreen()
    {
        OffScreenTime = Random.Range(10.0f, 15.0f);
        MaxTimeOnScreen = Time.time + Random.Range(10.0f, 25.0f) + OffScreenTime;
        OnScreen = false;
        waitOff = false;
		animatorThis.SetBool("IsFlying", true);
        yield return new WaitForSeconds(OffScreenTime);
        waitOff = true;


    }*/
    IEnumerator HeightChange()
    {
        // heightTime = false;
        yield return new WaitForSeconds(Random.Range(1.0f, 5.0f));
        //  heightTime = true;

    }
    IEnumerator SideChange()
    {
        // sideTime = false;
        yield return new WaitForSeconds(Random.Range(1.0f, 5.0f));
        //  sideTime = true;
        
    }
    IEnumerator OnFlower() {
        animatorThis.SetBool("IsFlying", false);
        InFlower = true;
        yield return new WaitForSeconds(Random.Range(10.0f, 15.0f));
        InFlower = false;
        animatorThis.SetBool("IsFlying", true);
    }
    public void StopLocalCoroutine()
    {
        StopCoroutine("OnFlower");
    }
    void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.tag == "Flower")
        {
            InFlower = true;
            FlowerCenter = other.gameObject.transform.Find("TargetPosition").GetComponentInChildren<Transform>();
        }
        if (other.gameObject.tag == "ButterFly")
        {
           // Debug.Log("BUTTERFLY");
            butterFlyScript = other.gameObject.GetComponent<ButterFlyMechanic>();
            if (butterFlyScript.InFlower) {
                butterFlyScript.StopLocalCoroutine();
                butterFlyScript.InFlower = false;
            }
        }
        if(other.gameObject.tag == "destroyButterfly")
        {
            Destroy(gameObject);
        }
    }

    
    

}
