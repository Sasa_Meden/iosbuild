﻿using UnityEngine;
using System.Collections;

public class CloudMove : MonoBehaviour
{
    //public GameObject cloud;

    public float Speed = 0.3f;
    private float xLeft = -27.0f;
    private float xRight = 27.0f;
    private float y = 10.0f;
    private float z = 0.0f;
    private Transform transformThis;
    // Use this for initialization
    void Start()
    {
        transformThis = this.transform;

    }

    // Update is called once per frame
    void Update()
    {
        if (transformThis.position.x > xRight)
        {
            SetPositionAndSpeedAndScale();
        }
        transformThis.Translate(Vector3.right * Speed * Time.deltaTime, Space.World);

    }

    public void SetPositionAndSpeedAndScale()
    {
        Speed = Random.Range(0.3f, 0.8f);
        y = Random.Range(8.5f, 11.0f);
        transformThis.position = new Vector3(xLeft, y, z);
        float scale = Random.Range(0.4f, 0.6f);
        transformThis.localScale = new Vector3(scale, scale, scale);
    }
}