﻿using UnityEngine;
using System.Collections;

public class BallonDissapear : MonoBehaviour
{
    public float height = -0.5f;
    public GameObject FloatBallon;
    private Transform transformBallon;
    private Transform TargettransformBallon;
   // public GameObject ExplodeBallon;
    private float Scale_speed = 0.1f;
    private float verticalSpeed = 2.5f;
    public float directionSpeed = 0.3f;
    public float startScale;
    private float directionKoef = 0.001f;
    private Renderer spriteRenderOrder;
    private BirdScript birdScript;
    private float rotationSpeed = 1.0f;
    public float maxRotation = 10.0f;
   

    public int index = 0;
    // Use this for initialization
    void Start()
    {
        transformBallon = FloatBallon.transform;
        //spriteRenderOrder = this.renderer;
        //transformBallon.rotation = new Quaternion().;
        spriteRenderOrder = FloatBallon.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
       
        transformBallon.Translate(Vector3.up * verticalSpeed * Time.deltaTime, Space.World);
        transformBallon.Translate(Vector3.right * directionSpeed * Time.deltaTime, Space.World);
        transformBallon.rotation = Quaternion.Euler(0.0f, 0.0f, maxRotation * Mathf.Sin(Time.time * rotationSpeed));
        if (transformBallon.localScale.x < 0.04f || transformBallon.position.y > 12.0f || transformBallon.position.x > 23.0 || transformBallon.position.x < -23.0)
        {
            Destroy(FloatBallon);
        }
        if (transformBallon.position.y < -9.7)
        {
            Scale_speed = 0.07f;
            verticalSpeed = 3.5f;
            spriteRenderOrder.sortingOrder = -3;
            // maxRotation -= 2.0f;
        }

        if (transformBallon.position.y > -5.8)
        {
            Scale_speed = 0.1f;
            verticalSpeed = 2.5f;
            spriteRenderOrder.sortingOrder = -4;
            // maxRotation -= 4.0f;
            if (directionSpeed > 0.0)
            {
                directionSpeed += directionKoef;
            }
            else
            {
                directionSpeed -= directionKoef;
            }
            if (transformBallon.position.y > 0.0f)
            {
                Scale_speed = 0.15f;
            }
            // directionSpeed += 0.006f;
            SetScaleSpeed();
            transformBallon.localScale -= Vector3.one * Scale_speed * Time.deltaTime;
            //height = transformBallon.localPosition.y;
        }
        
    }
    void SetScaleSpeed()
    {
        directionKoef = Random.Range(0.001f, 0.009f);
    }
}
