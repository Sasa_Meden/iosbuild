#if UNITY_IOS
using System.Collections;
using System.IO;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using UnityEditor;
using UnityEngine;

public class UnityWebViewPostprocessBuild {
    [PostProcessBuild(100)]
    public static void OnPostprocessBuild(BuildTarget buildTarget, string path) {
        if (buildTarget == BuildTarget.iOS) {
            
			string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
			PBXProject proj = new PBXProject();
            proj.ReadFromString(File.ReadAllText(projPath));
			string target = proj.TargetGuidByName("Unity-iPhone");
			proj.AddFrameworkToProject(target, "WebKit.framework", false);
			proj.AddFrameworkToProject(target, "CoreBluetooth.framework", false);
			proj.SetBuildProperty(target, "IPHONEOS_DEPLOYMENT_TARGET", "9.0");
			proj.SetBuildProperty(target, "DEVELOPMENT_TEAM", "QR582WPHW7");
			proj.SetBuildProperty(target, "CODE_SIGN_IDENTITY", "iPhone Distribution: Ammar Darkazanli (QR582WPHW7)");
			proj.SetBuildProperty(target, "PROVISIONING_PROFILE_SPECIFIER", "63adbd1a-2940-4957-a83f-a8e9f88951e3");
            File.WriteAllText(projPath, proj.WriteToString());


			// Get plist
			string plistPath = path + "/Info.plist";
			PlistDocument plist = new PlistDocument();
			plist.ReadFromString(File.ReadAllText(plistPath));
			// Get root
			PlistElementDict rootDict = plist.root;
			// Change value of CFBundleVersion in Xcode plist
			rootDict.SetString("CFBundleVersion", "4");
			rootDict.SetString("CFBundleShortVersionString", "1.0");
			rootDict.SetString("CFBundleIdentifier", "com.mps.brushies");
			// Write to file
			File.WriteAllText(plistPath, plist.WriteToString());

        }
    }
}
#endif
